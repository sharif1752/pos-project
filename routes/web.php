<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/vueHome', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/script','pos\ScriptController@index');
Route::get('/user/login','Auth\UserLoginController@showLoginForm')->name('user.login');
Route::post('/user/submit','Auth\UserLoginController@login')->name('user.login.submit');
Route::get('user/logout','Auth\UserLoginController@logout')->name('user.logout');

Route::get('auto-logout-action', 'MasterController@autoLogOutAction')->name('autoLogOutAction');
Route::group(['middleware' => 'auth.user'], function () {
	Route::get('/','MasterController@posIndex')->name('pos.home');
});
Route::get('jsBaseURLs', 'AdapterController@jsBaseURLs')->name('jsBaseURLs');
// pos
Route::group(['middleware' => ['auth.user'],'prefix' => 'pos', 'as'=>'pos.'], function (){
	Route::get('/', 'MasterController@posIndex')->name('home');
	Route::get('jsBaseURLs', 'AdapterController@posJsBaseURLs')->name('jsBaseURLs');
	Route::get('home', 'MasterController@home');
	Route::get('/getmenu','MasterController@getUserMenuList');
	Route::group(['namespace' => 'pos'], function (){
		posRoute();
	});
	//reportRoute();
});

// Admin
Route::get('/admin/','AdminHomeController@index')->name('admin.home');
Route::get('/admin/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/submit','Auth\AdminLoginController@login')->name('admin.login.submit');
Route::post('admin/logout','Auth\AdminLoginController@logout')->name('admin.logout');
Route::group(['middleware' => ['auth.admin'],'prefix' => 'admin', 'as'=>'admin.'], function(){
	Route::get('jsBaseURLs', 'AdapterController@adminJsBaseURLs')->name('jsBaseURLs');
	Route::group(['namespace' => 'admin'], function (){
		Route::get('/list','AdminController@index');
		Route::post('/add','AdminController@store');
		Route::get('/create','AdminController@create');
		Route::delete('delete/{id}','AdminController@destroy');
		Route::get('edit/{id}','AdminController@show');
		Route::get('/create/role','UserRoleController@create');
		Route::get('/create/adminrole','AdminRoleCocntroller@create');
		Route::get('/role/list','UserRoleController@index');
		Route::get('/adminrole/list','AdminRoleCocntroller@index');
		Route::post('/add/role','UserRoleController@store');
		Route::post('/add/adminrole','AdminRoleCocntroller@store');
		Route::get('/role/edit/{id}','UserRoleController@show');
		Route::get('/adminrole/edit/{id}','AdminRoleCocntroller@show');
		Route::delete('/role/delete/{id}','UserRoleController@destroy');
		Route::delete('/adminrole/delete/{id}','AdminRoleCocntroller@destroy');
		Route::get('/menu/list','MenuController@index');
		Route::post('/add/menu','MenuController@store');
		Route::get('/create/menu','MenuController@create');
		Route::get('/edit/menu/{id}','MenuController@show');
		Route::get('/getmenu','MenuController@getAdminMenuList');
		Route::delete('/delete/menu/{id}','MenuController@destroy');
		Route::post('/passwordchange','ChangePasswordController@passwordChange');
		Route::get('/user/list','UserController@index');
		Route::post('/user/add','UserController@store');
		Route::get('/user/create','UserController@create');
		Route::delete('/user/delete/{id}','UserController@destroy');
		Route::get('/user/edit/{id}','UserController@show');

		// Sales person
		Route::get('/salesPerson','SalesPersonController@index');
		Route::get('/salesPerson/create','SalesPersonController@create');
		Route::post('/salesPerson/add','SalesPersonController@store');
		Route::get('/salesPerson/edit/{id}','SalesPersonController@edit');
		Route::delete('/salesPerson/delete/{id}','SalesPersonController@destroy');
		//vndor
		Route::get('/vendor/list','VendorController@index');
		Route::get('/create/vendor','VendorController@create');
		Route::post('/add/vendor','VendorController@store');
		Route::get('/edit/vendor/{id}','VendorController@edit');
		Route::delete('/delete/vendor/{id}','VendorController@destroy');
		//customer info
		Route::get('/customerinfo/list','CompanyConfigurController@index');
		Route::get('/create/customerinfo','CompanyConfigurController@create');
		Route::post('/add/customerinfo','CompanyConfigurController@store');
		Route::get('/edit/customerinfo/{id}','CompanyConfigurController@edit');
		Route::delete('/delete/customerinfo/{id}','CompanyConfigurController@destroy');
		Route::get('/dashboard_data','DashboardController@index');
	});
});

function posRoute() {
	// Inventory Category
	Route::get('/inventory_category/list','InventoryCategoryController@index');
	Route::get('/inventory_category/create','InventoryCategoryController@create');
	Route::post('/inventory_category/add','InventoryCategoryController@store');
	Route::get('/inventory_category/edit/{id}','InventoryCategoryController@edit');
	Route::delete('/inventory_category/delete/{id}','InventoryCategoryController@destroy');
	// Inventory Item
	Route::get('/inventory_item/list','InventoryItemController@index');
	Route::get('/inventory_item/create','InventoryItemController@create');
	Route::post('/inventory_item/add','InventoryItemController@store');
	Route::get('/inventory_item/edit/{id}','InventoryItemController@edit');
	Route::delete('/inventory_item/delete/{id}','InventoryItemController@destroy');
	// Inventory Receive
	Route::get('/inventory_receive/list','InventoryReceiveController@index');
	Route::get('/inventory_receive/create','InventoryReceiveController@create');
	Route::post('/inventory_receive/add','InventoryReceiveController@store');
	Route::get('/inventory_receive/edit/{id}','InventoryReceiveController@edit');
	Route::delete('/inventory_receive/delete/{id}','InventoryReceiveController@destroy');
	Route::get('/inventory_receive/view','InventoryReceiveController@view');
	//vendor
	Route::get('/vendor/list','VendorController@index');
	Route::get('/create/vendor','VendorController@create');
	Route::post('/add/vendor','VendorController@store');
	Route::get('/edit/vendor/{id}','VendorController@edit');
	Route::delete('/delete/vendor/{id}','VendorController@destroy');
	//customer
	Route::get('/customer/list','customerController@index');
	Route::get('/create/customer','customerController@create');
	Route::post('/add/customer','customerController@store');
	Route::get('/edit/customer/{id}','customerController@edit');
	Route::delete('/delete/customer/{id}','customerController@destroy');

	Route::get('/pos/list','InventoryPosController@index');
	Route::get('pos/create','InventoryPosController@create');
	Route::post('/pos/add','InventoryPosController@store');
	Route::get('/pos/pos_data','InventoryPosController@posValue');
	Route::get('/invoicehistory/pos/pos_data','InventoryPosController@posValue');

  	Route::get('/view_statement/list','InventoryArViewController@index');
  	Route::get('/view_statement/edit/{id}','InventoryArViewController@edit');
  	Route::get('/post_ar_view_statement/edit/{id}','InventoryArViewController@post_ar_view_statement');
	Route::get('/post_ar_payment/list','InventoryArPostController@index');
	Route::post('/post_ar_payment/edit','InventoryArPostController@edit');
	//Inventory Report
	Route::get('/geilist','ItemReportController@create');
	Route::get('/geilist/report','ItemReportController@itemList');
	Route::get('/bestseller/report','ItemReportController@bestSeller');
	Route::get('/worstseller/report','ItemReportController@worstSeller');
	Route::get('/itemorder/report','ItemReportController@intemOnOrder');
	Route::get('/belowreorder/report','ItemReportController@itemBelowReorder');
	Route::get('/pricelist/report','ItemReportController@pricelist');
	Route::get('/physicalinventory/report','ItemReportController@physicalInventory');
	Route::get('/valuebycatogory/report','ItemReportController@itemValueByCategory');
	Route::get('/valuebyvendor/report','ItemReportController@itemValueByVendor');
	Route::get('/invetorysales/report','ItemReportController@iventorySales');
	Route::get('/invoicehistory/report','ItemReportController@invoiceHistory');
	Route::get('/accounts-income/report','AccountsReportController@incomeReport');
	Route::get('/accounts-paytype/report','AccountsReportController@paymentType');
	Route::get('/sales-tex/report','AccountsReportController@salesTex');
	Route::get('/account-summery/report','AccountsReportController@accountSummery');
	Route::get('/account-information/report','AccountsReportController@accountingInformationReport');
	Route::get('/ar-asing/report','ItemReportController@arAsingReport');
	//Inventory Report
	//customer report
	Route::get('/customer/report','CustomerReportController@customerList');
	//customer report
	//vendor report
	Route::get('/vendor/report','VendorReportController@vendorList');
	//vendor report
	//salesperson report
	Route::get('/getsalespersons/report','SlaespersonReportController@index');
	Route::get('/salesperson/report','SlaespersonReportController@salespersonList');
	Route::get('/salespersonsales/report','SlaespersonReportController@getSalesperson');
	//salesperson report
	Route::get('/dashboard_data','DashboardController@index');
}

Route::group(['middleware' => ['auth.user'],'prefix' => 'user', 'namespace' => 'user'], function(){
	Route::post('/passwordchange','ChangePasswordController@passwordChange');
	Route::get('/passwordchangeForm','ChangePasswordController@changePasswordView')->name('changePasswor');
	Route::post('/passwordchangeAction','ChangePasswordController@passwordChangeAction')->name('changePassworAction');
});

Route::get('/home', 'HomeController@index')->name('home');
