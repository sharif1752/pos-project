@include('includs.masterheader')

<style>
	.user-password-change.login .box{
		width: 500px;
	}
	.user-password-change.login .box .input-icon input{
		
	}
</style>

<body >
    <div class="login user-password-change">
		<!-- Logo -->
		<div class="logo">
			<img src="{{asset('images/tmss-logo.png')}}">
		</div>
		<!-- /Logo -->

		<!-- Login Box -->
		<div class="box">
			<div class="content">
				<!-- Login Formular -->
				<h3 class="form-title">Change Password</h3>
				@if (session('status'))
				    @if(session('status') == 'false')
					    <div class="alert alert-danger">
					        Old password id wrong
					    </div>
					@else
						<div class="alert alert-success">
					        Password is successfully changed
					    </div>
					@endif
				@endif
				<form class="form-vertical login-form" method="POST" action="{{ route('changePassworAction') }}">
					 @csrf
					<!-- Title -->

					<!-- Error Message -->
					<div class="alert fade in alert-danger" style="display: none;">
						<i class="icon-remove close" data-dismiss="alert"></i>
						Enter any username and password.
					</div>

					<!-- Input Fields -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label class="control-label">Old Password<span class="required">*</span></label>
							</div>
							<div class="col-md-8">
								<div class="input-icon">
									<i class="icon-lock"></i>
									<input id="oldpassword" type="password" class="form-control @error('oldpassword') is-invalid @enderror" name="oldpassword" value="{{ old('oldpassword') }}" autocomplete="oldpassword" autofocus  placeholder="Old Password" />

									@error('oldpassword')
			                            <span class="text-danger" role="alert">
			                                <strong>{{ $message }}</strong>
			                            </span>
			                        @enderror
		                    	</div>
	                        </div>
	                    </div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label class="control-label">New Password<span class="required">*</span></label>
							</div>
							<div class="col-md-8">
								<div class="input-icon">
									<i class="icon-lock"></i>
									<input id="newpassword" type="password" class="form-control @error('newpassword') is-invalid @enderror" name="newpassword" autocomplete="newpassword" autofocus  placeholder="New Password" />

									@error('newpassword')
			                            <span class="text-danger" role="alert">
			                                <strong>{{ $message }}</strong>
			                            </span>
			                        @enderror
		                    	</div>
	                        </div>
                        </div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<label class="control-label">Confirm Password<span class="required">*</span></label>
							</div>
							<div class="col-md-8">
								<div class="input-icon">
									<i class="icon-lock"></i>
									<input id="confirmpassword" type="password" class="form-control @error('confirmpassword') is-invalid @enderror" name="confirmpassword"  autocomplete="confirmpassword" autofocus  placeholder="Confirm Password" />

									@error('confirmpassword')
			                            <span class="text-danger" role="alert">
			                                <strong>{{ $message }}</strong>
			                            </span>
			                        @enderror
		                    	</div>
	                        </div>
                        </div>
					</div>
					<!-- /Input Fields -->

					<!-- Form Actions -->
					<div class="form-actions">
						<a class="submit btn btn-primary pull-right" href="{{url(route('user.home'))}}">
							Go To Root <i class="icon-angle-right"></i>
						</a>
						<button id="password_submit" type="submit" class="submit btn btn-primary pull-right">
							Submit <i class="icon-angle-right"></i>
						</button>
					</div>
				</form>
				<!-- /Login Formular -->
			</div> <!-- /.content -->

			<!-- Forgot Password Form -->
			<div class="inner-box">
				<div class="content">
					<!-- Close Button -->
					<i class="icon-remove close hide-default"></i>

					<!-- Link as Toggle Button -->
					<a href="#" class="forgot-password-link">Change Password</a>

					<!-- Forgot Password Formular -->
					<form class="form-vertical forgot-password-form hide-default" action="login.html" method="post">
						<!-- Input Fields -->
						<div class="form-group">
							<!--<label for="email">Email:</label>-->
							<div class="input-icon">
								<i class="icon-envelope"></i>
								<input type="text" name="email" class="form-control" placeholder="Enter email address" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email." />
							</div>
						</div>
						<!-- /Input Fields -->

						<button type="submit" class="submit btn btn-default btn-block">
							Reset your Password
						</button>
					</form>
					<!-- /Forgot Password Formular -->

					<!-- Shows up if reset-button was clicked -->
					<div class="forgot-password-done hide-default">
						<i class="icon-ok success-icon"></i> <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
						<span>Great. We have sent you an email.</span>
					</div>
				</div> <!-- /.content -->
			</div>
			<!-- /Forgot Password Form -->
		</div>
		<!-- /Login Box -->
	</div>
</body>

@include('includs.masterfooter')

<script>
	$(document).ready(function(){

		$('#password_submit').prop('disabled', true);

		/*$('#confirmpassword').keyup(function () {
            if ($('#newpassword').attr('value') == $('#confirmpassword').attr('value')) {
                $('#password_submit').prop('disabled', true);
                console.log("true");
            } else {
                $('#password_submit').prop('disabled', false);
                console.log("flase");
            }
        });*/

        $('#confirmpassword').keyup(function() {
		    if ($('#newpassword').val() === $('#confirmpassword').val()) {
		    	$('#password_submit').prop('disabled', false);
		    	console.log("true");
		    } else { 
		    	$('#password_submit').prop('disabled', true);
		    	console.log("flase");
		    }
		});

	});
</script>