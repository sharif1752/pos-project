@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    #header-fixed { 
        position: fixed; 
        top: 0px; display:none;
        background-color:white;
    }


    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="1100" id="table-1">
                @if($period_status > 0)
    			    <thead class="header">
    			        @php $colspan=13; @endphp
    			        @include("report.reportTblHeader")
                        @if(!empty($id) && $colum == "category_id")
                            <tr>
                                <th colspan="14" class="tbc">{{$category_info->category_name}}</th>
                            </tr>
                        @else
                            @if(!empty($id))
                                <tr>
                                    <th colspan="14" class="tbc">{{$vendor_info->vendor_name}}</th>
                                </tr>
                            @endif
                        @endif
                        <tr>
                            <th colspan="2" class="tbc"></th>
                            <th colspan="4" class="tbc">{{$priod}}</th>
                        </tr>
    			        <tr>
                            <th class="tbc" style="width: 191px;">Item #</th>
                            <th class="tbc" style="width: 417px;">Description</th>
                            <th class="tbc" style="width: 108px;">Qty Sold</th>
                            <th class="tbc" style="width: 128px;">Sales</th>
                            <th class="tbc" style="width: 111px;">Avg Cost</th>
                            <th class="tbc">Profit</th>
                        </tr>
    			    </thead>
    			    <tbody>
                        @php
                            $total_qty_sold=0;
                            $total_sold=0;
                            $total_Avg_cost=0;
                            $total_Profit=0;
                        @endphp
                        @if(count($sales_list) > 0)
                            @foreach($sales_list as $result)
                                @php 
                                    $product_id=$inventory_list[$result->product_id];
                                    if($priod == 'Daily'){
                                        $profit = $product_id->total_profit_a;
                                    }
                                    elseif($priod == 'Monthly'){
                                        $profit = $product_id->total_profit_b;
                                    }
                                    else{
                                        $profit = $product_id->total_profit_c;
                                    }
                                @endphp
                                <tr>
                                    <td class="tbc">{{$product_id->inv_code}}</td>
                                    <td class="tbc">{{$product_id->inv_product_name}}</td>
                                    <td class="tbc text-right">{{$result->total_qty}}</td>
                                    <td class="tbc text-right">{{number_format($result->total_sales,2)}}</td>
                                    <td class="tbc text-right">{{$product_id->avg_cost}}</td>
                                    <td class="tbc text-right">{{number_format($profit,2)}}</td>
                                </tr>
                                @php
                                    $total_qty_sold+=$result->total_qty;
                                    $total_sold+=$result->total_sales;
                                    $total_Avg_cost+=$product_id->avg_cost;
                                    $total_Profit+=$profit;
                                @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="14" class="tbc">No Data Found</td>
                            </tr>
                        @endif
    			    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tbc" colspan="2">Grand Total</th>
                            <th class="tbc text-right" colspan="">{{$total_qty_sold}}</th>
                            <th class="tbc text-right" colspan="">{{number_format($total_sold,2)}}</th>
                            <th class="tbc text-right" colspan="">{{$total_Avg_cost}}</th>
                            <th class="tbc text-right" colspan="">{{number_format($total_Profit,2)}}</th>
                        </tr>
                    </tfoot>
                @else
                     <thead class="header">
                        @php $colspan=13; @endphp
                        @include("report.reportTblHeader")
                        @if(!empty($id) && $colum == "category_id")
                            <tr>
                                <th colspan="14" class="tbc">{{$category_info->category_name}}</th>
                            </tr>
                        @else
                            @if(!empty($id))
                                <tr>
                                    <th colspan="14" class="tbc">{{$vendor_info->vendor_name}}</th>
                                </tr>
                            @endif
                        @endif
                        <tr>
                            <th class="tbc" colspan="2"></th>
                            <th class="tbc" colspan="4">Dalily</th>
                            <th class="tbc" colspan="4">Monthly</th>
                            <th class="tbc" colspan="4">Year-To-Date</th>
                        </tr>
                        <tr>
                            <th  class="tbc" style="width: 101px;">Item #</th>
                            <th  class="tbc" style="width: 214px;">Description</th>
                            <th  class="tbc" style="width: 53px;">Qty Sold</th>
                            <th  class="tbc" style="width: 32px;">Sales</th>
                            <th  class="tbc" style="width: 54px;">Avg Cost</th>
                            <th  class="tbc" style="width: 38px;">Profit</th>
                            <th  class="tbc" style="width: 59px;">Qty Sold</th>
                            <th  class="tbc" style="width: 68px;">Sales</th>
                            <th  class="tbc" style="width: 70px;">Avg Cost</th>
                            <th  class="tbc" style="width: 61px;">Profit</th>
                            <th  class="tbc" style="width: 69px;">Qty Sold</th>
                            <th  class="tbc" style="width: 80px;">Sales</th>
                            <th  class="tbc" style="width: 69px;">Avg Cost</th>
                            <th  class="tbc" style="width: 72px;">Profit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $daily_total_qty_sold=0;
                            $daily_total_sold=0;
                            $daily_total_Avg_cost=0;
                            $daily_total_Profit=0;
                            $monthly_total_qty_sold=0;
                            $monthly_total_sold=0;
                            $monthly_total_Avg_cost=0;
                            $monthly_total_Profit=0;
                            $yearly_total_qty_sold=0;
                            $yearly_total_sold=0;
                            $yearly_total_Avg_cost=0;
                            $yearly_total_Profit=0;
                        @endphp
                        @if(count($sales_list) > 0)
                            @foreach($sales_list as $result)
                                <tr>
                                    <td class="tbc">{{$result->inv_code}}</td>
                                    <td class="tbc">{{$result->inv_product_name}}</td>
                                    <td class="tbc text-right">{{$result->daily_sold_qty}}</td>
                                    <td class="tbc text-right">{{number_format($result->daily_total_sale,2)}}</td>
                                    <td class="tbc text-right">{{$result->daily_total_avg_cost}}</td>
                                    <td class="tbc text-right">{{number_format($result->daily_total_profit,2)}}</td>
                                    <td class="tbc text-right">{{$result->monthly_sold_qty}}</td>
                                    <td class="tbc text-right">{{number_format($result->monthly_total_sale,2)}}</td>
                                    <td class="tbc text-right">{{$result->monthly_total_avg_cost}}</td>
                                    <td class="tbc text-right">{{number_format($result->monthly_total_profit,2)}}</td>
                                    <td class="tbc text-right">{{$result->yearly_sold_qty}}</td>
                                    <td class="tbc text-right">{{number_format($result->yearly_total_sale,2)}}</td>
                                    <td class="tbc text-right">{{$result->yearly_total_avg_cost}}</td>
                                    <td class="tbc text-right">{{number_format($result->yearly_total_profit,2)}}</td>
                                </tr>
                                @php
                                    $daily_total_qty_sold+=$result->daily_sold_qty;
                                    $daily_total_sold+=$result->daily_total_sale;
                                    $daily_total_Avg_cost+=$result->daily_total_avg_cost;
                                    $daily_total_Profit+=$result->daily_total_profit;
                                    $monthly_total_qty_sold+=$result->monthly_sold_qty;
                                    $monthly_total_sold+=$result->monthly_total_sale;
                                    $monthly_total_Avg_cost+=$result->monthly_total_avg_cost;
                                    $monthly_total_Profit+=$result->monthly_total_profit;
                                    $yearly_total_qty_sold+=$result->yearly_sold_qty;
                                    $yearly_total_sold+=$result->yearly_total_sale;
                                    $yearly_total_Avg_cost+=$result->yearly_total_avg_cost;
                                    $yearly_total_Profit+=$result->yearly_total_profit;
                                @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="14" class="tbc">No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tbc" colspan="2">Grand Total</th>
                            <th class="tbc text-right">{{number_format($daily_total_qty_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($daily_total_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($daily_total_Avg_cost,2)}}</th>
                            <th class="tbc text-right">{{number_format($daily_total_Profit,2)}}</th>
                            <th class="tbc text-right">{{number_format($monthly_total_qty_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($monthly_total_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($monthly_total_Avg_cost,2)}}</th>
                            <th class="tbc text-right">{{number_format($monthly_total_Profit,2)}}</th>
                            <th class="tbc text-right">{{number_format($yearly_total_qty_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($yearly_total_sold,2)}}</th>
                            <th class="tbc text-right">{{number_format($yearly_total_Avg_cost,2)}}</th>
                            <th class="tbc text-right">{{number_format($yearly_total_Profit,2)}}</th>
                        </tr>
                    </tfoot>
                @endif
			</table>
            <table width="1100" id="header-fixed">

            </table>
@include("report.reportFooter")
