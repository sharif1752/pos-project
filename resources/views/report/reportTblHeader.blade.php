<tr>
    <th colspan="{{$colspan}}"><p class="company_name">TMSS</p></th>
</tr>
<tr>
    <th colspan="{{$colspan}}"><p class="title" style="font-size: 16px;"><strong>Point Of Sales</strong></p></th>
</tr>
<tr>
    <th colspan="{{$colspan}}">
    	<p class="title">
	    	<strong>
	    		{{$report_name}} @if(isset($shorting_name)){{$shorting_name}}@endif
			</strong>
		</p>
	</th>
</tr>

<tr>
    <th colspan="{{$colspan}}"><p class="title" style="font-size: 14px;"><strong>{{$report_title}}</RP></strong></p></th>
</tr>
<tr>
    <th colspan="{{$colspan}}"><p class="title" style="font-size: 14px;"><strong>{{$reportDate}}</strong></p></th>
</tr>
<tr>
    <th colspan="{{$colspan}}" class="mid-space"></th>
</tr>