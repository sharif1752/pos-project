@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")

			        <tr>
			            <th  class="tbc">Item #</th>
			            <th  class="tbc">Description</th>
                        <th  class="tbc">Qty On Hand</th>
                        <th  class="tbc">Last Cost</th>
                        <th  class="tbc">Price A</th>
                        <th  class="tbc">Category</th>
                        <th  class="tbc">Vendor</th>
                        <th  class="tbc">Last Date Sold</th>
			            <th  class="tbc">Year To Date sales</th>
			        </tr>
			    </thead>
			    <tbody>
                    @php $total_qty=0; @endphp
                    @if(count($reports_result) > 0)
    			        @foreach($reports_result as $result)
                            @php $total_qty= $total_qty + $result->qty_hand; @endphp
        			        <tr>
                                <td class="tbc ">{{$result->inv_code}}</td>
                                <td class="tbc text-left">{{$result->inv_product_name}}</td>
                                <td class="tbc text-right">{{$result->qty_hand}}</td>
                                <td class="tbc text-right">{{$result->last_cost}}</td>
                                <td class="tbc text-right">{{$result->price_a}}</td>
                                <td class="tbc text-left">{{$result->category_name}}</td>
                                <td class="tbc text-left">{{$result->vendor_name}}</td>
                                <td class="tbc ">{{$result->last_date_sold}}</td>
        			            <td class="tbc  text-right">{{number_format($result->total_revenue_c,2)}}</td>

        			        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="tbc">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
                <tfoot>
                    <tr>
                        <th class="tbc" colspan="2">Grand Total</th>
                        <th class="tbc  text-right" colspan="">{{$total_qty}}</th>
                        <th class="tbc" colspan="5"></th>
                        <th class="tbc" colspan=""></th>
                    </tr>
                </tfoot>
			</table>
@include("report.reportFooter")
