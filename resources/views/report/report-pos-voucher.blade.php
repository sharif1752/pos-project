@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody {
        border: 1px solid black;
        border-left: none;
        border-right: none;
    }
    tbody tr{
        border-bottom: 1px solid #736666;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }

    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }
</style>
			<table width="55%" style="margin: auto;">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")
                    <tr>
                        <th  class=" text-left" colspan="5">
                            Served By: {{$sales_perrson_name->name ?? "unknown"}}
                        </th>
                    </tr>
			    </thead>
			    <tbody>
                    <tr style="background: #e3e3e3">
                        <td class=" text-left">Code #</td>
                        <td >Items</td>
                        <td >Qty</td>
                        <td >Price</td>
                        <td >Total</td>
                    </tr>
                    @if(count($data_value) > 0)
                        @foreach($data_value as $result)
                             @php $total=$result->product_qty*$result->product_price; 
                             @endphp
                            <tr>
                                <td class=" text-left">{{$result->inv_code}}</td>
                                <td class="text-center">{{$result->inv_product_name}}</td>
                                <td>{{$result->product_qty}}</td>
                                <td>{{$result->product_price}}</td>
                                <td class="text-center">{{number_format($total,2)}}</td>
                            </tr>
                        @endforeach
                            <tr style="border-bottom: none;">
                                <td class=" text-left" colspan="4">Sub Total:</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->sub_total}}</td>
                            </tr>
                            <tr style="border-bottom: none;">
                                <td class=" text-left" colspan="4">Sales Tax:</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->sales_tax}}</td>
                            </tr>
                            <tr>
                                <td  class=" text-left" colspan="4">Discount:</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->total_discount}}</td>
                            </tr>
                            <tr>
                                <td  class=" text-left" colspan="4">Net Amount:</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->sub_total + $fetch_data->sales_tax}}</td>
                            </tr>
                            <tr style="border-bottom: none;">
                                <td  class=" text-left" colspan="4">Pay Type</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->pay_type}}</td>
                            </tr>
                           <!--  <tr>
                                <td  class=" text-left" colspan="4">Paid Amount:</td>
                                <td  class=" text-center" colspan="1">{{$fetch_data->total_pay}}</td>
                            </tr> -->
                    @else
                        <tr>
                            <td colspan="9">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
			</table>
@include("report.reportFooter")
