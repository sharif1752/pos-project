@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")
			        <tr>
			            <th  class="tbc">Customer</th>
			            <th  class="tbc">Current</th>
                        <th  class="tbc">30 to 60 Days</th>
                        <th  class="tbc">60 to 90 Days</th>
			            <th  class="tbc">90+ Days</th>
			        </tr>
			    </thead>
			    <tbody>
                    @if(count($due_info) > 0)
                        @foreach($due_info as $result)
                            <tr>
                                <td class="tbc">{{$result['customer']}}</td>
                                <td class="tbc text-right">{{number_format($result['current'],2)}}</td>
                                <td class="tbc text-right">{{$result['days_30_60']}}</td>
                                <td class="tbc text-right">{{$result['days_60_90']}}</td>
                                <td class="tbc text-right">{{$result['days_plus_90']}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="tbc">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
			</table>
@include("report.reportFooter")
