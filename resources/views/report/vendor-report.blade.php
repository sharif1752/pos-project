@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")
			        <tr>
			            <th  class="tbc">Customer #</th>
			            <th  class="tbc">Name</th>
                        <th  class="tbc">Phone</th>
                        <th  class="tbc">Last Date Purchased</th>
			            <th  class="tbc">Year-To-Date Vendor Sales</th>
			        </tr>
			    </thead>
			    <tbody>
                    @if(count($reports_result) > 0)
                        @foreach($reports_result as $result)
                            <tr>
                                <td class="tbc">{{$result->vendor_number}}</td>
                                <td class="tbc text-left">{{$result->vendor_name}}</td>
                                <td class="tbc">{{$result->vendor_phone}}</td>
                                <td class="tbc">{{$result->last_date_product_sold}}</td>
                                <td class="tbc text-right">{{number_format($result->total_vendor_sales_3,2)}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="tbc">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
			</table>
@include("report.reportFooter")
