@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody {
        border: 1px solid black;
        border-left: none;
        border-right: none;
    }
    tbody tr{
        border-bottom: 1px solid black;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }

    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }
</style>
			<table width="55%" style="margin: auto;">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")
                    <tr>
                        <th  class=" text-left" colspan="5">Recieve Date: {{$recieve_data->date}}</th>
                    </tr>
                    <tr>
                        <th  class=" text-left" colspan="5">Invoice #: 00{{$recieve_data->invoice_number}}</th>
                    </tr>
			    </thead>
			    <tbody>
                    <tr style="background: #e3e3e3">
                        <td >Number</td>
                        <td >Description</td>
                        <td >Last Cost</td>
                        <td >Recieved</td>
                        <td >Cost Of Goods</td>
                    </tr>
                    @if(count($recieve_data_details) > 0)
                        @foreach($recieve_data_details as $result)
                             @php $total=$result->receive_qty*$result->receive_cost; @endphp
                            <tr>
                                <td>{{$result->inv_code}}</td>
                                <td class="text-center">{{$result->inv_product_name}}</td>
                                <td>{{$result->receive_cost}}</td>
                                <td>{{$result->receive_qty}}</td>
                                <td class="text-center">{{number_format($total,2)}}</td>
                            </tr>
                        @endforeach
                            <tr style="border-bottom: none;">
                                <td  class=" text-left" colspan="3">Total Recived Quantity</td>
                                <td  class=" text-center" colspan="1">{{$recieve_data->total_item}}</td>
                                <td  class=" text-center" colspan="1">{{number_format($recieve_data->total_amount,2)}}</td>
                            </tr>
                    @else
                        <tr>
                            <td colspan="9">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
			</table>
@include("report.reportFooter")
