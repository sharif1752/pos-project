@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
                @if($invoice_status == 'invoice_number')
    			    <thead class="header">
    			        @php $colspan=13; @endphp
    			        @include("report.reportTblHeader")
    			        <tr>
    			            <th  class="tbc">Invoice #</th>
    			            <th  class="tbc">Invoice Date</th>
                            <th  class="tbc">Sales Person</th>
                            <th  class="tbc">Cusotmer Name</th>
                            <th  class="tbc">Invoice Total</th>
                            <th  class="tbc">Pay Type</th>
    			        </tr>
    			    </thead>
    			    <tbody>
                        @php $invoice_total=0; @endphp
                        @if(count($reports_result) > 0)
                            @foreach($reports_result as $result)
                                @php
                                    $sub_total=$result->sub_total + $result->sales_tax;
                                    $invoice_total+=$sub_total;
                                @endphp
                                <tr>
                                    <td class="tbc">
                                        <a href="pos/pos_data?id={{$result->id}}&date={{$result->date}}">{{$result->invoice_number}}</a>
                                    </td>
                                    <td class="tbc">{{$result->date}}</td>
                                    <td class="tbc text-left">
                                        @if(array_key_exists($result->created_by,$salesperson_list))
                                            {{$salesperson_list[$result->created_by]->name}}
                                        @endif
                                    </td>
                                    <td class="tbc text-left">
                                        @if(array_key_exists($result->customer_name,$csuromer_list))
                                            {{$csuromer_list[$result->customer_name]->customer_name}}
                                        @endif
                                    </td>
                                    <td class="tbc text-right">{{number_format($sub_total,2)}}</td>
                                    <td class="tbc">{{$result->pay_type}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="tbc">No Data Found</td>
                            </tr>
                        @endif
    			    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tbc" colspan="4">Grand Total</th>
                            <th class="tbc text-right" colspan="">{{number_format($invoice_total,2)}}</th>
                            <th class="tbc" colspan=""></th>
                        </tr>
                    </tfoot>
                @else
                    <thead class="header">
                        @php $colspan=13; @endphp
                        @include("report.reportTblHeader")
                        <tr>
                            <th  class="tbc">Invoice #</th>
                            <th  class="tbc">Invoice Date</th>
                            <th  class="tbc">Sales Person</th>
                            <th  class="tbc">Cusotmer Name</th>
                            <th  class="tbc">Invoice Total</th>
                            <th  class="tbc">Pay Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $invoice_total=0; @endphp
                        @if(count($reports_result) > 0)
                            @foreach($reports_result as $key => $value)
                                @if($invoice_status == 'customer_name')
                                    @if(array_key_exists($key,$csuromer_list))
                                        <tr>
                                            <td class="tbc" colspan="6">
                                                {{$csuromer_list[$key]->customer_name}} (#{{$csuromer_list[$key]->customer_number}})
                                            </td>
                                        </tr>
                                    @endif
                                @else
                                    <tr>
                                        <td class="tbc" colspan="6">
                                            {{$salesperson_list[$key]->name}} (#{{$salesperson_list[$key]->number}})
                                        </td>
                                    </tr>
                                @endif
                                @php $invoice_group_by_total=0; @endphp
                                @foreach($value as $result)
                                    @php
                                        $sub_total=$result->sub_total + $result->sales_tax;
                                        $invoice_group_by_total+=$sub_total;
                                    @endphp
                                    <tr>
                                        <td class="tbc">{{$result->invoice_number}}</td>
                                        <td class="tbc">{{$result->date}}</td>
                                        <td class="tbc text-left">{{$salesperson_list[$result->created_by]->name}}</td>
                                        <td class="tbc text-left">
                                            @if(array_key_exists($result->customer_name,$csuromer_list))
                                                {{$csuromer_list[$result->customer_name]->customer_name}}
                                            @endif
                                        </td>
                                        <td class="tbc text-right">{{number_format($sub_total,2)}}</td>
                                        <td class="tbc">{{$result->pay_type}}</td>
                                    </tr>
                                @endforeach
                                    @php $invoice_total+=$invoice_group_by_total; @endphp
                                    <tr>
                                        <td class="tbc" colspan="4">Total</td>
                                        <td class="tbc text-right">{{number_format($invoice_group_by_total,2)}}</td>
                                        <td class="tbc"></td>
                                    </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="tbc">No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tbc" colspan="4">Grand Total</th>
                            <th class="tbc text-right" colspan="">{{number_format($invoice_total,2)}} </th>
                            <th class="tbc" colspan=""></th>
                        </tr>
                    </tfoot>
                @endif
			</table>
@include("report.reportFooter")
