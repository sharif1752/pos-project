@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
			    <thead class="header">
			        @php $colspan=13; @endphp
			        @include("report.reportTblHeader")
			        <tr>
			            <th  class="tbc">Item #</th>
			            <th  class="tbc">Description</th>
                        <th  class="tbc">Qty On Hand</th>
                        <th  class="tbc">Average Cost</th>
                        <th  class="tbc">Item Value</th>
			            <th  class="tbc">Year To Date Revenue</th>
			        </tr>
			    </thead>
			    <tbody>
                    @php
                        $total_qty=0;
                        $item_value=0;
                        $total_revenue=0;
                        $g_total_qty=0;
                        $g_item_value=0;
                        $g_total_revenue=0;
                    @endphp
                    @if(count($reports_result) > 0)
                        @foreach($reports_result as $key => $result)
                            <tr>
                                @if($key)
                                    <td class="tbc" colspan="6" style="text-transform: uppercase;">{{$vendor_name[$key]->vendor_name}}</td>
                                @else
                                    <td class="tbc" colspan="6" style="text-transform: uppercase;">Unknown</td>
                                @endif
                            </tr>
                                @foreach($result as $value)
                                    @php
                                        $total_qty= $total_qty + $value->qty_hand;
                                        $item_value= $item_value +($value->qty_hand*$value->avg_cost);
                                        $total_revenue= $total_revenue + $value->total_revenue_c;
                                    @endphp
                                    <tr>
                                        <td class="tbc">{{$value->inv_code}}</td>
                                        <td class="tbc text-left">{{$value->inv_product_name}}</td>
                                        <td class="tbc text-right">{{$value->qty_hand}}</td>
                                        <td class="tbc text-right">{{$value->avg_cost}}</td>
                                        <td class="tbc text-right">{{$value->qty_hand*$value->avg_cost}}</td>
                                        <td class="tbc text-right">{{number_format($value->total_revenue_c,2)}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="tbc" colspan="2">Total</td>
                                    <td class="tbc text-right">{{$total_qty}}</td>
                                    <td class="tbc"></td>
                                    <td class="tbc text-right">{{$item_value}}</td>
                                    <td class="tbc text-right">{{number_format($total_revenue,2)}}</td>
                                </tr>
                                @php
                                    $g_total_qty += $total_qty;
                                    $g_item_value += $item_value;
                                    $g_total_revenue += $total_revenue;
                                    $total_qty=0;
                                    $item_value=0;
                                    $total_revenue=0;
                                @endphp
                        @endforeach
                        <tr>
                            <td class="tbc" colspan="2">Grand Total</td>
                            <td class="tbc text-right">{{$g_total_qty}}</td>
                            <td class="tbc"></td>
                            <td class="tbc text-right">{{$g_item_value}}</td>
                            <td class="tbc text-right">{{number_format($g_total_revenue,2)}}
                        </tr>
                    @else
                        <tr>
                            <td colspan="9" class="tbc">No Data Found</td>
                        </tr>
                    @endif
			    </tbody>
			</table>
@include("report.reportFooter")
