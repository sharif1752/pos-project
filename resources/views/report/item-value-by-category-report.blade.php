@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
                @if($item_list_type == 1)
    			    <thead class="header">
    			        @php $colspan=13; @endphp
    			        @include("report.reportTblHeader")
    			        <tr>
    			            <th  class="tbc">Item #</th>
    			            <th  class="tbc">Description</th>
                            <th  class="tbc">Qty On Hand</th>
                            <th  class="tbc">{{$shorting_name}}</th>
                            <th  class="tbc">Item Value</th>
    			            <th  class="tbc">Year To Date Revenue</th>
    			        </tr>
    			    </thead>
    			    <tbody>
                        @php
                            $total_qty=0;
                            $item_value=0;
                            $total_revenue=0;
                            $g_total_qty = 0;
                            $g_item_value = 0;
                            $g_total_revenue = 0;
                        @endphp
                        @if(count($reports_result) > 0)
                            @foreach($reports_result as $key => $result)
                                <tr>
                                    @if($key)
                                        <td class="tbc" colspan="6" style="text-transform: uppercase;">{{$category_name[$key]->category_name}}</td>
                                    @else
                                        <td class="tbc" colspan="6" style="text-transform: uppercase;">Unknown</td>
                                    @endif
                                </tr>
                                    @foreach($result as $value)
                                    @php
                                        $total_qty= $total_qty + $value->qty_hand;
                                        $item_value= $item_value+($value->qty_hand*$value->$value_by);
                                        $total_revenue= $total_revenue + $value->total_revenue_c;
                                    @endphp
                                    <tr>
                                        <td class="tbc">{{$value->inv_code}}</td>
                                        <td class="tbc text-left">{{$value->inv_product_name}}</td>
                                        <td class="tbc text-right">{{$value->qty_hand}}</td>
                                        <td class="tbc text-right">{{$value->$value_by}}</td>
                                        <td class="tbc text-right">{{$value->qty_hand*$value->$value_by}}</td>
                                        <td class="tbc text-right">{{number_format($value->total_revenue_c,2)}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="tbc" colspan="2">Total</td>
                                        <td class="tbc text-right">{{$total_qty}}</td>
                                        <td class="tbc"></td>
                                        <td class="tbc text-right">{{$item_value}}</td>
                                        <td class="tbc text-right">{{number_format($total_revenue,2)}}</td>
                                    </tr>
                                    @php
                                        $g_total_qty += $total_qty;
                                        $g_item_value += $item_value;
                                        $g_total_revenue += $total_revenue;
                                        $total_qty=0;
                                        $item_value=0;
                                        $total_revenue=0;
                                    @endphp
                            @endforeach
                            <tr>
                                <td class="tbc" colspan="2">Grand Total</td>
                                <td class="tbc text-right">{{$g_total_qty}}</td>
                                <td class="tbc"></td>
                                <td class="tbc text-right">{{$g_item_value}}</td>
                                <td class="tbc text-right">{{number_format($g_total_revenue,2)}}
                            </tr>
                        @else
                            <tr>
                                <td colspan="9" class="tbc">No Data Found</td>
                            </tr>
                        @endif
    			    </tbody>
                @else
                    <thead class="header">
                        @php $colspan=13; @endphp
                        @include("report.reportTblHeader")
                        <tr>
                            <th class="tbc" colspan="2"></th>
                            <th class="tbc" colspan="3">Revenue</th>
                            <th class="tbc" colspan="3">Profit</th>
                        </tr>
                        <tr>
                            <th  class="tbc">Category Name</th>
                            <th  class="tbc">Value By {{$value_by}}</th>
                            <th  class="tbc">Daily</th>
                            <th  class="tbc">Monthly</th>
                            <th  class="tbc">Year-To-Date</th>
                            <th  class="tbc">Daily</th>
                            <th  class="tbc">Monthly</th>
                            <th  class="tbc">Year-To-Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($reports_result) > 0)
                            @php
                                $daily_r=0;
                                $monthly_r=0;
                                $yearly_r=0;
                                $daily_p=0;
                                $monthly_p=0;
                                $yearly_p=0;
                                $value_average_cost = 0;
                                $total_daily_r = 0;
                                $total_monthly_r = 0;
                                $total_yearly_r = 0;
                                $total_daily_p = 0;
                                $total_monthly_p = 0;
                                $total_yearly_p = 0;
                                $total_value_average_cost = 0;
                            @endphp
                            @foreach($reports_result as $key => $result)
                                <tr>
                                    @foreach($result as $value)
                                        @php
                                            $daily_r= $daily_r + $value->total_revenue_a;
                                            $monthly_r= $monthly_r + $value->total_revenue_b;
                                            $yearly_r= $yearly_r + $value->total_revenue_c;
                                            $daily_p= $daily_p + $value->total_profit_a;
                                            $monthly_p= $monthly_p + $value->total_profit_b;
                                            $yearly_p= $yearly_p + $value->total_profit_c;
                                            $value_average_cost +=($value->$value_by * $value->qty_hand);
                                        @endphp
                                    @endforeach
                                    @if($key)
                                        <td class="tbc">{{$category_name[$key]->category_name}}</td>
                                    @else
                                        <td class="tbc">Unknown</td>
                                    @endif
                                    <td class="tbc">{{$value_average_cost}}</td>
                                    <td class="tbc">{{$daily_r}}</td>
                                    <td class="tbc">{{$monthly_r}}</td>
                                    <td class="tbc">{{$yearly_r}}</td>
                                    <td class="tbc">{{$daily_p}}</td>
                                    <td class="tbc">{{$monthly_p}}</td>
                                    <td class="tbc">{{$yearly_p}}</td>
                                </tr>
                                @php
                                    $total_daily_r = $total_daily_r + $daily_r;
                                    $total_monthly_r = $total_yearly_r + $monthly_r;
                                    $total_yearly_r = $total_yearly_r +  $yearly_r;
                                    $total_daily_p = $total_daily_p + $daily_p;
                                    $total_monthly_p = $total_yearly_p + $monthly_p;
                                    $total_yearly_p = $total_yearly_p +  $yearly_p;
                                    $total_value_average_cost += $value_average_cost;
                                @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="tbc">No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tbc">Grand Total</th>
                            <th class="tbc">{{$total_value_average_cost}}</th>
                            <th class="tbc">{{$total_daily_r}}</th>
                            <th class="tbc">{{$total_monthly_r}}</th>
                            <th class="tbc">{{$total_yearly_r}}</th>
                            <th class="tbc">{{$total_daily_p}}</th>
                            <th class="tbc">{{$total_monthly_p}}</th>
                            <th class="tbc">{{$total_yearly_p}}</th>
                        </tr>
                    </tfoot>
                @endif

			</table>
@include("report.reportFooter")
