@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }
    .slaes-person-header{
        border-bottom: 1px solid #000;
        background: #e3e3e3;
    }

    .slaes-person-header th{
        padding: 10px 0px
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table style="margin-bottom: 30px;" width="100%">
    			    <thead class="header">
    			        @php $colspan=13; @endphp
    			        @include("report.reportTblHeader")
                        <tr class="">
                            <th  class=" text-left">
                                <h4>
                                    Income
                                <h4>
                            </th>
                        </tr>
    			        <tr class="slaes-person-header">
                            <th></th>
                            @if($company_config->account_header_a != 0)
                            <th>Shift</th>
                            @endif
                            @if($company_config->account_header_b != 0)
                            <th>Daily</th>
                            @endif
                            @if($company_config->account_header_c != 0)
                            <th>Mothly</th>
                            @endif
                            @if($company_config->account_header_d != 0)
                            <th>Year-To-Date</th>
                            @endif
    			        </tr>
    			    </thead>
    			    <tbody>
                        @if($account_collection)
                            @foreach($account_collection as $result)
                                <tr>
                                    <td class=" text-left" width="20%">{{$result['title']}}</td>
                                    @if($company_config->account_header_a != 0)
                                    <td class="">
                                        {{$result['shift']}}{{$result['title']=='Total Invoice'?'':' TK'}}
                                    </td>
                                    @endif
                                    @if($company_config->account_header_b != 0)
                                    <td class="">
                                        {{$result['daily']}}{{$result['title']=='Total Invoice'?'':' TK'}}
                                    </td>
                                    @endif
                                    @if($company_config->account_header_c != 0)
                                    <td class="">
                                        {{$result['monthly']}}{{$result['title']=='Total Invoice'?'':' TK'}}
                                    </td>
                                    @endif
                                    @if($company_config->account_header_d != 0)
                                    <td class="">
                                        {{$result['yearly']}}{{$result['title']=='Total Invoice'?'':' TK'}}
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            <tr style="font-weight: bold;border-top: 1px solid #000;margin-top: 15px;">
                                <td class=" text-left">Grand Total</td>
                                @if($company_config->account_header_a != 0)
                                <td>TK {{$grand_total_shift}}</td>
                                @endif
                                @if($company_config->account_header_b != 0)
                                <td>TK {{$grand_total_daily}}</td>
                                @endif
                                @if($company_config->account_header_c != 0)
                                <td>Tk {{$grand_total_monthly}}</td>
                                @endif
                                @if($company_config->account_header_d != 0)
                                <td>Tk {{$grand_total_yearly}}</td>
                                @endif
                            </tr>
                            <hr style="width: 100%;">
                            <tr>
                                <td class=" text-left">{{$total_account_receive['title']}}</td>
                                @if($company_config->account_header_a != 0)
                                <td>{{$total_account_receive['shift']}}</td>
                                @endif
                                @if($company_config->account_header_b != 0)
                                <td>{{$total_account_receive['daily']}}</td>
                                @endif
                                @if($company_config->account_header_c != 0)
                                <td>{{$total_account_receive['monthly']}}</td>
                                @endif
                                @if($company_config->account_header_d != 0)
                                <td>{{$total_account_receive['yearly']}}</td>
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td colspan="9" class="">No Data Found</td>
                            </tr>
                        @endif
    			    </tbody>
			</table>
            <table style="margin-bottom: 30px;" width="100%">
                    <thead class="header">
                        @php $colspan=13; @endphp
                        <tr class="">
                            <th  class=" text-left">
                                <h4>
                                    Payment
                                <h4>
                            </th>
                        </tr>
                        <tr class="slaes-person-header">
                            <th></th>
                            @if($company_config->account_header_a != 0)
                            <th>Shift</th>
                            @endif
                            @if($company_config->account_header_b != 0)
                            <th>Daily</th>
                            @endif
                            @if($company_config->account_header_c != 0)
                            <th>Mothly</th>
                            @endif
                            @if($company_config->account_header_d != 0)
                            <th>Year-To-Date</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if($accout_payment_type)
                            @foreach($accout_payment_type as $result)
                                <tr>
                                    <td class=" text-left" width="20%">{{$result['title']}}</td>
                                    @if($company_config->account_header_a != 0)
                                    <td class="">{{$result['shift']}} TK</td>
                                    @endif
                                    @if($company_config->account_header_b != 0)
                                    <td class="">{{$result['daily']}} TK</td>
                                    @endif
                                    @if($company_config->account_header_c != 0)
                                    <td class="">{{$result['monthly']}} TK</td>
                                    @endif
                                    @if($company_config->account_header_d != 0)
                                    <td class="">{{$result['yearly']}} TK</td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="">No Data Found</td>
                            </tr>
                        @endif
                    </tbody>
            </table>
            <table style="margin-bottom: 100px;" width="100%">
                <thead class="header">
                    @php $colspan=13; @endphp
                    <tr class="">
                        <th  class=" text-left">
                            <h4>
                                Salex Tex
                            <h4>
                        </th>
                    </tr>
                    <tr class="slaes-person-header">
                        <th></th>
                        @if($company_config->account_header_a != 0)
                        <th>Shift</th>
                        @endif
                        @if($company_config->account_header_b != 0)
                        <th>Daily</th>
                        @endif
                        @if($company_config->account_header_c != 0)
                        <th>Mothly</th>
                        @endif
                        @if($company_config->account_header_d != 0)
                        <th>Year-To-Date</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if($get_sales_tex_info)
                        @foreach($get_sales_tex_info as $result)
                            <tr>
                                <td class=" text-left" width="20%">{{$result['title']}}</td>
                                @if($company_config->account_header_a != 0)
                                <td class="">{{$result['shift']}} TK</td>
                                @endif
                                @if($company_config->account_header_b != 0)
                                <td class="">{{$result['daily']}} TK</td>
                                @endif
                                @if($company_config->account_header_c != 0)
                                <td class="">{{$result['monthly']}} TK</td>
                                @endif
                                @if($company_config->account_header_d != 0)
                                <td class="">{{$result['yearly']}} TK</td>
                                @endif
                            </tr>
                        @endforeach
                        <hr style="width: 100%;">
                        <tr style="font-weight: bold; border-top: 1px solid #000">
                            <td class=" text-left">Grand Total</td>
                            @if($company_config->account_header_a != 0)
                            <td>{{$get_sales_tex_info_shift}} TK</td>
                            @endif
                            @if($company_config->account_header_b != 0)
                            <td>{{$get_sales_tex_info_daily}} TK</td>
                            @endif
                            @if($company_config->account_header_c != 0)
                            <td>{{$get_sales_tex_info_monthly}} TK</td>
                            @endif
                            @if($company_config->account_header_d != 0)
                            <td>{{$get_sales_tex_info_yearly}} TK</td>
                            @endif
                        </tr>
                    @else
                        <tr>
                            <td colspan="9" class="">No Data Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <table style="margin-bottom: 30px;" width="100%">
                <thead class="header">
                    @php $colspan=13; @endphp
                    <tr class="">
                        <th  class=" text-left">
                            <h4>
                                Invoice Summery
                            <h4>
                        </th>
                    </tr>
                    <tr class="slaes-person-header">
                        <th>Invoice #</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Sales Person</th>
                        <th>Total</th>
                        <th>Pay Type</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($summery)>0)
                        @foreach($summery as $result)
                            <tr>
                                <td>{{$result['number']}}</td>
                                <td>{{$result['date']}}</td>
                                <td>{{$result['c_name']}}</td>
                                <td>{{$result['sp_name']}}</td>
                                <td>{{$result['total']}}</td>
                                <td>{{$result['paytype']}}</td>
                            </tr>
                        @endforeach
                        <hr>
                        <tr style="border-top: 1px solid #000">
                            <td class="text-left">Income Recieved</td>
                            <td colspan="4" class="text-right">{{$recieved_income}}</td>
                        </tr>
                         <tr>
                            <td class="text-left">Account Receiveable Sales</td>
                            <td colspan="4" class="text-right">{{$recieved_on_account}}</td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="9" class="">No Data Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>

@include("report.reportFooter")
