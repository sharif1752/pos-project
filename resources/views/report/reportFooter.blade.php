@if(!isset($excel) || !$excel)
			</div>
		</div>
    </body>
<script type="text/javascript" src="{{asset('melon/assets/js/libs/jquery-1.10.2.min.js')}}"></script>
    <script type="text/javascript">
        function printDocument() {
            window.print();
        }

        window.addEventListener('load', function () {
		   printDocument();
		})

        $(document).ready(function(){
            var tableOffset = $("#table-1").offset().top;
            var $header = $("#table-1 > thead").clone();
            var $fixedHeader = $("#header-fixed").append($header);

            $(window).bind("scroll", function() {
                var offset = $(this).scrollTop();
                
                if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                    $fixedHeader.show();
                }
                else if (offset <= tableOffset) {
                    $fixedHeader.hide();
                }
            });
        });
    </script>
</html>
@endif