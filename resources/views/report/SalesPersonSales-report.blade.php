@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }
    .slaes-person-header{
        border-bottom: 1px solid #000;
        background: #e3e3e3;
    }

    .slaes-person-header th{
        padding: 10px 0px
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
			<table width="100%">
                @if($id)
    			    <thead class="header">
    			        @php $colspan=13; @endphp
    			        @include("report.reportTblHeader")
                        <tr class="">
                            <th  class=" text-left">
                                <h4>
                                    Sales Person: {{$salespersons[$id]->name}}
                                <h4>
                            </th>
                        </tr>
    			        <tr class="slaes-person-header">
                            <th  class=" text-left"></th>
                            <th  class="">Daily</th>
                            <th  class="">Monthly</th>
    			            <th  class="">Yearly</th>
    			        </tr>
    			    </thead>
    			    <tbody>
                            @foreach($personsales as $result)
                                <tr>
                                    <td class=" text-left" width="20%">{{$result['title']}}</td>
                                    <td class="">{{$result['daily']}}</td>
                                    <td class="">{{$result['monthly']}}</td>
                                    <td class="">{{$result['yearly']}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="">No Data Found</td>
                            </tr>
    			    </tbody>
                @endif
			</table>
            <table width="100%">
                <thead class="header">
                    <tr>
                        <th colspan="2"  class="text-left">
                            <h4>Shiftwise Daily Sales</h4>
                        </th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($shifting_time as $key=>$val)
                            <tr width="100%">
                                <td colspan="2" class="text-center slaes-person-header">{{$get_time[$key]}}</td>
                            </tr>
                            @php
                                $total_sub = 0;
                                $total_invoice = 0;    
                                $total_due = 0;    
                                $total_cash = 0;    
                            @endphp
                            @foreach($val as $v)
                                @php
                                    $total_sub += $v->sub_total;
                                    $total_invoice++;
                                    if($v->total_due > 0){
                                        $total_due +=$v->total_due;
                                    }
                                    if($v->total_pay > 0){
                                        $total_cash +=$v->total_pay;
                                    }
                                @endphp
                            @endforeach
                            <tr>
                                <td class="text-left" width="20%">Total Invoice</td>
                                <td>{{$total_invoice}}</td>
                            </tr>
                            <tr>
                                <td class="text-left" width="20%">Total Sales</td>
                                <td>{{$total_sub}}</td>
                            </tr>
                            <tr>
                                <td class="text-left" width="20%">Cash</td>
                                <td>{{$total_cash}}</td>
                            </tr>
                            <tr>
                                <td class="text-left" width="20%">Account Receivable</td>
                                <td>{{$total_due}}</td>
                            </tr>
                            @php
                                $total_sub = 0;
                                $total_invoice = 0;    
                                $total_due = 0;    
                                $total_cash = 0;   
                            @endphp
                        @endforeach
                </tbody>
            </table>

@include("report.reportFooter")
