<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <!--<meta charset="utf-8">-->
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" href="{!! asset('public/css/google-font.css') !!}" />
        <link href="{{asset('melon/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('melon/assets/css/fontawesome/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('melon/assets/css/newfontawesome/font-awesome.min.css')}}">
        <script src="<?php echo url('resources/assets/themes/js/libs/jquery.min.js'); ?>"></script>
        <style type="text/css">
            @page {
                margin: 2%;
            }
            @media print {   
                .print_button {
                    display: none !important;
                }

                .print_hide {
                    display: none !important;
                }
               

                table td, table th {
                    font-size: 12px;
                }
            }

            body { 
                margin: 0; 
                padding: 0;
                background-color: white;
                font-family: 'Open Sans', sans-serif;
                font-weight: normal;
                font-size: 12px; 
                color: black;
            }
            .container {
                position: relative;
            }
            .header {
                text-align: center;
                font-family: 'Open Sans', sans-serif;
            }
            .print_button {
                position:absolute;
                top:30px;
                right:0px;
                padding-right: 16px;
            }
            .overflow {
                overflow-x: auto;
                padding: 0 1px;
            }
            .overflow.tableScrollOut{
                overflow-x: visible;
            }
            .company_name {
                font-family: 'Open Sans', sans-serif;
                text-align: center;
                font-size: 20px;
                font-weight: 600;
                margin-top: 20px;
                margin-bottom: -3px;
            }

            /*table tr td, table tr th {
                page-break-inside: avoid;
            }*/

            table td, table th {
                font-family: "Times New Roman", Times, serif;
                height:100%;
                font-weight:bold;
                font-size: 12px;
            }
            table th {
                text-align: center;
            }
            table th p {
                margin-bottom: 0;
            }
            table td {
                padding: 2px !important;
                text-align: right;
            }
            
            table div {
                width:100%;
                height:100%;
            }

            .title {
                font-size: 18px;
                font-family: 'the times roman;'
            }
            .title br {
                margin-bottom: 10px;
            }

            .border-none{
                border: 0px!important;
            }
            
            .txt-normal{
                font-weight:normal!important;
            }
            
            .fill{
                width:100%;
                height:100%;
                display: table;
                border:1px solid #ccc;
            }
            .fill-border{
                border:1px solid #ccc;
            }
            .fill span {
                vertical-align:middle;
                display: table-cell;
                padding:2px 10px;
            }
            
            .space{
                height:25px;
            }
            .mid-space{
                height:18px;
            }
            .short-space{
                height:10px;
            }
            
            .total-amount{
                border-bottom: double;
            }
            .top-border{
                border-top: 1px solid;
            }
            .surplus-border{
                border-bottom: 1px solid black;
            }
            
            .p0{
                padding:0!important;    
            }
            .pt0{
                padding-top:0!important;    
            }
            .pb0{
                padding-bottom:0!important;
            }
            .pb1{
                padding-bottom:1px!important;
            }
            .pb2{
                padding-bottom:2px!important;
            }
            .pb5{
                padding-bottom:5px!important;
            }
            .pl5{
                padding-left:5px!important;
            }
            .pl10{
                padding-left:10px!important;
            }
            .pr11{
                padding-right:11px!important;
            }
            .pr13{
                padding-right:13px!important;
            }
            
            .bt0{
                border-top:none!important;
            }
            .bb0{
                border-bottom:none!important;
            }

            .header-particular tr td{
                font-weight: normal!important;
            }

            .header-date tr td{
                font-weight: normal!important;
                padding: 2px 10px 5px 5px!important;
                border: 1px solid #ccc;
            }
            
            .inner-heading{
                font-size: 14px;
                margin-top: 0;
                font-weight:bold;
                text-decoration: underline;
                float: left;
            }
            .hand {
                cursor: pointer;
            }
            .data-not-found {
                height: 70px;
                color: #999;
                vertical-align: middle !important;
                text-align: center;
            }
            body {
                font-family: 'openSans', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="print_button">
                <button class="btn btn-default" onclick="printDocument()"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
            </div>
            <div class="overflow tableScrollOut">