@include("report.reportHeader")

<style type="text/css">

    body,td,th {
        font-size: 12px;
    }

    tbody .td-talign-r{
        text-align: right;
    }
    tbody .tal{
        text-align: left;
    }

    table tbody  tr th {
        text-align: center;
        vertical-align:middle !important;
    }

    tbody .grand-tt{
        background-color: #ddd;
        text-align: center;
    }

    tbody .grand-tt tr td{
        text-align: center;
        border: 1px solid black !important;
    }

    table tbody, tbody td, tbody th {
        border: 1px solid black !important;
    }

    tbody .tbc {
        padding: 4px !important;
        border: 1px solid black !important;
    }

    table tbody, tbody tr,tbody td{
        text-align: center;
    }

    table tbody td{
        padding: 8px !important;
    }


    tbody .data-not-found{
        height:50px;
    }

    .tbc{border:1px solid black !important; padding: 2px !important}
    .bl{border-left:1px solid black !important;}
    .br{border-right:1px solid black !important;}
    .bt{border-top:1px solid black !important;}
    .bb{border-bottom:1px solid black !important;}


    @media print {
         tbody .tbc {
            padding: 4px !important;
            border: 1px solid black !important;
        }
      }

</style>
            <table width="100%">
                <thead class="header">
                    @php $colspan=13; @endphp
                    @include("report.reportTblHeader")
                    <tr>
                        <th  class="tbc">Item #</th>
                        <th  class="tbc">Description</th>
                        <th  class="tbc">Qty On Hand</th>
                        <th  class="tbc">Total Items Sold</th>
                        <th  class="tbc">Last Date Sold</th>
                        <th  class="tbc">sales</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total_qty=0; @endphp
                    @if(count($reports_result) > 0)
                        @if(!empty($id) && $colum == "category_id")
                            <tr>
                                <td class="tbc" colspan="6">{{$category_info->category_name}}</td>
                            </tr>
                        @else
                            @if(!empty($id))
                                <tr>
                                    <td class="tbc" colspan="6">{{$vendor_info->vendor_name}}</td>
                                </tr>
                            @endif
                        @endif
                        @foreach($reports_result as $result)
                            @php
                             $product_id=$inventory_list[$result->product_id];
                             $total_qty= $total_qty + $product_id->qty_hand;
                            @endphp
                            <tr>
                                <td class="tbc">{{$product_id->inv_code }}</td>
                                <td class="tbc text-left">{{$product_id->inv_product_name}}</td>
                                <td class="tbc text-right">{{$product_id->qty_hand}}</td>
                                <td class="tbc text-right">{{$result->total_qty}}</td>
                                <td class="tbc">{{$product_id->last_date_sold}}</td>
                                <td class="tbc text-right">{{number_format($product_id->total_revenue_c,2)}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="tbc">No Data Found</td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th class="tbc" colspan="2">Grand Total</th>
                        <th class="tbc text-right" colspan="">{{$total_qty}}</th>
                        <th class="tbc" colspan="2"></th>
                        <th class="tbc" colspan=""></th>
                    </tr>
                </tfoot>
            </table>
@include("report.reportFooter")
