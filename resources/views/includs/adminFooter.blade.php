<!--=== JavaScript ===-->

    <script type="text/javascript" src="{{asset('melon/assets/js/libs/jquery-1.10.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('melon/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/assets/js/libs/lodash.compat.min.js')}}"></script> 

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="{{asset('melon/plugins/touchpunch/jquery.ui.touch-punch.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/event.swipe/jquery.event.move.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/event.swipe/jquery.event.swipe.js')}}"></script>

    <!-- General -->
    <script type="text/javascript" src="{{asset('melon/assets/js/libs/breakpoints.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/respond/respond.min.js')}}"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="{{asset('melon/plugins/cookie/jquery.cookie.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/slimscroll/jquery.slimscroll.horizontal.min.js')}}"></script>

    <!-- Page specific plugins -->
    <!-- Charts -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="plugins/flot/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{{asset('melon/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    
   
    <!-- Forms -->
    <script type="text/javascript" src="{{asset('melon/plugins/uniform/jquery.uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/select2/select2.min.js')}}"></script>

    <!-- App -->
    <script type="text/javascript" src="{{asset('melon/assets/js/app_them.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/assets/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/assets/js/plugins.form-components.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.resize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.time.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.orderBars.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.pie.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.selection.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/flot/jquery.flot.growraf.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('melon/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
    <!-- Demo JS -->
    <script type="text/javascript" src="{{asset('melon/assets/js/custom.js')}}"></script>
   
    <script>
        function initAllJs(){            
            "use strict";
            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins    
        }
        function cash_receivable(total_cash,total_account){
          var d_pie = [];

          d_pie[0] = { label: "Account Receivable", data: total_account }
          d_pie[1] = { label: "Cash", data: total_cash }

          $.plot("#chart_pie", d_pie, $.extend(true, {}, Plugins.getFlotDefaults(), {
            series: {
              pie: {
                show: true,
                radius: 1,
                label: {
                  show: true
                }
              }
            },
            grid: {
              hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
              content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
              shifts: {
                x: 20,
                y: 0
              }
            }
          }));
        }
        function best_seeling(best_seller){
          var seller_product = [];
          for(index=0; index < best_seller.length; index+=5){
            Chunk = best_seller.slice(index, index+5);
            seller_product.push(Chunk);
          }
          var ds = new Array();
          var d1 = [];
          if(typeof seller_product[0] != 'undefined'){
            for (var i = 0; i < seller_product[0].length; i += 1){
              d1.push([i,seller_product[0][i].total_sold_c,seller_product[0][i].inv_product_name]);
            }
          }
          var d2 = [];
          if(typeof seller_product[1] != 'undefined'){
            for (var i = 0; i < seller_product[1].length; i += 1){
              d2.push([i,seller_product[1][i].total_sold_c,seller_product[1][i].inv_product_name]);
            }
          }

          ds.push({
            data: d1,
            bars: {
              show: true,
              barWidth: 0.2,
              order: 1
            }
          });
          if(d2.length > 0){
            ds.push({
              data: d2,
              bars: {
                show: true,
                barWidth: 0.2,
                order: 2
              }
            });
          }
          // Initialize Chart
          $.plot("#chart_bars_vertical", ds, $.extend(true, {}, Plugins.getFlotDefaults(), {
            series: {
              lines: { show: false },
              points: { show: false }
            },
            grid:{
              hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
              content: '%s: %y'
            }
          }));
        }
        function worst_sell_bar(worst_seller){
          var seller_product = [];
          for(index=0; index < worst_seller.length; index+=5){
            Chunk = worst_seller.slice(index, index+5);
            seller_product.push(Chunk);
          }
         // console.log(seller_product[0]);
         var d1 = [];
         if(typeof seller_product[0] != 'undefined'){
            for (var i = 0; i < seller_product[0].length; i += 1){
              d1.push([i,seller_product[0][i].total_sold_c,seller_product[0][i].inv_product_name]);
            }
          }
          var d2 = [];
          if(typeof seller_product[1] != 'undefined'){
            for (var i = 0; i < seller_product[1].length; i += 1){
              d2.push([i,seller_product[1][i].total_sold_c,seller_product[1][i].inv_product_name]);
            }
          }

          var ds = new Array();
          ds.push({
            data: d1,
            bars: {
              show: true,
              barWidth: 0.2,
              order: 1
            }
          });
          if(d2.length > 0){
            ds.push({
              data: d2,
              bars: {
                show: true,
                barWidth: 0.2,
                order: 2
              }
            });
          }


          // Initialize Chart
          $.plot("#chart_bars_vertical-1", ds, $.extend(true, {}, Plugins.getFlotDefaults(), {
            series: {
              lines: { show: false },
              points: { show: false }
            },
            grid:{
              hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
              content: '%s: %y'
            }
          }));

        }
        function sell_by_category(category_data){

            var d_pie = [];

             for (var i = 0; i <category_data.length; i += 1){
               d_pie[i] = { label: category_data[i].category_name, data:category_data[i].total_sold }

             }
            // console.log(category_data);


            $.plot("#chart_bars_horizontal", d_pie, $.extend(true, {}, Plugins.getFlotDefaults(), {
              series: {
                pie: {
                  show: true,
                  radius: 1,
                  label: {
                    show: true
                  }
                }
              },
              grid: {
                hoverable: true
              },
              tooltip: true,
              tooltipOpts: {
                content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
                shifts: {
                  x: 20,
                  y: 0
                }
              }
            }));



        }
        function profit_loss(profit,loss)
        {
          var d1 = [];
          $.each(profit, function( key, value ) {
              var time=(new Date(value.y, value.increment, value.d)).getTime();
              d1.push([time,value.amount]);
          });

          var d2 = [];
          $.each(loss, function( key, value ) {
              var time=(new Date(value.y, value.increment, value.d)).getTime();
              d2.push([time,value.amount]);
          });
         // console.log(profit);


          var date = new Date();

            var data1 = [
                { label: "Total Profit", data: d1, color: App.getLayoutColorCode('green') },
            { label: "Total Loss", data: d2, color: App.getLayoutColorCode('red') }
            ];

            $.plot("#chart_filled_red", data1, $.extend(true, {}, Plugins.getFlotDefaults(), {
                xaxis: {
                    min: (new Date(date.getFullYear(),0,01)).getTime(),
                    max: (new Date(date.getFullYear(), 12, 01)).getTime(),
                    mode: "time",
                    tickSize: [1, "month"],
                    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    tickLength: 0
                },
                series: {
                    lines: {
                        fill: true,
                        lineWidth: 1.5
                    },
                    points: {
                        show: true,
                        radius: 2.5,
                        lineWidth: 1.1
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%s: %y'
                }
            }));

        }
        $(document).ready(function(){
           /* setTimeout(function(){
                "use strict";

                App.init(); // Init layout and core plugins
                Plugins.init(); // Init all plugins
                FormComponents.init(); // Init all form-specific plugins
            }, 1000);*/
            $('#set-dropdown>a').click(function(){
                if($(this).parent().hasClass("show")){
                    $(this).parent().removeClass("open");
                }else{
                    $(this).parent().addClass("open");
                }
            });
        });
    </script>