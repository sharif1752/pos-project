@include('includs.masterheader')

<style type="text/css">
	.user-panales .container{
		margin-top: 60px;
		max-width: 1170px !important;
		padding-right: 15px;
		padding-left: 15px;
	}
	.tmss-logo{
		margin-bottom: 40px;
	}
</style>
<body>
	<header class="header navbar navbar-fixed-top" role="banner">
	    <!-- Top Navigation Bar -->
	    <div class="container">

	        <!-- Only visible on smartphones, menu toggle -->
	        <ul class="nav navbar-nav">
	            <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
	        </ul>

	        <!-- Logo -->
	        <a class="navbar-brand" href="index.html">
	            <img src="{{asset('melon/assets/img/logo.png')}}" alt="logo" />
	            <strong>ME</strong>LON
	        </a>
	        <!-- /logo -->

	        <!-- Top Right Menu -->
	        <ul class="nav navbar-nav navbar-right">


	            <!-- User Login Dropdown -->
	            <li class="dropdown user user-layout" id="set-dropdown">
	                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                    <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
	                    <div class="profile-img">
	                        <img src="{{asset('images/'.$user->photo)}}">
	                        <span class="username">{{$user->name}}</span>
	                        <i class="icon-caret-down small"></i>
	                    </div>
	                </a>
	                <ul class="dropdown-menu">
	                    <li><a href="pages_user_profile.html"><i class="icon-user"></i> My Profile</a></li>
	                    <li>
	                        <a href="{{url(route('changePasswor'))}}">Change Password</a>
	                    </li>
	                    <li><a href="{{ route('user.logout') }}" onclick="event.preventDefault();
	                        document.getElementById('logout-form').submit();"><i class="icon-key"></i> Log Out</a></li>
	                </ul>
	                <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
	                        @csrf
	                </form>
	            </li>
	            <!-- /user login dropdown -->
	        </ul>
	        <!-- /Top Right Menu -->
	    </div>
	    <!-- /top navigation bar -->
	</header> <!-- /.header -->

	<div class="login user-panales">
		<!-- Logo -->
		<!-- <div class="logo">
			<strong>USER</strong>LOGIN
		</div> -->
		<!-- /Logo -->

				<!-- Login Formular -->
				<div class="container text-center">
					<div class="tmss-logo text-center">
						<img src="{{asset('images/tmss-logo.png')}}">
					</div>
					<div class="row layout-user-box">
						<div class="col-md-4">
							<div class="box">
								<div class="content">
									<!-- Login Formular -->
									<h2 class="form-title">Central</h2>
									<form class="form-vertical login-form" method="POST" action="">
										<!-- Input Fields -->
										<div class="form-group">
											<!--<label for="username">Username:</label>-->
											<div class="input-icon">
												<select disabled class="form-control">
			                                        <option>All Corporate</option>
			                                    </select>
											</div>
										</div>
										<div class="form-actions">
											<button id="central-btn"  type="button" class="submit btn btn-primary pull-left">
												Go to panel <i class="icon-angle-right"></i>
											</button>
										</div>
									</form>
									<!-- /Login Formular -->
								</div> <!-- /.content -->
							</div>
						</div>
						<div class="col-md-4">
							<div class="box">
			                   <div class="content">
									<!-- Login Formular -->
									<h2 class="form-title">Corporate</h2>
									<form class="form-vertical login-form" method="POST" action="">
										<!-- Input Fields -->
										<div class="form-group">
											<!--<label for="username">Username:</label>-->
											<div class="input-icon">
												 <select name="corporate_id" id="corporate_id" class="form-control">
										        	<option value="" selected disabled>Select Corporate</option>
										        	@foreach($corporateInfo as $v)
										        	    <option value="{{$v->id}}">{{$v->name}}</option>
										        	@endforeach
						        				</select>
											</div>
										</div>
										<div class="form-actions">
											<button id="corporate-btn" type="button" class="submit btn btn-primary pull-left">
												Go to panel <i class="icon-angle-right"></i>
											</button>
										</div>
									</form>
									<!-- /Login Formular -->
								</div> <!-- /.content -->
							</div>
						</div>
						<div class="col-md-4">
							<div class="box">
								<div class="content">
									<!-- Login Formular -->
									<h2 class="form-title">Branch/Showroom</h2>
									<form class="form-vertical login-form" method="POST" action="">
										<!-- Input Fields -->
										<div class="form-group">
											<!--<label for="username">Username:</label>-->
											<div class="input-icon">
												 <select name="showroom_id" id="showroom_id" class="form-control">
										        	<option value="" selected disabled>Select Branch/Showroom</option>
										        	@foreach ($showrooms as $k => $showroomsList)
											        	<optgroup label="<?php echo $corporateInfo[$k]->name ?>">
		                                                    <?php foreach ($showroomsList as $v) { ?>
		                                                        <option value="<?php echo $v->id ?>"><?php echo $v->showroom_name; ?></option>
		                                                    <?php } ?>
		                                                </optgroup>
		                                            @endforeach
						        				</select>
											</div>
										</div>
										<div class="form-actions">
											<button id="showroom-btn" type="button" class="submit btn btn-primary pull-left">
												Go to panel <i class="icon-angle-right"></i>
											</button>
										</div>
									</form>
									<!-- /Login Formular -->
								</div> <!-- /.content -->
							</div>
						</div>
					</div>
				</div>
				<!-- /Login Formular -->
		<!-- /Login Box -->
	</div>
</body>
@include('includs.masterfooter')
<script src="<?php echo url(route('jsBaseURLs')); ?>"></script>
<script type="text/javascript">
	$(document).ready(function()
    {
    	$("#central-btn").click(function(e){
            e.preventDefault();
            location.replace(URL.baseUrl('central'));
        });

        $("#corporate-btn").click(function(e){
            e.preventDefault();
            var corporate_id = $("#corporate_id").val();
            if(!corporate_id) {
                alert("Please Select Corporate");
            } else {
                location.replace(URL.baseUrl(corporate_id+'/corporate'));
            }
        });

        $("#showroom-btn").click(function(e){
            e.preventDefault();
            var showroom_id = $("#showroom_id").val();
            if(!showroom_id) {
                alert("Please Select Showroom");
            } else {
                location.replace(URL.baseUrl(showroom_id+'/showroom'));
            }
        });


    });
</script>
