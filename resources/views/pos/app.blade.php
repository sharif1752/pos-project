@include('includs.posHeader')
<style>
    .modal-backdrop{
        z-index: 999;
    }
    .modal-btn-wraper{
        overflow: hidden;
    }
    .modal-btn-wraper a{
        margin-left: 10px;
    }
    .modal-header h4{
        display: inline-block;
    }
</style>
<body>

    <!-- Header -->
    <div id="app">

        <header class="header navbar navbar-fixed-top" role="banner">
            <!-- Top Navigation Bar -->
            <div class="container">

                <!-- Only visible on smartphones, menu toggle -->
                <ul class="nav navbar-nav">
                    <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
                </ul>

                <!-- Logo -->
                <a class="navbar-brand" href="{{ url('pos/') }}">
                    <img src="{{asset('melon/assets/img/logo.png')}}" alt="logo" />
                    POS
                </a>
                <!-- /logo -->
                <!-- Sidebar Toggler -->
                <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                    <i class="icon-reorder"></i>
                </a>
                <!-- /Sidebar Toggler -->
                <pos-topbar></pos-topbar>
                <!-- Top Right Menu -->
                <ul class="nav navbar-nav navbar-right">


                    <!-- User Login Dropdown -->
                    <li class="dropdown user user-layout" id="set-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
                            <div class="profile-img">
                                <img src="{{asset('images/default.png')}}">
                                <span class="username">{{$user->name}}</span>
                                <i class="icon-caret-down small"></i>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="pages_user_profile.html"><i class="icon-user"></i> My Profile</a></li>
                            <li>
                                <pos-header-link></pos-header-link>
                            </li>
                            @if(Auth::guard('user')->user()->role_id == 1)
                                <li><a href="" data-toggle="modal" data-target="#shiftModal"><i class="icon-key"></i> Log Out</a>
                                </li>
                            @else
                                <li><a href="" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="icon-key"></i> Log Out</a>
                                </li>
                            @endif
                        </ul>
                        <form id="logout-form" action="{{ route('user.logout') }}" method="GET" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <!-- /user login dropdown -->
                </ul>
                <!-- /Top Right Menu -->
                <div class="modal fade" id="shiftModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        
                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title">Shifting Close</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          
                          <!-- Modal body -->
                          <div class="modal-body">
                            <h4>Do you want to close your shift</h4>
                            <div class="modal-btn-wraper">
                                <a href="user/logout?shift_close=0&id={{$user->id}}" class="btn btn-danger pull-right">No</a>
                                <a href="user/logout?shift_close=1&id={{$user->id}}" class="btn btn-primary pull-right">Yes</a>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /top navigation bar -->
        </header> <!-- /.header -->

        <div id="container">
            <div id="sidebar" class="sidebar-fixed">
                <div id="sidebar-content">

                    <!-- Search Input -->
                        <!-- <form class="sidebar-search">

                            <div class="input-box">
                                <button type="submit" class="submit">
                                    <i class="icon-search"></i>
                                </button>
                                <span>
                                    <input type="text" placeholder="Search...">
                                </span>
                            </div>

                        </form> -->
                    <!-- Search Results -->

                    <div class="sidebar-search-results">

                        <i class="icon-remove close"></i>
                        <!-- Documents -->
                        <div class="title">
                            Documents
                        </div>
                        <ul class="notifications">
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="col-left">
                                        <span class="label label-info"><i class="icon-file-text"></i></span>
                                    </div>
                                    <div class="col-right with-margin">
                                        <span class="message"><strong>John Doe</strong> received $1.527,32</span>
                                        <span class="time">finances.xls</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="col-left">
                                        <span class="label label-success"><i class="icon-file-text"></i></span>
                                    </div>
                                    <div class="col-right with-margin">
                                        <span class="message">My name is <strong>John Doe</strong> ...</span>
                                        <span class="time">briefing.docx</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /Documents -->
                        <!-- Persons -->
                        <div class="title">
                            Persons
                        </div>
                        <ul class="notifications">
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="col-left">
                                        <span class="label label-danger"><i class="icon-female"></i></span>
                                    </div>
                                    <div class="col-right with-margin">
                                        <span class="message">Jane <strong>Doe</strong></span>
                                        <span class="time">21 years old</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div> <!-- /.sidebar-search-results -->

                    <!--=== Navigation ===-->

                    <pos-sidebar></pos-sidebar>

                </div>
                <div id="divider" class="resizeable"></div>
            </div>
            <!-- /Sidebar -->

            <div id="content">
                <div class="container">
                    <!-- Breadcrumbs line -->
                   <!--  <div class="crumbs">
                        <ul id="breadcrumbs" class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.html">Dashboard</a>
                            </li>
                            <li class="current">
                                <a href="pages_calendar.html" title="">Calendar</a>
                            </li>
                        </ul>
                    </div> -->
                    <bredcrumb></bredcrumb>
                    <!-- /Breadcrumbs line -->
                    <!--=== Page Content ===-->
                <div class="row">
                    <router-view></router-view>
                </div>
                <!-- /Page Content -->
            </div>
                <!-- /.container -->
            </div>
        </div>

 </div>
    <script src="<?php echo url(route('pos.jsBaseURLs')); ?>"></script>
    <script src="{{asset('js/app.js')}}"></script>
@include('includs.posFooter')
</body>
</html>
