
@include('includs.header')

<body>

    <!-- Header -->
    <div id="app">

        @yield('content')
    </div>    
    <script src="{{asset('js/app.js')}}"></script>
@include('includs.footer')
</body>
</html>