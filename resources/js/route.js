import home from "./components/home.vue";
import Menu from "./components/admin/menu.vue";
import packageTest from "./components/admin/PackageTest.vue";
import AdminChangePassword from "./components/admin/changepassword.vue";
import UserChangePassword from "./components/pos/ChangePassword.vue";
import Setting from "./components/admin/Setting.vue";

/*Report*/
import Itemlist from "./components/Report/ReportItemList.vue";
import Bestseller from "./components/Report/ReporBestseller.vue";
import Worstseller from "./components/Report/ReporWorstseller.vue";
import Orderditem from "./components/Report/ReportItemOrder.vue";
import Pricelist from "./components/Report/ReportPriceList.vue";
import Customerlist from "./components/Report/ReportCustomerList.vue";
import Vendorlist from "./components/Report/ReportVendorList.vue";
import Slaespersonlist from "./components/Report/ReportSalespersonList.vue";
import SlaespersonSales from "./components/Report/ReportSalespersonSales.vue";
import PysicalInventory from "./components/Report/ReportPhysicalInventory.vue";
import Valuebycategory from "./components/Report/ReportValueByCategory.vue";
import Valuebyvendor from "./components/Report/ReportValueByVendor.vue";
import Itembelowreorder from "./components/Report/ReportBelowReorder.vue";
import Slesreport from "./components/Report/ReportInventorySales.vue";
import Invoicehistory from "./components/Report/ReportInvoiceHistory.vue";
import Accounts from "./components/accounts/Accounts.vue";
import Arasing from "./components/Report/ReportArAsing.vue";
/*Report*/

/*Admin People*/
import UserRole from "./components/admin/UserRole.vue";
import AdminRole from "./components/admin/AdminRole.vue";
import Admin from "./components/admin/admin.vue";
import User from "./components/admin/user.vue";
/*Admin People*/

/*Sales Person*/
import SalesPerson from "./components/admin/SalesPerson.vue";
/*Sales Person*/

/*POS*/
import InventoryItem from "./components/pos/InventoryItem.vue";
import InventoryCategory from "./components/pos/InventoryCategory.vue";
import InventoryReceive from "./components/pos/InventoryReceive.vue";
import InventoryPos from "./components/pos/InventoryPos.vue";
import Vendor from "./components/pos/Vendor.vue";
import Customer from "./components/pos/Customer.vue";
import InventoryArView from "./components/pos/InventoryArView.vue";
import InventoryArPostView from "./components/pos/InventoryArPostView.vue";

/*POS*/

export const routes=[
	{path:"/", component:home, meta: {fetchUrl:'dashboard_data',title:"Dashboard",icon:"icon-dashboard"}},
	{path:"/changepassword", component:AdminChangePassword,meta:{title:"ChangePassword",icon:"icon-table"}},
	{path:"/user/changepassword", component:UserChangePassword,meta:{title:"ChangePassword",icon:"icon-table"}},
	{path:"/packagetest", component:packageTest},
	{path:"/adminlist", component:Admin, meta: { fetchUrl:'list',title:"peoples",subtitle:"Admin",icon:"icon-group"}},
	{path:"/userlist", component:User, meta: { fetchUrl:'user/list',title:"Management",subtitle:"Manager",icon:"icon-group"}},
	{path:"/adminRole", component:AdminRole, meta: { fetchUrl:'adminrole/list',title:"peoples",subtitle:"Admin Role",icon:"icon-group"}},
	{path:"/userRole", component:UserRole, meta: { fetchUrl:'role/list',title:"Management",subtitle:"Role",icon:"icon-group"}},
	{path:"/menu", component:Menu,  meta: { fetchUrl:'menu/list',title:"Menu",icon:"icon-table"}},
	{path:"/inventory_category",component:InventoryCategory,meta:{fetchUrl:'inventory_category/list',title:"Inventory",subtitle:"Inventory Catgeory",icon:"icon-edit"}},
	{path:"/inventory_item",component:InventoryItem,meta:{fetchUrl:'inventory_item/list',title:"Inventory",subtitle:"Inventory Item",icon:"icon-reorder"}},
	{path:"/inventory_receive",component:InventoryReceive,meta:{fetchUrl:'inventory_receive/list',title:"Inventory",subtitle:"Inventory Receive",icon:"icon-signin"}},
	{path:"/salesPerson",component:SalesPerson,meta:{fetchUrl:'salesPerson',title:"Sales Person",icon:"icon-bar-chart"}},
	{path:"/pos",component:InventoryPos,meta:{title:"Pos",icon:"icon-edit-sign"}},
	{path:"/vendor",component:Vendor,meta:{fetchUrl:'vendor/list',title:"Vendor",icon:"icon-table"}},
	{path:"/customer", component:Customer,  meta: { fetchUrl:'customer/list',title:"Customer",icon:"icon-table"}},
	{path:"/setting", component:Setting,  meta: { fetchUrl:'customerinfo/list',title:"Setting",icon:"icon-cogs"}},
	{path:"/itemlist", component:Itemlist,  meta: {reportUrl:'geilist/report',title:"Report",subtitle:"Inventory",deeptitle:"Item List",icon:"icon-table"}},
	{path:"/bestseller", component:Bestseller,  meta: {reportUrl:'bestseller/report',title:"Report",subtitle:"Inventory",deeptitle:"Best Seller",icon:"icon-table"}},
	{path:"/worstseller", component:Worstseller,  meta: {reportUrl:'worstseller/report',title:"Report",subtitle:"Inventory",deeptitle:"Worst Seller",icon:"icon-table"}},
	{path:"/itemsorder", component:Orderditem,  meta: {reportUrl:'itemorder/report',title:"Report",subtitle:"Inventory",deeptitle:"Items On Order",icon:"icon-table"}},
	{path:"/pricelist", component:Pricelist,  meta: {reportUrl:'pricelist/report',title:"Report",subtitle:"Inventory",deeptitle:"Price List",icon:"icon-table"}},
	{path:"/customerlist", component:Customerlist,  meta: {reportUrl:'customer/report',title:"Report",subtitle:"Customer",deeptitle:"Customer List",icon:"icon-table"}},
	{path:"/vendorlist", component:Vendorlist,  meta: {reportUrl:'vendor/report',title:"Report",subtitle:"Vendor",deeptitle:"Vendor List",icon:"icon-table"}},
	{path:"/salespersonlist", component:Slaespersonlist,  meta: {reportUrl:'salesperson/report',title:"Report",subtitle:"Sales Person",deeptitle:"Salesperson List",icon:"icon-table"}},
	{path:"/personwisesales", component:SlaespersonSales,  meta: {reportUrl:'salespersonsales/report',title:"Report",subtitle:"Sales Person",deeptitle:"Salesperson Sales",icon:"icon-table"}},
	{path:"/physicalinventory", component:PysicalInventory,  meta: {reportUrl:'physicalinventory/report',title:"Report",subtitle:"Inventory",deeptitle:"Physical Inventory List",icon:"icon-table"}},
	{path:"/inv-value-by-category", component:Valuebycategory,  meta: {reportUrl:'valuebycatogory/report',title:"Report",subtitle:"Inventory",deeptitle:"Inventory Value By Category",icon:"icon-table"}},
	{path:"/inv-value-by-vendor", component:Valuebyvendor,  meta: {reportUrl:'valuebyvendor/report',title:"Report",subtitle:"Inventory",deeptitle:"Inventory Value By Vendor",icon:"icon-table"}},
	{path:"/items-bellow-reorder-level", component:Itembelowreorder,  meta: {reportUrl:'belowreorder/report',title:"Report",subtitle:"Inventory",deeptitle:"Items Bellow Recoder Level",icon:"icon-table"}},
	{path:"/inventory-sales-report", component:Slesreport,  meta: {reportUrl:'invetorysales/report',title:"Report",subtitle:"Inventory",deeptitle:"Inventory Sales Report",icon:"icon-table"}},
	{path:"/invoicehistory", component:Invoicehistory,  meta: {reportUrl:'invoicehistory/report',title:"Report",subtitle:"Inventory",deeptitle:"Invoice History",icon:"icon-table"}},
	{path:"/accounts", component:Accounts,  meta: {reportUrl:'account-information/report',title:"Accounts",icon:"icon-cogs"}},
	{path:"/ar-asing-report", component:Arasing, meta:{reportUrl:'ar-asing/report',title:"Account Receiveable",subtitle:"Account Receiveable",icon:"icon-signin"}},
	{path:"/view_statement", component:InventoryArView,  meta: {fetchUrl:'view_statement/list',title:"Account Receivable",subtitle:"View Statement",icon:"icon-file-text-alt"}},
	{path:"/post_ar_payment", component:InventoryArPostView,  meta: {fetchUrl:'post_ar_payment/list',title:"Account Receivable",subtitle:"Post Account Receivable",icon:"icon-check"}}
]
