/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import CxltToastr from 'cxlt-vue2-toastr';
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';
import VModal from 'vue-js-modal';
import Vuelidate from 'vuelidate';
import {routes} from "./route";
import VueSweetalert2 from 'vue-sweetalert2';
import Crud from './components/mixins/crud';
import Multiselect from 'vue-multiselect';
//import Select2MultipleControl from 'v-select2-multiple-component';
import Datepicker from 'vuejs-datepicker';
import VoerroTagsInput from '@voerro/vue-tagsinput';
import Print from 'vue-print-nb';
Vue.use(VueRouter);
Vue.use(VModal);
Vue.use(Vuelidate);
Vue.use(require('vue-resource'));
Vue.use(VueSweetalert2);
Vue.mixin(Crud);



Vue.component('admin-header-link', require('./components/admin/headerLink.vue').default);
Vue.component('pos-header-link', require('./components/pos/headerLink.vue').default);
Vue.component('pos-topbar', require('./components/pos/posTopbar.vue').default);
Vue.component('admin-sidebar', require('./components/admin/AdminSidebar.vue').default);
Vue.component('bredcrumb', require('./components/BredCrumb.vue').default);
Vue.component('pos-sidebar', require('./components/pos/PosSidebar.vue').default);
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('pageLoading', require('./components/Loading.vue').default);
Vue.component('select2', require('./components/select2.vue').default);
//Vue.component('Select2MultipleControl', Select2MultipleControl);
Vue.component('vue-select', Multiselect);
Vue.component('Datepicker', Datepicker);
Vue.component('tags-input', VoerroTagsInput);

const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}

var toastrConfigs = {
    position: 'top right',
    showDuration:1000,
    hideDuration:2000
};
Vue.use(Print);
Vue.use(CxltToastr,toastrConfigs);

/*Vue.use(VueLoading, {
  dark: true, // default false
  text: 'Loading', // default 'Loading'
  loading: true, // default false
  customLoader:null, // replaces the spinner and text with your own
  background: 'rgb(255,255,255)', // set custom background
  classes: ['myclass'] // array, object or string
});*/

const router = new VueRouter({
  routes,
});


const app = new Vue({
  router
}).$mount('#app');
