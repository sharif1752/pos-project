export default {
  data(){
    return {
    lists:{},
    page_loading:false,
    form_data:{},
    report_data:{},
    option_data:{},
    search_input:{},
    grid_data:{},
    edit:false,
    paginate_data:0,
    sort:'id',
    order:'desc',
    paginate_num:10,
    dataUrl:null,
    validate:false,
    current_page_no:1,
    modal_loading:false,
    pagetitle:'',
    order_no:0,
    errors:null,
    dateFormat: "dd/MM/yyyy",
    }
  },
  computed:{
    isComplete: {
      cache: false,
      get: function () {
        return this.$v.form_data.$invalid;
      }
    },
    completeGridData:{
      cache: false,
      get: function () {
        return this.$v.grid_data.$invalid;
      }
    }
  },

  methods:{
    showModal () {
      this.$modal.show('myModal');
    },
    hideModal () {
      this.$modal.hide('myModal');
    },
    reload(){
      this.$router.go();
    },
    onChange(event) {
      let paginate =event.target.value;
      this.getResults();
    },
    isObject (v) {
      return (typeof v ==='object') ? true : false;
    },
    onFileSelected(event){
      let file = event.target.files[0];

      if(file.size > 1048770){
        this.showToster({status:0,message:"Image size is big"});
      }else{
        let reader = new FileReader();
        reader.onload = (e) => {
          this.form_data.photo = e.target.result;
        };
        reader.readAsDataURL(file);
      }
    },
    add(addUrl,callback){
      if (typeof this.button_loading != 'undefined') {
        this.button_loading = true;
      }
      axios.post(URL.baseUrl(addUrl.add),this.form_data)
      .then(res => {
        if(res.data.status==1){
          if(!this.form_data.id){
            this.formReset();
            this.getResults(1);
          }else{
            this.hideModal();
            this.getResults(this.current_page_no);
          }
        }
        this.errors =null;
        this.showToster(res.data);
        if (typeof this.button_loading != 'undefined') {
          this.button_loading = false;
        }
        if(callback){
          callback();
        }
      })
      .catch(error => {
        if(error.response.status == 422){
          this.errors = error.response.data.errors;
        }
        if (typeof this.button_loading != 'undefined') {
          this.button_loading = false;
        }
        var msg = 'opps! something went wrong';
        this.showToster({status:0,message:msg});
      });
    },
    formReset(){
      this.form_data={};
      console.log(this.form_data);
    },
    showToster(info){
      if(info.status == 1){
        this.$toast.success({
          title:'Success!',
          message:info.message
        });
      }else{
        if(typeof info.message === 'object'){
          this.errors = info.message;
          var msg = 'opps! something went wrong';
        }else{
          var msg = info.message;
        }
        this.$toast.error({
          title:'Error!',
          message:msg
        });
      }
    },
    deleteItem(deleteUrl){
      this.$swal({
        title: 'Are you sure?',
        text: 'You can\'t revert your action',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes Delete it!',
        cancelButtonText: 'No, Keep it!',
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then((result) => {
        if(result.value) {
          axios.delete(URL.baseUrl(deleteUrl.delUrl))
          .then(res => {
            if(res.data.status==0){
              this.showToster(res.data);
            }else{
              this.$swal('Deleted', 'You successfully deleted this file', 'success');
              if(Object.keys(this.paginate_data.data).length > 1){
                this.getResults(this.current_page_no);
              }else{
                this.getResults(this.current_page_no - 1);
              }
            }  
          })
          .catch(error => {
            console.log(error);
            // handle error
            this.showToster({status:0,message:'Opps something went wrong'});
          })
        } else {
          this.$swal('Cancelled', 'Your file is still intact', 'info')
        }
      });
    },
    getResults(page){
      if (typeof page === 'undefined') {
        page = 1;
      }

      var obj_params = {
        paginate_num: this.paginate_num,
        page: page,
        sort:this.sort,
        order:this.order
      };
      var data_params = Object.assign(this.search_input,obj_params);
      var fetchUrl = this.$route.meta.fetchUrl;
      this.current_page_no = page;
      axios.get(URL.baseUrl(fetchUrl), {
        params:data_params
      })
      .then(res => {
        if(res.data.status =='logout'){
          window.location.href=res.data.url;

        }else{
          if(res.data.dashboard_data ==1){
            this.test(res.data);
            this.form_data = res.data;
            console.log(this.form_data);
          }
          this.lists=res.data;
          if(typeof res.data.paginate_data !=='undefined'){
            this.paginate_data = res.data.paginate_data;
          }
          if(res.data.formData){
            this.form_data = res.data.formData;
            //this.option_data = res.data;
            console.log(res.data.formData);
          }
          if(res.data.search_input){
            this.search_input = res.data.search_input;
          }
          this.pagetitle = res.data.pagetitle;
        }

        if( page > 1){
          this.order_no = this.paginate_num * (page - 1);
        }else{
          this.order_no = 0;
        }
        this.page_loading = true;
      })
      .catch(error => {
        console.log(error);
        this.showToster({status:0,message:'opps! something went wrong'});
        this.page_loading = true;
      })
    },
    getModalData(event,obj,callback){
      this.showModal();
      if(obj && obj.dataUrl){

        this.modal_loading= false;
        axios.get(URL.baseUrl(obj.dataUrl))
        .then(res => {
          this.form_data = res.data;
          this.option_data = res.data;
          this.modal_loading= true;
          this.errors =null;
          if(!this.form_data.id){
            if(callback){
              callback();
            }
          }else{
            if(callback){
              callback();
            }
          }
        })
        .catch(error => {
          this.showToster({status:0,message:'opps! something went wrong'});
          this.modal_loading= true;
        })

      }else{
        this.form_data = {};
        this.modal_loading= true;
      }
    },
    sortingChanged(column){
      if(this.sort != column){
        this.order = 'asc';
      }else{
        if(this.order == 'asc') this.order = 'desc';
        else this.order = 'asc';
      }
      this.sort = column;
      this.getResults();
    },
    getSortingClass(column){
      if(this.sort == column){
        return (this.order == 'asc') ? 'asc' : 'desc';
      }
    },
    gridAdd(event, input) {
      event.preventDefault();
      console.log(input.index);
      /*let objCopy = Object.assign({}, input);
      this.form_data.details_data.push(objCopy);*/
      this.form_data.details_data.push(input);
      this.grid_data = {};
    },
    gridEdit(event, input){
      event.preventDefault();
      this.grid_data = input;
      var index = this.form_data.details_data.indexOf(input);
      this.form_data.details_data.splice(index,1);
    },
    gridRemove(event, data){
      event.preventDefault();
    },
    viewReport(formdata,rUrl){
      console.log(this.$route.meta.reportUrl);
      var urlParam = "";
      var Url = "";
      if(typeof formdata != 'undefined'){
        if(Object.keys(formdata).length > 0){
          for (let key in formdata) {
            urlParam += ((urlParam=="")?"?":"&")+key+"="+formdata[key];
          }
        }
      }
      if(rUrl){
        Url = rUrl;
      }else{
        Url = this.$route.meta.reportUrl;
      }
      var width = $(document).width();
      var height = $(document).height();
      var myWindow = window.open(URL.baseUrl(Url+urlParam), "", "width="+width+",height="+height);
    }

  }
}
