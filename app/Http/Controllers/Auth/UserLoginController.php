<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Model\SalesPersonModel;
use App\Model\SalesPersonShiftModel;
use DateTime;
use DB;
//use App\Model\UserModel;


class UserLoginController extends Controller
{
    public function __construct(){

        $this->middleware('guest:user')->except('logout');

	}

    public function showLoginForm(){
        return view('auth.user-login');
    }

	public function login(Request $request){

    	//Validate the form data

    	$this->validate($request,[

    		'username' => 'required',
    		'password' => 'required'

    	]);

        $valid_test = SalesPersonModel::valid()->where('username',$request->username)->first();

        //dd($valid_test);

        if($valid_test){

        	//Attempt to log the user

        	if(Auth::guard('user')->attempt(['username'=>$request->username,'password'=>$request->password])){
               
               //if successful, then redirect to their intended loaction
              if($valid_test->role_id == 1){
                self::shiftLogDetails($valid_test->id);
              }

        		return redirect(route('pos.home'));
        	}

        	//if unsuccessful, then redirect to the login with form data
            
            return back()->withInput()->with('status', "Username and password doesn't match");
        }

        return back()->withInput()->with('status', "Invalid username and password");
    }

    public static function shiftLogDetails($id)
    {

      $login_status=DB::table('inv_salesperson_shift')->where('valid',1)->where('sales_id',$id)->where('status',1)->first();
      $date=new DateTime();
      $date_time=$date->format('Y-m-d H:i:s');
      if(!empty($login_status)){

      $get_chek_date = explode(' ',$date_time);

      $pre_check=DB::table('inv_salesperson_shift')->where('valid',1)->where('status',1)->where('start_time','<',$get_chek_date[0])->first();

       if($pre_check){
          $format_time =explode(' ',$pre_check->start_time);
          $format_time[1] = '11:59:00';
          $pre_date_time = $format_time[0].' '.$format_time[1];
          DB::table('inv_salesperson_shift')->where('valid',1)->where('sales_id',$id)->update(array('status' => 0,"end_time"=>$pre_date_time));
           $data=[
             "sales_id"=>$id,
             "start_time"=>$date_time,
             "status"=>1
           ];
           $insert=SalesPersonShiftModel::create($data);
        }
        $current_check = DB::table('inv_salesperson_shift')->where('valid',1)->where('status',1)->where('start_time','=',$get_chek_date[0])->first(); 
        if( $current_check){
            $data=[
             "sales_id"=>$id,
             "start_time"=>$date_time,
             "status"=>1
           ];
           $insert=SalesPersonShiftModel::create($data);
        }

        }else{
            $data=[
             "sales_id"=>$id,
             "start_time"=>$date_time,
             "status"=>1
           ];
           $insert=SalesPersonShiftModel::create($data);
        }
    }

    public function logout(Request $request)
    { 
        if($request->shift_close == 1){
            $date=new DateTime();
            $date_time=$date->format('Y-m-d H:i:s');
            DB::table('inv_salesperson_shift')->where('valid',1)
                                          ->where('status',1)
                                          ->where('sales_id',$request->id)
                                          ->update(array('status' => 0,"end_time"=>$date_time));
        }
        Auth::guard('user')->logout();

        return redirect()->route('user.login');
    }
}
