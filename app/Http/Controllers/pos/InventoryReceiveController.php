<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\InventoryReceiveModel;
use App\Model\InventoryReceiveDetailsModel;
use App\Model\InventoryItemModel;
use App\Model\CompanyConfigur;
use DB;
class InventoryReceiveController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
      $paginate_num = $request->input('paginate_num');
      $search_key = $request->input('search_key');
      $order = $request->input('order');
      $sort = $request->input('sort');
      $paginate_data =InventoryReceiveModel::valid()->when($search_key, function($query, $search_key){
                  $query->where(function($query2)use($search_key){
                      $query2->where('invoice_number','LIKE','%'.$search_key.'%');
                  });
                  return $query;

          })->orderBy($sort,$order)->paginate($paginate_num);
      foreach ($paginate_data as $v) {
          $v->invoice_number = self::getNumber($v->invoice_number);
      }
      $data['paginate_data'] = $paginate_data;
      $data['formData']['add_row']=['0' =>['id'=>0,'product_id'=>'','description'=>'','receive_cost'=>'','receive_qty'=>'']];
      $data['formData']['invoice_number'] = self::getNumber();
      $product_data=collect(InventoryItemModel::valid()->where('inv_code','!=','')->get());
      $data['formData']['all_product']=$product_data->keyBy('inv_code')->all();
      return response()->json($data);

    }

    public function create()
    {
         $product_data=collect(InventoryItemModel::valid()->where('inv_code','!=','')->get());
         $data['all_product']=$product_data->keyBy('inv_code')->all();
         $data['invoice_number'] = self::getNumber();
         $data['add_row'] = ['0' =>['id'=>0,'product_id'=>'','description'=>'','receive_cost'=>'','receive_qty'=>'']];

         return response()->json($data);
    }


    public function store(Request $request)
    {
      DB::beginTransaction();
      try {
          $validation = [
                'invoice_number'=>'required',
                'total_amount'=>'required',
                'total_item'=>'required'
            ];
            $request->validate($validation);
            $date_get=date('Y-m-d');
            $add_row = $request->add_row;
            $total_amount = 0;
            $total_item = 0;
            // qty and amount check
            $flag=true;
            if($request->total_amount<1) $flag=false;
            if($flag){
              $product_ids = collect($add_row)->pluck('product_id')->all(); 
              $inv_products=InventoryItemModel::valid()->whereIn('inv_code',$product_ids)->get()->keyBy('inv_code')->all();
              foreach ($add_row as $k => $value) {
                  if(!empty($value['product_id'])&&!empty($value['description'])&&!empty($value['receive_cost'])&&!empty($value['receive_qty'])){
                      if($value['receive_qty']<1){
                        $flag=false;
                        break;
                      }else{
                        $total_amount+= $value['receive_qty']*$value['receive_cost'];
                        $total_item+= $value['receive_qty'];
                      } 
                  }
              }
            }

            if($flag){
              $data = ['invoice_number'=>$request->invoice_number,'total_amount'=>$total_amount,'total_item'=>$total_item, 'date'=>$date_get];
              $save =   InventoryReceiveModel::create($data);
              //dd($add_row);
              foreach ($add_row as $value) {
                if(!empty($value['product_id'])&&!empty($value['description'])&&!empty($value['receive_cost'])&&!empty($value['receive_qty'])){
                    $details_data=[
                      'product_id'=>$value['product_primary'],
                      'receive_cost'=>$value['receive_cost'],
                      'receive_qty'=>$value['receive_qty'],
                      'product_receive_id'=>$save->id,

                    ];
                    $inv_product=$inv_products[$value['product_id']];
                    if($inv_product->price_a == $inv_product->sale_price){
                      $price=self::getMarkup($inv_product->markup_a,$value['receive_cost']);
                      $price_code ='price_a';
                    }
                    elseif ($inv_product->price_b == $inv_product->sale_price) {
                      $price=self::getMarkup($inv_product->markup_b,$value['receive_cost']);
                      $price_code ='price_b';
                    }
                    elseif ($inv_product->price_c == $inv_product->sale_price) {
                      $price=self::getMarkup($inv_product->markup_c,$value['receive_cost']);
                      $price_code ='price_c';
                    }
                    elseif ($inv_product->price_d == $inv_product->sale_price) {
                      $price=self::getMarkup($inv_product->markup_d,$value['receive_cost']);
                      $price_code ='price_d';
                    }
                    if(!empty($inv_product)){
                      $qty_update=$inv_product->qty_hand+$value['receive_qty'];
                      $p_avg_amnt=$inv_product->qty_hand*$inv_product->avg_cost;
                      $c_avg_amnt=$value['receive_cost'] * $value['receive_qty'];
                      $avg_amnt = ($p_avg_amnt + $c_avg_amnt)/$qty_update;
                      $avg_cost=round($avg_amnt,2);                      
                    }

                    $inv_product->update(['avg_cost'=> @$avg_cost,'qty_hand'=> @$qty_update,'last_cost'=>$value['receive_cost'],'last_date_receive'=>$date_get,$price_code=>@$price,'sale_price'=>@$price]);
                    $save_details_data = InventoryReceiveDetailsModel::create($details_data);
                }
              }
              DB::commit();

              if($save){
                  $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
              }else{
                  $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];
              }

            }else{
                $output = ['status' => 0, 'message' => 'Receive Qty must be greater then zero'];
            }
          } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return response()->json($output);
    }
    public function view(Request $request)
    {
      // $data['viewdata']=InventoryReceiveDetailsModel::join('inv_product_receive','inv_product_receive_details.product_receive_id','=','
      //                                          inv_product_receive_detail.id')
      //                                         ->join('inv_product','inv_product_receive_details.product_id','=','inv_product.inv_code')
      //                                         ->select('inv_product.inv_product_name','inv_product_receive_details.*')
      //                                         ->where('inv_product_receive_details.id',$id)->first();
      // dd($id);

      $data['report_name'] = "Received Inventory Report";
      $data['report_title'] = "";
      $data['reportDate'] = date('F d, Y');

      $data['recieve_data']=InventoryReceiveModel::valid()->where('invoice_number',$request->id)->first();
      $data['recieve_data_details']=InventoryReceiveDetailsModel::join('inv_product','inv_product_receive_details.product_id','=',
                                                        'inv_product.id')
                                                        ->select('inv_product.inv_product_name','inv_product.inv_code','inv_product_receive_details.*')
                                                        ->valid()->where('product_receive_id', $data['recieve_data']['id'])->get() ;
      $data['company_details']=CompanyConfigur::valid()->first();
      // $data['product_name']=InventoryReceiveDetailsModel::
      //                                                   ->valid()
      //                                                   ->where('product_id', $data['recieve_data_details']['id'])->get() ;
      //return response()->json($data);
      return view('report.report-inventory-recive-voucher',$data);
    }

    public function edit($id)
    {

    }

    public function destroy($id)
    {

    }
    public static  function getNumber($num=0){
       if($num>0){
          $number = $num;
       }else{
          $invoice_info = InventoryReceiveModel::valid()->orderBy('invoice_number','desc')->first();
          $number = (!empty($invoice_info)) ? $invoice_info->invoice_number + 1 : 1;
       }
       $numlength = strlen((string)$number);
       if($numlength<4){
          if($numlength ==1){
             $number = '000'.$number;
          }else if($numlength ==2){
             $number = '00'.$number;
          }else{
            $number = '0'.$number;
          }
       }else{
          $number = $number;
       }
       return $number;
   }
   public static function getMarkup($price,$last_cost)
   {
        //$difference=$price-$last_cost;
        //$markup=($difference*100)/$price;
        //return $markup;
        $g_price = ($price/100)*$last_cost;
        $l_price = $g_price + $last_cost;
        return $l_price;

   }


}
