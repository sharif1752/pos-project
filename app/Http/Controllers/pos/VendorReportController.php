<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor;
use Auth;

class VendorReportController extends Controller
{

	public function index(Request $request)
    {

    }

    public function vendorList(Request $request){
        
        $order = $request->input('item_value');
        $data['shorting_name'] = $request->input('shorting_name');
        $data['report_name'] = "Vendor List";
        $data['report_title'] = "Vendor Report";
        $data['reportDate'] = date('F d, Y');

        $data['reports_result'] = Vendor::valid()->orderBy($order,'desc')->get();
        return view('report.vendor-report',$data);
    }

    public function create()
    {
    }
    
    public function store(Request $request)
    {
        
    }

    public function edit(Request $request)
    {

    }

    public function destroy(Request $request)
    {
    

    }
    
    
}
