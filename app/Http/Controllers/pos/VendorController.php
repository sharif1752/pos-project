<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor;
use Auth;
use App\Model\CompanyConfigur;
use App\Model\InventoryItemModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use DB;

class VendorController extends Controller
{
    public function index(Request $request)
    {
        $paginate_num = $request->input('paginate_num');
        $search_key = $request->input('search_key');
        $order = $request->input('order');
        $sort = $request->input('sort');
        $data['paginate_data'] =Vendor::valid()->when($search_key, function($query, $search_key){
                    $query->where(function($query2) use ($search_key){
                        $query2->where('vendor_name','LIKE','%'.$search_key.'%')
                          ->orWhere('vendor_email','LIKE', '%'.$search_key.'%');
                    });
                    return $query;

            })->orderBy($sort,$order)->paginate($paginate_num);

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $validation = [
         'vendor_number' => 'required|unique:inv_vendor,vendor_number,'.$request->id.'id',
         'vendor_name' => 'required'
        ];
        $message=[
          'vendor_name.required' => 'The Company field is required.',
          'vendor_contact_person.required' => 'The Contact Name field is required.',
        ];
        $request->validate($validation,$message);
        $data = $request->only('vendor_name','vendor_contact_person','vendor_address','vendor_phone','vendor_number','vendor_fax','vendor_email','vendor_url','last_date_product_sold','vendor_comment');
        if(!empty($request->id)){
            $save =  Vendor::valid()->findOrFail($request->id)->update($data);
        }else{
            $save =   Vendor::create($data);
        }

        if($save){
            $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
        }else{
            $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];
        }
        return response()->json($output);
    }

    public function edit(Request $request)
    {

      $vedor = Vendor::valid()->findOrFail($request->id);

      $company_config=CompanyConfigur::first();
      $vedor->company_config =$company_config;
      $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();
      //dd($last_shift);
      $get_vendor_product = InventoryItemModel::valid()->where('vendor_id',$vedor->id)->pluck('id')->all();

      //dd($get_vendor_product);

      if($last_shift){
            $sales_nontaxable_shift = InventoryPosDetailsModel::valid()
            ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
            ->select(DB::raw('inv_pos_details.product_qty*inv_pos_details.product_price as sub_total'))
            ->where('inv_product.service','=', '0')
            ->where('inv_pos_details.created_at','>=',$last_shift->start_time)
            ->where('inv_pos_details.created_at','<=',$last_shift->end_time)
            ->whereIn('inv_pos_details.product_id',$get_vendor_product)
            ->get()
            ->sum('sub_total');
            $vedor->shift = $sales_nontaxable_shift;
        }else{
            $vedor->shift = 0;
        }
        return response($vedor);
    }

    public function destroy(Request $request)
    {

        $vendor = Vendor::valid()->findOrFail($request->id);

        $get_product = InventoryPosDetailsModel::valid()->groupBy('product_id')->pluck('product_id')->all();

        $get_vendor_product = InventoryItemModel::valid()->whereIn('id',$get_product)->groupBy('vendor_id')->get()->keyBy('vendor_id')->all();

        if(array_key_exists($request->id,$get_vendor_product)){
            $message=['status' => 0, 'message' => 'This vendor has transection'];
        }else{
            if($vendor->delete()){
                return response(['status' => 1, 'message' => 'Your data is successfully deleted']);
            }
        }

        return response($message);

    }

    public function create()
    {
      $company_config=CompanyConfigur::first();
      $data['company_config'] =$company_config;
        return response($data);
    }

}
