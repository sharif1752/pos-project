<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryItemModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use App\Model\InventoryCategoryModel;
use App\Model\CompanyConfigur;
use App\Model\Customer;
use App\Model\Vendor;
use Auth;
use DB;

class DashboardController extends Controller
{

	public function index(Request $request)
    {
			 // DB::enableQueryLog();
			 //date according to date get data
			 $date_get=date('Y-m-d');
			 $year_start=date('Y').'-'.'01'.'-'.'01';
			 $monthly_start=date('Y').'-'.date('m').'-'.'01';

			 $pos_details_data=InventoryPosDetailsModel::join('inv_product','inv_product.id','=','inv_pos_details.product_id')
					->select(DB::raw('inv_pos_details.product_price * inv_pos_details.product_qty + (inv_pos_details.product_price *(inv_pos_details.product_tax/100)) * inv_pos_details.product_qty  as sale_total'),'inv_pos_details.date','inv_pos_details.product_id',DB::raw('inv_product.last_cost* inv_pos_details.product_qty +inv_pos_details.product_discount as last_purchase_total'),DB::raw('inv_product.avg_cost * inv_pos_details.product_qty+inv_pos_details.product_discount as avg_purchase_total'))
					->whereBetween('date',[$year_start,$date_get])
					->get();
 		 	 $pos_all_data=InventoryPosModel::whereBetween('date',[$year_start,$date_get])->get();
			 $this_year_sale=collect($pos_details_data)->pluck('product_id');
			 $sale_product=InventoryItemModel::whereIn('id',$this_year_sale)->get();
			 $configure_data=CompanyConfigur::where('id',1)->first();
			 //daily sale data
			 $daily_sell_product=collect($pos_details_data)->where('date',$date_get)->sum('sale_total');
			 if($daily_sell_product>0){
					$data['daily_sale']=$daily_sell_product;
			 }
			 else{
					$data['daily_sale']=0;
				}

			 //monthly sale
			 $monthly_sell_product=collect($pos_details_data)->whereBetween('date',[$monthly_start,$date_get])->sum('sale_total');
			 if($monthly_sell_product>0){
					$data['monthly_sale']=$monthly_sell_product;
			}
			else{
					$data['monthly_sale']=0;
			 }
			 //yearly Sale
			 $yearly_sell_product=collect($pos_details_data)->sum('sale_total');
			 if($yearly_sell_product>0) {
				 $data['yearly_sale']=$yearly_sell_product;

			 }
			 else {
			  $data['yearly_sale']=0;
			 }

       //cash account Sale
				$data['cash_sale']=collect($pos_all_data)->where('customer_name','==',null)->sum('sub_total');
				$data['receivable']=collect($pos_all_data)->where('customer_name','!==',null)->sum('sub_total');

			//profit loss
      $month_date=date('m');
			$profit=[];
			$loss=[];

			for($i=1;$i<=$month_date;$i++)
			{
				$month_start_date=date('Y').'-'.'0'.$i.'-'.'01';
				$month_end_date=date("Y-m-t", strtotime($month_start_date));
				$total_revenue=collect($pos_details_data)->whereBetween('date',[$month_start_date,$month_end_date])
																										 ->sum('sale_total');
				if($configure_data->calculate_profit_type=1){
          $purchase_total=collect($pos_details_data)->whereBetween('date',[$month_start_date,$month_end_date])
                                                      		->sum('last_purchase_total');

        }
        else {
          $purchase_total=collect($pos_details_data)->whereBetween('date',[$month_start_date,$month_end_date])
                                                       ->sum('avg_purchase_total');
        }
			  $loss_profit_total=($total_revenue - $purchase_total);

				$m=date('m',strtotime($month_start_date));
			  $d=date('d',strtotime($month_start_date));
				$y=date('Y',strtotime($month_start_date));
				$incremnt=$i-1;
				if($loss_profit_total>0){
					$profit[$i]=['amount'=>$loss_profit_total,'m'=>$m,'d'=>$d,'y'=>$y,'increment'=>$incremnt];
					$loss[$i]=['amount'=>0,'m'=>$m,'d'=>$d,'y'=>$y,'increment'=>$incremnt];
				}
				else {
					$loss[$i]=['amount'=>abs($loss_profit_total),'m'=>$m,'d'=>$d,'y'=>$y,'increment'=>$incremnt];
					$profit[$i]=['amount'=>0,'m'=>$m,'d'=>$d,'y'=>$y,'increment'=>$incremnt];
				}


			}

			$data['profit']=$profit;
			$data['loss']=$loss;
			 //first block all total data
			 $data['daily_sale']=$daily_sell_product;
			 $data['total_sold_item']=collect($sale_product)->sum('total_sold_c');
			 $data['total_category']=count(InventoryCategoryModel::all());
			 $data['total_vendor']=count(Vendor::all());
			 $data['total_product']=InventoryItemModel::valid()->select(DB::raw('IFNULL(sum(qty_hand),0) as totalQty'))->first()->totalQty;
			 $data['monthly_sale']=$monthly_sell_product;
			 $data['total_sale']=collect($sale_product)->sum('total_revenue_c');
			 $data['total_profit']=collect($sale_product)->sum('total_profit_c');
			 $data['best_seller']=InventoryPosDetailsModel::valid()
                    ->select('inv_product.inv_product_name','inv_pos_details.product_id', DB::raw('IFNULL(SUM(inv_pos_details.product_qty),0) as total_sold_c'))
                    ->join('inv_product', 'inv_pos_details.product_id', '=', 'inv_product.id')
                    ->whereBetween('date',[$year_start,$date_get])
                    ->orderBy('total_sold_c','desc')->groupBy('product_id')->limit(10)->get();
			$data['worst_seller']=InventoryPosDetailsModel::valid()
                    ->select('inv_product.inv_product_name','inv_pos_details.product_id', DB::raw('IFNULL(SUM(inv_pos_details.product_qty),0) as total_sold_c'))
                    ->join('inv_product', 'inv_pos_details.product_id', '=', 'inv_product.id')
                    ->whereBetween('date',[$year_start,$date_get])
                    ->orderBy('total_sold_c','asc')->groupBy('product_id')->limit(10)->get();

            $data['sell_by_category']=InventoryPosDetailsModel::valid()
                    ->select('inv_product_category.category_name','inv_product.category_id', DB::raw('IFNULL(SUM(inv_pos_details.product_qty),0) as total_sold'))
                    ->join('inv_product', 'inv_pos_details.product_id', '=', 'inv_product.id')
                    ->join('inv_product_category', 'inv_product.category_id', '=', 'inv_product_category.id')
                    ->whereBetween('date',[$year_start,$date_get])
                    ->orderBy('total_sold','desc')->groupBy('category_id')->limit(5)->get();

			 $data['dashboard_data']=1;

			 // print_r(DB::getQueryLog());


       return response()->json($data);
    }



    public function create()
    {
    }

    public function store(Request $request)
    {

    }

    public function edit(Request $request)
    {

    }

    public function destroy(Request $request)
    {


    }


}
