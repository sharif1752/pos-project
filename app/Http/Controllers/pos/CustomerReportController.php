<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;
use Auth;

class CustomerReportController extends Controller
{

	public function index(Request $request)
    {

    }

    public function customerList(Request $request){
        
        $order = $request->input('item_value');
        $data['shorting_name'] = $request->input('shorting_name');
        $data['report_name'] = "Customer List";
        $data['report_title'] = "Customer Report";
        $data['reportDate'] = date('F d, Y');

        $data['reports_result'] = Customer::valid()->orderBy($order,'asc')->get();
        return view('report.customer-report',$data);
    }

    public function create()
    {
    }
    
    public function store(Request $request)
    {
        
    }

    public function edit(Request $request)
    {

    }

    public function destroy(Request $request)
    {
    

    }
    
    
}
