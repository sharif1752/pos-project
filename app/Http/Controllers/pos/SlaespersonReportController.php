<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SalesPersonModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use App\Model\SalesPersonShiftModel;
use App\Model\InvArTransactionModel;
use DB;
use Auth;
use DateTime;
use Carbon;

class SlaespersonReportController extends Controller
{

	public function index(Request $request)
    {
        $data['salespersons']=array();
        //$data['salespersons'][0]=['id'=>'','text'=>"--All--"];
        $data['salespersons_value'] =[];
        $salespersons = SalesPersonModel::valid()->where('role_id',1)->get();
        foreach ($salespersons as $value) {
            array_push($data['salespersons'],['id'=>$value['id'],'text'=>$value['name']]);
        }
        return response($data);
    }

    public function salespersonList(Request $request){
        
        $order = $request->input('item_value');
        $data['shorting_name'] = $request->input('shorting_name');
        $data['report_name'] = "Salesperson List";
        $data['report_title'] = "Salesperson Report";
        $data['reportDate'] = date('F d, Y');

        $data['reports_result'] = SalesPersonModel::valid()->orderBy($order,'asc')->get();
        return view('report.saplesperson-report',$data);
    }

    public function getSalesperson(Request $request){
        $current_date =$month_end_date=$year_end_date= date('Y-m-d');
        $date_explode= explode('-', $current_date);
        $month_start_date = $date_explode[0]."-".$date_explode[1]."-"."01";
        $year_start_date = $date_explode[0]."-"."01"."-"."01";

        $data['report_name'] = "Salesperson Sales";
        $data['report_title'] = "Salesperson Report";
        $data['reportDate'] = date('F d, Y');
        $data['id']= $request->input('by_id');

        $data['salespersons'] = SalesPersonModel::valid()->get()->keyBy('id')->all();

        $invoice_query=InventoryPosModel::valid()->get();

        if($data['id']){
            $total_invoice_daily =$invoice_query->where('date',$current_date)->where('created_by',$request->input('by_id'))->all();
            $total_invoice_monthly =$invoice_query->whereBetween('date', [$month_start_date, $current_date])->where('created_by',$request->input('by_id'))->all();
            $total_invoice_yearly =$invoice_query->whereBetween('date', [$year_start_date, $current_date])->where('created_by',$request->input('by_id'))->all();
            //total invoice
            $data['personsales']['invoice'] =[
            'title' => 'Total Invoice',
            'daily' => count($total_invoice_daily),
            'monthly' => count($total_invoice_monthly),
            'yearly' => count($total_invoice_yearly)
            ];
            //total sales
            $data['personsales']['sales'] =[
            'title' => 'Total Sales',
            'daily' => $data['salespersons'][$request->input('by_id')]->total_sales_1,
            'monthly' => $data['salespersons'][$request->input('by_id')]->total_sales_2,
            'yearly' => $data['salespersons'][$request->input('by_id')]->total_sales_3
            ];
            //cash sales
            $cash_sales_query = InventoryPosModel::valid()
                                ->select('inv_pos.*',DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                                ->whereNull('customer_name')
                                ->where('created_by',$request->input('by_id'))
                                ->where('pay_type','=','Cash')
                                ->get();
            $paytype_query_d =$cash_sales_query->where('date',$current_date)->sum('cash_sub_total');

            $paytype_query_m =$cash_sales_query->whereBetween('date', [$month_start_date, $current_date])
                            ->sum('cash_sub_total');

            $paytype_query_y =$cash_sales_query->whereBetween('date', [$year_start_date, $current_date])
                             ->sum('cash_sub_total');

            $data['personsales']['cash'] = [
                'title' => 'Cash',
                'daily' => $paytype_query_d,
                'monthly' => $paytype_query_m,
                'yearly' => $paytype_query_y,
            ];

            //account reciveable
            $account_receiveable_query =InventoryPosModel::valid()
                                    ->select('inv_pos.*',DB::raw('inv_pos.total_due as ar_sub_total'))
                                    ->where('created_by',$request->input('by_id'))
                                    ->whereNotNull('customer_name')
                                    ->get();

            $total_invoice_due_daily =$account_receiveable_query
                                ->where('date',$current_date)
                                ->sum('ar_sub_total');
            $total_invoice_monthly =$account_receiveable_query
                            ->whereBetween('date', [$month_start_date, $current_date])
                            ->sum('ar_sub_total');
            $total_invoice_yearly =$account_receiveable_query
                            ->whereBetween('date', [$year_start_date, $current_date])
                            ->sum('ar_sub_total');

            $data['personsales']['account_receiveable']= [
                'title' => 'Account Receiveable',
                'daily' => $total_invoice_due_daily,
                'monthly' => $total_invoice_monthly,
                'yearly' => $total_invoice_yearly,
            ];

            //received on account
            $recieive_on_query =InvArTransactionModel::valid()
                            ->where('ar_payment','!=','0')
                            ->where('created_by',$request->input('by_id')) 
                            ->get();

            $account_received_d = $recieive_on_query
            ->where('date',$current_date)
            ->sum('ar_payment');

            $account_received_m = $recieive_on_query
            ->whereBetween('date', [$month_start_date, $current_date])
            ->sum('ar_payment');

            $account_received_y = $recieive_on_query
            ->whereBetween('date', [$year_start_date, $current_date])
            ->sum('ar_payment');

            $data['personsales']['ar'] =[
                'title' => 'Rceived on Account(AR)',
                'daily' => $account_received_d,
                'monthly' => $account_received_m,
                'yearly' => $account_received_y
            ];

            //shift wise sale process
            $get_shift_start = DB::table('inv_salesperson_shift')
                                ->select('start_time','end_time')
                                ->where(DB::raw('CAST(end_time AS DATE)'),$current_date)
                                ->where('sales_id',$request->input('by_id'))
                                ->where('status',0)
                                ->where('valid',1)
                                ->get();

            $shift_querys = InventoryPosModel::valid()->where(DB::raw('CAST(created_at AS DATE)'),$current_date)->get();

            $data['get_time'] = array();
            $data['shifting_time'] = array();

            foreach ($get_shift_start as $value) {

                $shift_query = $shift_querys->where('created_at','>=',$value->start_time)->where('created_at','<=',$value->end_time)->all();

                $shit_start = explode(' ',$value->start_time);
                $shit_end = explode(' ',$value->end_time); 
                $range_time=$shit_start[1].' -To- '.$shit_end[1];
                array_push($data['get_time'],$range_time); 
                array_push($data['shifting_time'],$shift_query); 
            }
            //return $data['shifting_time'];
        }else{
           //$data['personsales']['invoice']=InventoryPosModel::valid()->get()->groupBy('created_by')->all();
        }
        
        //return $data;
        return view('report.SalesPersonSales-report',$data);
    }

    public function create()
    {
    }
    
    public function store(Request $request)
    {
        
    }

    public function edit(Request $request)
    {

    }

    public function destroy(Request $request)
    {
    

    }
    
    
}
