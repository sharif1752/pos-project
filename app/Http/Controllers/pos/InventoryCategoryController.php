<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\InventoryCategoryModel;
use App\Model\MenuTable;

class InventoryCategoryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){
      $paginate_num = $request->input('paginate_num');
      $search_key = $request->input('search_key');
      $order = $request->input('order');
      $sort = $request->input('sort');
      $data['paginate_data'] =InventoryCategoryModel::valid()->when($search_key, function($query, $search_key){
                  $query->where(function($query2)use($search_key){
                      $query2->where('category_name','LIKE','%'.$search_key.'%');
                  });
                  return $query;

          })->orderBy($sort,$order)->paginate($paginate_num);

      return response()->json($data);

    }

    public function create(){

    }

    public function store(Request $request)
    {
         $validate=[
           'category_name'=>'required'
         ];
         $request->validate($validate);
         $data=$request->only('category_name');
         if(!empty($request->id))
         {
           $update_data=InventoryCategoryModel::valid()->findOrFail($request->id);
           $save_data=$update_data->update($data);
           $message=['status' => 1, 'message' => 'Your data is successfully updated'];
         }
         else {
            $save_data=InventoryCategoryModel::create($data);
            $message=['status' => 1, 'message' => 'Your data is successfully saved'];
         }

         if(!$save_data)

         {
           $message=['status' => 0, 'message' => 'Ops! Something went worng.'];

         }
       return response($message);





    }

    public function edit($id)
    {
      $edit_data=InventoryCategoryModel::valid()->findOrFail($id);
      return response($edit_data);

    }

    public function destroy($id)
     {
      $delete_data=InventoryCategoryModel::valid()->findOrFail($id);
      if($delete_data->delete())
      {
        $message=['status' => 1, 'message' => 'Your data is successfully deleted'];
      }
      return response($message);
    }









}
