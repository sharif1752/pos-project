<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\InventoryCategoryModel;
use App\Model\InventoryItemModel;
use App\Model\InvArTransactionModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use App\Model\CompanyConfigur;
use App\Model\Customer;
use App\Model\SalesPersonModel;
use App\Model\Vendor;
use DB;

class InventoryPosController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // private $save_id='check';
    public function index(Request $request){


    }

    public function create(){
      $product_data=collect(InventoryItemModel::valid()->where('inv_code','!=','')->get());
      $customer_data=Customer::valid()->get();
      $data['all_product']=$product_data->keyBy('inv_code')->all();
      $data['product_select']=array();
      $data['customer_data']=array();
      $data['invoice_number'] = self::getNumber();
      foreach ($product_data as $value) {
          array_push($data['product_select'],['id'=>$value['inv_code'],'text'=>$value['inv_product_name']]);
      }
      foreach ($customer_data as $value) {
          array_push($data['customer_data'],['id'=>$value['id'],'text'=>$value['customer_name']]);
      }

      $data['add_row'] = ['0' =>['id'=>0,'product_id'=>'','product_qty'=>'','product_name'=>'','product_qty_hand'=>'','product_price'=>'','product_amount'=>'']];

      return response()->json($data);
    }

    public function store(Request $request)
    {
      DB::beginTransaction();
      try {

         $validate=[
           'invoice_number'=>'required',
           'sub_total'=>'required',
         ];
         
          $msg=[];
          $check=true;
          $row_product_id=collect($request->add_row)->pluck('product_tbl_id');
          $product_data=InventoryItemModel::valid()->whereIn('id',$row_product_id)->get()->keyBy('id')->all();
          $add_row_group=collect($request->add_row)->groupBy('product_tbl_id')->all();
          foreach ($add_row_group as $k => $value):    // stock check
            if($k>0){
              $inv_product=$product_data[$k];
              $product_qty=$value->sum('product_qty');   
              if($product_qty>$inv_product->qty_hand || $inv_product->qty_hand==0 )
              {
                $msg[] = [
                'Insufficient Stock For '.$inv_product->inv_product_name
                ];
                $check=false;
              }
            }
          endforeach;
          if($check){
           if($request->pay_type == 'Account Receivable'){
               $validate=array_add( $validate,'customer_name','required');
           }
           $request->validate($validate);

           $data=$request->only('invoice_number','sub_total','sales_tax','total_discount','pay_type','total_pay','comment');
           // dd($data);
           $date_get=date('Y-m-d');
           $monthly_start=date('Y').'-'.date('m').'-'.'01';
           $year_start=date('Y').'-'.'01'.'-'.'01';
           if($request->pay_type == 'Account Receivable'){
              $data=array_add($data,'customer_name',$request->customer_name);
              $data=array_add($data,'total_due',$request->total_pay);
              $data=array_set($data,'total_pay',0);
            }

           else {
              $data=array_add($data,'total_due',0);
            }
           $data=array_add($data,'date', $date_get);

           $save =InventoryPosModel::create($data);

           if($request->customer_name !=null){
             //for total sale calculation in customer table call function
             self::customertotalSale($request->customer_name,$date_get,$monthly_start,$year_start);
             $ar_sale=$request->sub_total;
             if($request->sales_tax !=null){
                $ar_sale=$ar_sale+$request->sales_tax;
             }
             if($request->total_discount !=''){
                $ar_sale=$ar_sale-$request->total_discount;
             }
             // else {
             //
             // }
             $ar_details=[
                'pos_id'=>$save->id,
                'customer_id'=>$data['customer_name'],
                'date'=>$data['date'],
                'ar_sale'=>$ar_sale,
                'ar_payment'=>$data['total_pay'],

              ];

             $ar_transaction=InvArTransactionModel::create($ar_details);

           }
           //for total sale calculation in salesperson table call function
           self::salespersontaotalSale($save->created_by,$date_get,$monthly_start,$year_start);
           // $product_collect=collect($product_data);           
            foreach ($request->add_row as $value):

              if(!empty($value['product_id'])&&!empty($value['product_qty'])&&!empty($value['product_price']) && !empty($value['product_tbl_id']))
              {

                $product_update=InventoryItemModel::valid()->where('id',$value['product_tbl_id'])->first();

                $details_data=[
                 'product_id'=>$value['product_tbl_id'],
                 'date'=>$date_get,
                 'product_qty'=>$value['product_qty'],
                 'product_price'=>$value['product_price'],
                 'product_tax'=>$value['product_tax'],
                 'product_discount'=>$value['product_discount'],
                 'pos_id'=>$save->id,

                ];
                $qty_update=$product_update->qty_hand-$value['product_qty'];
                $product_update->update(['qty_hand'=> $qty_update]);
                $product_update->update(['last_date_sold'=>$date_get]);

                $company_info=CompanyConfigur::valid()->first();

                if($company_info->calculate_profit_type == '1'){
                   $details_data = array_add($details_data,'product_cost_price',$value['last_cost']);
                }else{
                   $details_data = array_add($details_data,'product_cost_price',$value['avg_cost']);
                }

                $save_details_data = InventoryPosDetailsModel::create($details_data);
                self::inventorysaleTotal($value['product_tbl_id'],$date_get,$monthly_start,$year_start);
                self::vendorsaleTotal($value['product_vendor'],$date_get,$monthly_start,$year_start);
                self::totalRevenueProfit($value['product_tbl_id'],$date_get,$monthly_start,$year_start);
            }
            endforeach;
            DB::commit();
            $output = ['status' => 1, 'message' => 'Your data is successfully saved','save_id'=> $save->id];
          }else{
            $output = ['status' => 0, 'message' =>$msg];
          }
        } catch (Throwable $e) {
          DB::rollback();
          throw $e;
      }

      return response()->json($output);
    }
    public function posValue(Request $request)
    {

      $data['report_name'] = "Invoice";
      $data['report_title'] = "";
      if(!empty($request->date)){
        $data['reportDate'] = $request->date;
      }else{
        $data['reportDate'] = date('F d, Y');
      }

      $data['company_details']=CompanyConfigur::valid()->first();
      $data['fetch_data']=InventoryPosModel::valid()->where('id',$request->id)->first();
      $data['sales_perrson_name'] = SalesPersonModel::where('id',$data['fetch_data']->created_by)->first();
      $data['data_value']=InventoryPosDetailsModel::valid()->
        join('inv_product','inv_pos_details.product_id','=','inv_product.id')
        ->select('inv_product.inv_product_name','inv_product.inv_code','inv_pos_details.*')
        ->where('pos_id',$request->id)->get();
      //return $data['sales_perrson_name'];
      return view('report.report-pos-voucher',$data);
    }

    public static  function getNumber($num=0){
       if($num>0){
          $number = $num;
       }else{
          $invoice_info = InventoryPosModel::valid()->orderBy('invoice_number','desc')->first();
          $number = (!empty($invoice_info)) ? $invoice_info->invoice_number + 1 : 1;
       }
       $numlength = strlen((string)$number);
       if($numlength<4){
          if($numlength ==1){
             $number = '000'.$number;
          }else if($numlength ==2){
             $number = '00'.$number;
          }else{
            $number = '0'.$number;
          }
       }else{
          $number = $number;
       }
       return $number;
   }
   public static function customertotalSale($customer_id,$daily_date,$monthly_start,$year_start)
   {

     $post_data=InventoryPosModel::where('customer_name',$customer_id)
                                  ->whereBetween('date',[$year_start,$daily_date])
                                  ->get();
     $customer_data=Customer::where('id',$customer_id)->first();

     $daily_sale_total=collect($post_data)->where('customer_name',$customer_id)
                                    ->where('date',$daily_date)
                                    ->sum('sub_total');
     $customer_data->update(['customer_purchage_1'=>$daily_sale_total]);

     $monthly_sale_total=collect($post_data)->where('customer_name',$customer_id)
                                   ->whereBetween('date',[$monthly_start,$daily_date])
                                   ->sum('sub_total');
     $customer_data->update(['customer_purchage_2'=>$monthly_sale_total]);

     $yearly_sale_total=collect($post_data)->where('customer_name',$customer_id)
                                   ->whereBetween('date',[$year_start,$daily_date])
                                   ->sum('sub_total');
     $customer_data->update(['customer_purchage_3'=>$yearly_sale_total]);

   }
   public static function salespersontaotalSale($person_id,$daily_date,$monthly_start,$year_start)
   {
     $post_data=InventoryPosModel::where('created_by',$person_id)
                                  ->whereBetween('date',[$year_start,$daily_date])
                                  ->get();

     $salesperson_data=SalesPersonModel::where('id',$person_id)->first();

     $daily_sale_total=collect($post_data)->where('created_by',$person_id)
                                    ->where('date',$daily_date)
                                    ->sum('sub_total');
     $daily_commission_total=$daily_sale_total*($salesperson_data->commission_percentage/100);
     $salesperson_data->update(['total_sales_1'=>$daily_sale_total,'commission_earned_1'=>$daily_commission_total]);

     $monthly_sale_total=collect($post_data)->where('created_by',$person_id)
                                   ->whereBetween('date',[$monthly_start,$daily_date])
                                   ->sum('sub_total');
     $monthly_commission_total=$monthly_sale_total*($salesperson_data->commission_percentage/100);
     $salesperson_data->update(['total_sales_2'=>$monthly_sale_total,'commission_earned_2'=>$monthly_commission_total]);

     $yearly_sale_total=collect($post_data)->where('created_by',$person_id)
                                   ->whereBetween('date',[$year_start,$daily_date])
                                   ->sum('sub_total');
     $yearly_commission_total=$yearly_sale_total*($salesperson_data->commission_percentage/100);
     $salesperson_data->update(['total_sales_3'=>$yearly_sale_total,'commission_earned_3'=>$yearly_commission_total]);
   }
   public static function inventorysaleTotal($product_id,$daily_date,$monthly_start,$year_start)
   {
     $post_details_data=InventoryPosDetailsModel::where('product_id',$product_id)
                                                ->select('product_qty','date','product_id')
                                                ->whereBetween('date',[$year_start,$daily_date])
                                                ->get();
     $item_data=InventoryItemModel::where('id',$product_id)->first();

     $daily_sale_total=collect($post_details_data)->where('product_id',$product_id)
                                    ->where('date',$daily_date)
                                    ->sum('product_qty');

     $item_data->update(['total_sold_a'=>$daily_sale_total]);
     $monthly_sale_total=collect($post_details_data)->where('product_id',$product_id)
                                   ->whereBetween('date',[$monthly_start,$daily_date])
                                   ->sum('product_qty');

     $item_data->update(['total_sold_b'=>$monthly_sale_total]);
     $yearly_sale_total=collect($post_details_data)->where('product_id',$product_id)
                                   ->whereBetween('date',[$year_start,$daily_date])
                                   ->sum('product_qty');
     $item_data->update(['total_sold_c'=>$yearly_sale_total]);
   }
   public static function vendorsaleTotal($vendor_id,$daily_date,$monthly_start,$year_start)
   {
        $post_details_data = InventoryPosDetailsModel::select(DB::raw('product_price * product_qty as sale_total'),'date','product_id')
                                                     ->whereBetween('date',[$year_start,$daily_date])
                                                     ->get();
        $iteam_get_by_vendor=InventoryItemModel::where('vendor_id',$vendor_id)->get()->pluck('id');
        $vendor_data=Vendor::where('id',$vendor_id)->first();
        $vendor_wise_pos_details =collect($post_details_data)->whereIn('product_id',$iteam_get_by_vendor);
        $daily_sale_total=collect($vendor_wise_pos_details)->where('date',$daily_date)
                                            ->sum('sale_total');
        if($vendor_data){
          $vendor_data->update(['total_vendor_sales_1'=>$daily_sale_total]);
          $monthly_sale_total=collect($vendor_wise_pos_details)->whereBetween('date',[$monthly_start,$daily_date])
                                       ->sum('sale_total');
          $vendor_data->update(['total_vendor_sales_2'=>$monthly_sale_total]);
          $yearly_sale_total=collect($vendor_wise_pos_details)->whereBetween('date',[$year_start,$daily_date])
                                         ->sum('sale_total');
          $vendor_data->update(['total_vendor_sales_3'=>$yearly_sale_total]);
        }

   }
   public static function totalRevenueProfit($product_id,$daily_date,$monthly_start,$year_start)
   {
        $item_data=InventoryItemModel::where('id',$product_id)->first();
        $post_details_data = InventoryPosDetailsModel::join('inv_product','inv_product.id','=','inv_pos_details.product_id')
                                                     ->where('inv_pos_details.product_id',$product_id)
                                                     ->select(DB::raw('IFNULL(inv_pos_details.product_price,0) * IFNULL(inv_pos_details.product_qty,0) + (IFNULL(inv_pos_details.product_price,0) *(IFNULL(inv_pos_details.product_tax,0)/100)) * IFNULL(inv_pos_details.product_qty,0)  as sale_total'),'inv_pos_details.date','inv_pos_details.product_id',DB::raw('IFNULL(inv_product.last_cost,0)* IFNULL(inv_pos_details.product_qty,0) + IFNULL(inv_pos_details.product_discount,0) as last_purchase_total'),DB::raw('IFNULL(inv_pos_details.product_cost_price,0) * IFNULL(inv_pos_details.product_qty,0)+ IFNULL(inv_pos_details.product_discount,0) as avg_purchase_total'))
                                                     ->whereBetween('inv_pos_details.date',[$year_start,$daily_date])
                                                     ->valid()
                                                     ->get();

        $daily_revenue_total=collect($post_details_data)->where('date',$daily_date)
                                                     ->sum('sale_total');
        $configure_data=CompanyConfigur::where('id',1)->first();
        if($configure_data->calculate_profit_type==1){
          $daily_purchase_total=collect($post_details_data)->where('date',$daily_date)
                                                      ->sum('last_purchase_total');

        }
        else {
          $daily_purchase_total=collect($post_details_data)->where('date',$daily_date)
                                                       ->sum('avg_purchase_total');
        }



        $daily_profit_total=($daily_revenue_total - $daily_purchase_total);
        $item_data->update(['total_revenue_a'=>$daily_revenue_total,'total_profit_a'=>$daily_profit_total]);


        $monthly_revenue_total=collect($post_details_data)->whereBetween('date',[$monthly_start,$daily_date])
                                                          ->sum('sale_total');
        if($configure_data->calculate_profit_type==1){
            $monthly_purchase_total=collect($post_details_data)->whereBetween('date',[$monthly_start,$daily_date])
                                                               ->sum('last_purchase_total');
        }
        else {
            $monthly_purchase_total=collect($post_details_data)->whereBetween('date',[$monthly_start,$daily_date])
                                                             ->sum('avg_purchase_total');
        }
        $monthly_profit_total=($monthly_revenue_total - $monthly_purchase_total);

        $item_data->update(['total_revenue_b'=>$monthly_revenue_total,'total_profit_b'=>$monthly_profit_total]);

        $yearly_revenue_total=collect($post_details_data)->sum('sale_total');

        if($configure_data->calculate_profit_type==1){
            $yearly_purchase_total=collect($post_details_data)->sum('last_purchase_total');
        }
        else {
            $yearly_purchase_total=collect($post_details_data)->sum('avg_purchase_total');
        }
        $yearly_profit_total=($yearly_revenue_total - $yearly_purchase_total);
        $item_data->update(['total_revenue_c'=>$yearly_revenue_total,'total_profit_c'=>$yearly_profit_total]);

   }


}
