<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\InventoryPosModel;
use App\Model\InvArTransactionModel;
use App\Model\Customer;
use DB;

class InventoryArPostController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){
      $paginate_num = $request->input('paginate_num');
      $search_key = $request->input('search_key');
      $order = $request->input('order');
      $sort = $request->input('sort');
      $data['paginate_data'] =InventoryPosModel::valid()
                 ->whereNotNull('total_due')
                 ->join('inv_customer','inv_pos.customer_name','=','inv_customer.id')
                 ->select('inv_customer.customer_name' ,'inv_customer.id','inv_customer.customer_number','inv_customer.customer_phone','inv_pos.customer_name AS customer_id',DB::raw('IFNULL(sum(sub_total),0) AS total_sub'),DB::raw('IFNULL(sum(sales_tax),0) AS total_tax'),DB::raw('IFNULL(sum(total_discount),0) AS discount_total'),DB::raw('IFNULL(sum(total_due),0) AS due_balance'),DB::raw('IFNULL(sum(total_pay),0) AS payment'))
                 ->whereNotNull('inv_pos.customer_name')
                 ->where('inv_pos.pay_type','Account Receivable')
                 ->where('total_due','!=',0)
                 ->when($search_key, function($query, $search_key){
                   $query->where(function($query2)use($search_key){
                   $query2->where('inv_customer.customer_name','LIKE','%'.$search_key.'%')
                      ->where('total_due','!=',0)
                      ->where('pay_type','Account Receivable')
                      ->orWhere('inv_customer.customer_number','LIKE','%'.$search_key.'%');
                   });
                   return $query;

           })->groupBy('inv_pos.customer_name')->orderBy($sort,$order)->paginate($paginate_num);
      return response()->json($data);

    }
    public function edit(Request $request)
    {

      DB::beginTransaction();
      try{

        $customer_id=$request['customer_details']['id'];
        $payment=$request->pay_amount;
        $total_due = floatval(str_replace(',','', $request['payment_details']['total_due']));
        $date_get=date('Y-m-d');
        if(empty($payment))
        {
          $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];
        }
        else
        {
          if($payment <= $total_due){
            //update process
            $payment_details=InventoryPosModel::valid()
                                              ->whereNotNull('total_due')
                                              ->where('total_due','>','0')
                                              ->where('pay_type','Account Receivable')
                                              ->where('customer_name',$customer_id)
                                              ->get();

            foreach ($payment_details as $payment_fetch) {
                    $payment_fetch=collect($payment_details)->where('id',$payment_fetch->id)->first();
                    if($payment >= $payment_fetch->total_due){
                      $payment -= $payment_fetch->total_due;
                      $total_pay=$payment_fetch->total_due+$payment_fetch->total_pay;
                      $ar_pay=$payment_fetch->total_due;
                      $payment_fetch->update(['total_pay' =>$total_pay, 'total_due' => 0, 'pay_type'=> 'Cash']);
                    }else{
                        if($payment>0){
                            $total_due = $payment_fetch->total_due - $payment;
                            $total_pay=$payment_fetch->total_pay+$payment;
                            $ar_pay=$payment;
                            $payment_fetch->update(['total_pay' =>$total_pay, 'total_due' => $total_due, 'pay_type'=> 'Account Receivable']);
                            $payment = 0;
                        }

                    }

                  $ar_tran=new InvArTransactionModel;
                  $ar_tran->pos_id=$payment_fetch->id;
                  $ar_tran->customer_id=$payment_fetch->customer_name;
                  $ar_tran->date=$date_get;
                  $ar_tran->ar_payment=$ar_pay;
                  $ar_tran->save();
                  if($payment==0)
                  {
                    break;
                  }


            }
            $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
            DB::commit();

          }else{
            $output = ['status' => 0, 'message' => 'Pay amount should not be grater then due amount'];
          }
        }
      } catch (Throwable $e) {
          DB::rollback();
          throw $e;
      }

      return response()->json($output);

    }

    public function create(){

    }


    //
    // public function edit($id)
    // {
    //   // $edit_data=InventoryCategoryModel::valid()->findOrFail($id);
    //   $due_details=InventoryPosModel::valid()->whereNotNull('total_due')->where('pay_type','Account Receivable')->where('customer_name',$id)->get();
    //   $data['customer_details']=Customer::valid()->where('id',$id)->select('customer_number','customer_name','customer_contact','customer_address','customer_city','customer_state','customer_zip','customer_phone')->first();
    //   $total_sale=(collect($due_details)->sum('sub_total')+collect($due_details)->sum('sales_tax'))-collect($due_details)->sum('total_discount');
    //   $total_due=collect($due_details)->sum('total_due');
    //   $total_pay=collect($due_details)->sum('total_pay');
    //   $data['payment_details']=[
    //         'total_sale'=>$total_sale,
    //         'total_due'=>$total_due,
    //         'total_pay'=>$total_pay,
    //   ];
    //
    //  return response()->json($data);
    //
    //   // $data['basic_details'] =InventoryPosModel::valid()
    //   //           ->join('inv_customer','inv_pos.customer_name','=','inv_customer.id')
    //   //           ->select('inv_customer.customer_name','inv_customer.customer_number','inv_customer.customer_address','inv_customer.customer_city','inv_customer.customer_zip','inv_customer.customer_state','inv_customer.customer_phone', 'inv_pos.*')
    //   //           ->where('inv_pos.customer_name',$id)->();
    // // $data['invoice_details'] =
    //
    //   // ->select('inv_pos.sub_total', DB::raw('sum(*) as total_sale'))
    //   // ->select('inv_pos.sales_tax', DB::raw('sum(*) as total_sale_tax'))
    //   // ->select('inv_pos.total_discount', DB::raw('sum(*) as total_discount'))
    //   // ->GroupBy('inv_pos.customer_name')
    //   //  ->select(DB::raw('YEAR(date) as year'), DB::raw('sum(amount) as total'))
    //   // ->groupBy(DB::raw('YEAR(date)') )
    //   // $data['ppaginate_data']=['ok'];
    //
    //
    //
    // }

}
