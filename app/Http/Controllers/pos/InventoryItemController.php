<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryItemModel;
use App\Model\Vendor;
use App\Model\CompanyConfigur;
use App\Model\InventoryCategoryModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use DB;
class InventoryItemController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
      $paginate_num = $request->input('paginate_num');
      $search_key = $request->input('search_key');
      $order = $request->input('order');
      $sort = $request->input('sort');
      $data['paginate_data'] =InventoryItemModel::valid()->when($search_key, function($query, $search_key){
                  $query->where(function($query2)use($search_key){
                      $query2->where('inv_product_name','LIKE','%'.$search_key.'%')
                             ->Orwhere('inv_code','LIKE','%'.$search_key.'%');
                  });
                  return $query;

          })->orderBy($sort,$order)->paginate($paginate_num);

       $company_config=CompanyConfigur::first();
       $data['formData']['company_config'] = $company_config;

      return response()->json($data);
    }

    public function create()
    {
      $data['category_data']=array();
      $data['vendor_data']=array();
      $category_data=InventoryCategoryModel::valid()->get();
      $vendor_data=Vendor::valid()->get();
      foreach ($category_data as $value) {
          array_push($data['category_data'],['id'=>$value['id'],'text'=>$value['category_name']]);
      }
      foreach ($vendor_data as $key => $value) {
          array_push($data['vendor_data'],['id'=>$value['id'],'text'=>$value['vendor_name']]);
      }
      $item_tex_codes = CompanyConfigur::first();
      $data['item_tax_code_1'] = $item_tex_codes->sales_tax_a;
      $data['item_tax_code_2'] = $item_tex_codes->sales_tax_b;
      $data['item_tax_code_3'] = $item_tex_codes->sales_tax_c;
      $data['item_tax_code_4'] = $item_tex_codes->sales_tax_d;
      $data['taxable'] = 0;

      $company_config=CompanyConfigur::first();
      $data['company_config'] =$company_config;
      $data['inv_code'] ='';
      $data['inv_product_name'] ='';
      $data['qty_hand'] ='';
      $data['last_cost'] ='';
      $data['a_markup_value'] ='';
      return response($data);

    }

    public function store(Request $request)
    {
      //dd($request);
      $validation=[
        'inv_code'=>'required|unique:inv_product,inv_code,'.$request->id,
        'inv_product_name'=>'required',
        'qty_hand'=>'required',
        'last_cost'=>'required',
        'price_a'=>'required'
      ];
      $message=[
        'inv_code.unique' => 'The Item number has already been taken',
      ];
       $request->validate($validation,$message);
       $data=$request->only('inv_code','inv_alt_code','inv_product_name','category_id','vendor_id','qty_hand','qty_order','reorder_level','reorder_qty','last_cost','avg_cost','price_a','price_b','price_c','price_d','markup_a','markup_b','markup_c','markup_d','sale_price','begain_sale_date','end_sale_date','service','commission','taxable','taxcode','last_date_receive','last_date_sold','item_loction','comment');

       if($request->serial_check ==1)
       {
          $serial_value=collect($request->selectedTags)->pluck('value')->toArray();
          $data=array_add($data,'sell_by_serial_num',serialize($serial_value));

       }


        if(!empty($request->id))
        {
            $update_data=InventoryItemModel::valid()->findOrFail($request->id);
            $save_data=$update_data->update($data);
            $message=['status' => 1, 'message' => 'Your data is successfully updated'];
        }
        else {
           $save_data=InventoryItemModel::create($data);
           $message=['status' => 1, 'message' => 'Your data is successfully saved'];
        }

        if(!$save_data)

        {
          $message=['status' => 0, 'message' => 'Ops! Something went worng.'];

        }

        return response($message);


    }

    public function edit($id)
    {
      $data=InventoryItemModel::valid()->findOrFail($id);
      $category_list = InventoryCategoryModel::valid()->get()->keyBy('id')->all();
      $vendor_list = Vendor::valid()->get()->keyBy('id')->all();

      if(!$data->category_id){
        $data->category_value = ['id'=>'','text'=>''];
      }else{
        $data->category_value = ['id'=>$data->category_id,'text'=>$category_list[$data->category_id]->category_name];
      }
      if(!$data->vendor_id){
        $data->vendor_value = ['id'=>'','text'=>''];
      }else{
        $data->vendor_value = ['id'=>$data->vendor_id,'text'=>$vendor_list[$data->vendor_id]->vendor_name];
      }

      $category_data = array();
      $vendor_data = array();
      foreach ($category_list as $value) {
          array_push($category_data,['id'=>$value['id'],'text'=>$value['category_name']]);
      }
      foreach ($vendor_list as $key => $value) {
          array_push($vendor_data,['id'=>$value['id'],'text'=>$value['vendor_name']]);
      }
      $data->category_data =  $category_data;
      $data->vendor_data =  $vendor_data;

      $item_tex_codes = CompanyConfigur::first();
      $data->item_tax_code_1 = $item_tex_codes->sales_tax_a;
      $data->item_tax_code_2 = $item_tex_codes->sales_tax_b;
      $data->item_tax_code_3 = $item_tex_codes->sales_tax_c;
      $data->item_tax_code_4 = $item_tex_codes->sales_tax_d;
      if($data->taxable != 0){
        $data->taxable = 1;
        $data->codeVisible = false;
      }else{
        $data->codeVisible = true;
      }

      $company_config=CompanyConfigur::first();
      $data->company_config =$company_config;
      $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();
      //dd($last_shift);
      if($last_shift){
            $sales_nontaxable_shift = InventoryPosDetailsModel::valid()
            ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
            ->select(DB::raw('inv_pos_details.product_qty*inv_pos_details.product_price as sub_total'))
            ->where('inv_product.service','=', '0')
            ->where('inv_pos_details.created_at','>=',$last_shift->start_time)
            ->where('inv_pos_details.created_at','<=',$last_shift->end_time)
            ->where('inv_pos_details.product_id',$data->id)
            ->get()
            ->sum('sub_total');
            $data->shift = $sales_nontaxable_shift;
        }else{
            $data->shift = 0;
        }


      return response($data);
    }

    public function destroy($id)
    {
      $delete_data=InventoryItemModel::valid()->findOrFail($id);
      $check_transection = InventoryPosDetailsModel::valid()->where('product_id',$id)->first();
      if($check_transection){
        $message=['status' => 0, 'message' => 'This item has transection'];
      }else{
        if($delete_data->delete())
        {
          $message=['status' => 1, 'message' => 'Your data is successfully deleted'];
        }
      }
      return response($message);
    }









}
