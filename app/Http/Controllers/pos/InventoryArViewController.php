<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\InventoryPosModel;
use App\Model\InvArTransactionModel;
use App\Model\Customer;
use DateTime;
use DB;

class InventoryArViewController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){
      $paginate_num = $request->input('paginate_num');
      $search_key = $request->input('search_key');
      $order = $request->input('order');
      $sort = $request->input('sort');

      $data['paginate_data'] =InventoryPosModel::valid()
                ->whereNotNull('total_due')
                ->join('inv_customer','inv_pos.customer_name','=','inv_customer.id')
                ->select('inv_customer.customer_name' ,'inv_customer.id','inv_customer.customer_number','inv_customer.customer_phone','inv_pos.customer_name AS customer_id',DB::raw('IFNULL(sum(sub_total),0) AS total_sub'),DB::raw('IFNULL(sum(sales_tax),0) AS total_tax'),DB::raw('IFNULL(sum(total_discount),0) AS discount_total'),DB::raw('IFNULL(sum(total_due),0) AS due_balance'),DB::raw('IFNULL(sum(total_pay),0) AS payment'))
                ->whereNotNull('inv_pos.customer_name')
                ->where('inv_pos.pay_type','Account Receivable')
                //->where('total_due','!=',0)
                ->when($search_key, function($query, $search_key){
                  $query->where(function($query2)use($search_key){
                  $query2->where('inv_customer.customer_name','LIKE','%'.$search_key.'%')
                        //->where('total_due','!=',0)
                        ->orWhere('inv_customer.customer_number','LIKE','%'.$search_key.'%');
                  });
                  return $query;

          })->groupBy('inv_pos.customer_name')->orderBy($sort,$order)->paginate($paginate_num);

       return response()->json($data);

    }

    public function create(){

    }



    public function edit($id)
    {
      // pos invoice data
      $pos_details=InventoryPosModel::valid()
                    ->select('id','total_due','total_pay','date','invoice_number',DB::raw(' (IFNULL(sub_total,0) + IFNULL(sales_tax,0)) - IFNULL(total_discount,0) as invoice_amount'))
                    ->whereNotNull('total_due')
                    ->where('pay_type','Account Receivable')
                    ->whereNotNull('customer_name')
                    ->where('customer_name',$id)
                    ->get();
      $due_details= $pos_details->where('total_due','!=',0);
      $pos_deatis_keyBy = $pos_details->keyBy('id'); 
      //id wise customer data
      $data['customer_details']=Customer::valid()
                            ->select('id','customer_number','customer_name','customer_contact','customer_address','customer_city','customer_state','customer_zip','customer_phone')
                            ->where('id',$id)
                            ->first();
      //ar transaction data
      $data['ar_tranction_data']=InvArTransactionModel::valid()
                                  ->where('customer_id','=',$id)
                                  ->get();

      $balance = 0;
      foreach ($data['ar_tranction_data'] as $key => $value) {
        $balance+=$value->ar_sale - $value->ar_payment;
        $value->balance = round($balance,2);
        if($value->ar_sale){
          $value->particular = 'Invoice#'.@$pos_deatis_keyBy[$value->pos_id]->invoice_number;
        }else{
          $value->particular = 'Payment';
        }
      }

      //$total_sale=collect($pos_details)->sum('invoice_amount');
      $total_due=number_format($balance,2);
      $data['due_duration'] = self::dueDuration($due_details);
      //$total_pay=collect($pos_details)->sum('total_pay');
      $data['payment_details']=[
        'total_due'=>$total_due
      ];

     return response()->json($data);



    }

    public static function dueDuration($due_details){

      $current_date =  new DateTime(date('Y-m-d'));

      $curent = 0;
      $days_30_60 = 0;
      $days_60_90 = 0;
      $days_plus_90 = 0;


      foreach ($due_details as $value) {

        $first_date = new DateTime($value['date']);
        $interval = $first_date->diff($current_date);
        $days = $interval->format('%a');

        if($days > 30){
           $days_30_60 += $value['total_due'];
        }elseif($days > 60){
            $days_60_90 += $value['total_due'];
        }elseif($days > 90){
            $days_plus_90 += $value['total_due'];
        }else{
            $curent += $value['total_due'];
        }
      }

      $due_info = [ 'current'  => $curent,
                    'days_30_60'  => $days_30_60,
                    'days_60_90'  => $days_60_90,
                    'days_plus_90'  => $days_plus_90
                  ];

      return $due_info;
    }


    public function post_ar_view_statement($id)
    {
      // pos invoice data
      $due_details=InventoryPosModel::valid()
                  ->select('id','total_due','total_pay','date','invoice_number',DB::raw(' (IFNULL(sub_total,0) + IFNULL(sales_tax,0)) - IFNULL(total_discount,0) as invoice_amount'))
                  ->whereNotNull('total_due')
                  ->where('total_due','!=',0)
                  ->where('pay_type','Account Receivable')
                  ->whereNotNull('customer_name')
                  ->where('customer_name',$id)
                  ->get();
      //id wise customer data
      $data['customer_details']=Customer::valid()
                  ->select('id','customer_number','customer_name','customer_contact','customer_address','customer_city','customer_state','customer_zip','customer_phone')
                  ->where('id',$id)
                  ->first();

      $pos_id=collect($due_details)->pluck('id');
      //ar transaction data
      $ar_data=InvArTransactionModel::valid()
                  ->select('date','ar_payment',DB::raw('"payment" as description'))
                  ->where('customer_id','=',$id)
                  ->where('ar_payment','!=',0)
                  ->whereIn('pos_id',$pos_id)
                  ->get();

      $total_sale=collect($due_details)->sum('invoice_amount');
      $total_due=collect($due_details)->sum('total_due');
      //$total_due=number_format($balance,2);
      $data['due_duration'] = self::dueDuration($due_details);
      $total_pay=collect($due_details)->sum('total_pay');
      $data['payment_details']=[
        'total_sale'=>$total_sale,
        'total_due'=>$total_due,
        'total_pay'=>$total_pay,
      ];

     return response()->json($data);



    }

   //  public static  function getNumber($num=0){
   //     if($num>0){
   //        $number = $num;
   //     }else{
   //        $invoice_info = InventoryPosModel::valid()->orderBy('invoice_number','desc')->first();
   //        $number = (!empty($invoice_info)) ? $invoice_info->invoice_number + 1 : 1;
   //     }
   //     $numlength = strlen((string)$number);
   //     if($numlength<4){
   //        if($numlength ==1){
   //           $number = '000'.$number;
   //        }else if($numlength ==2){
   //           $number = '00'.$number;
   //        }else{
   //          $number = '0'.$number;
   //        }
   //     }else{
   //        $number = $number;
   //     }
   //     return $number;
   // }

}
