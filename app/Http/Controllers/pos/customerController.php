<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\CompanyConfigur;
use App\Model\InventoryPosModel;
use Auth;

class customerController extends Controller
{
    public function index(Request $request)
    {
        $paginate_num = $request->input('paginate_num');
        $search_key = $request->input('search_key');
        $order = $request->input('order');
        $sort = $request->input('sort');
        $data['paginate_data'] =Customer::valid()->when($search_key, function($query, $search_key){
                $query->where(function($query2) use ($search_key){
                    $query2->where('customer_name','LIKE','%'.$search_key.'%')
                      ->orWhere('customer_contact','LIKE', '%'.$search_key.'%');
                });
                return $query;

        })->orderBy($sort,$order)->paginate($paginate_num);
        $data['formData']=['codeVisible' => false,'texcheck' => 0];
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $validation = [
            'customer_number'=> 'required|unique:inv_customer,customer_number,'.$request->id.'id',
            'customer_name' => 'required',
        ];
        $request->validate($validation);
        $data=$request->only('customer_name','customer_contact','customer_address','customer_phone','customer_number','customer_fax','customer_email','customer_last_date_purchage','customer_comment','customer_credit_limit','allow_ar','charge_interest','allow_layaway','ar_blance_forward','total_ar_charges','customer_price_code','customer_sales_tax_number','non_taxable_customer','item_tax_code_1','item_tax_code_2','item_tax_code_3','customer_purchage_1','customer_purchage_2','customer_purchage_3');
        if(!empty($request->id)){
            $save =  Customer::valid()->findOrFail($request->id)->update($data);
        }else{
            $save =   Customer::create($data);
        }

        if($save){
            $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
        }else{
            $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];
        }
        return response()->json($output);
    }

    public function edit(Request $request)
    {

        $customer = Customer::valid()->findOrFail($request->id);
        $item_tex_codes = CompanyConfigur::first();
        $customer->items_tax_code_1 = $item_tex_codes->sales_tax_a;
        $customer->items_tax_code_2 = $item_tex_codes->sales_tax_b;
        $customer->items_tax_code_3 = $item_tex_codes->sales_tax_c;
        $customer->items_tax_code_4 = $item_tex_codes->sales_tax_d;
        if($customer->item_tax_code_1){
            $customer->non_taxable_customer = 1;
        }
        return response($customer);
    }

    public function destroy(Request $request)
    {

        $customer = Customer::valid()->findOrFail($request->id);

        $chek_customer = InventoryPosModel::valid()->where('customer_name',$request->id)->first();

        if($chek_customer){
            $message=['status' => 0, 'message' => 'This customer has transection'];
        }else{
            if($customer->delete()){
                $message=['status' => 1, 'message' => 'Your data is successfully deleted'];
            }
        }

        return response($message);

    }

    public function create()
    {
        $item_tex_codes = CompanyConfigur::first();
        $data['items_tax_code_1'] = $item_tex_codes->sales_tax_a;
        $data['items_tax_code_2'] = $item_tex_codes->sales_tax_b;
        $data['items_tax_code_3'] = $item_tex_codes->sales_tax_c;
        $data['items_tax_code_4'] = $item_tex_codes->sales_tax_d;
        $data['texcheck'] = 0;
        return response($data);
    }

}
