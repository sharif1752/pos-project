<?php

namespace App\Http\Controllers\pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InventoryPosDetailsModel;
use App\Model\InventoryItemModel;
use App\Model\InventoryPosModel;
use App\Model\InventoryReceiveModel;
use App\Model\InventoryReceiveDetailsModel;
use App\Model\InvArTransactionModel;
use App\Model\CompanyConfigur;
use App\Model\Customer;
use App\Model\SalesPersonModel;
use App\Model\SalesPersonShiftModel;
use Auth;
use DB;
use Carbon\Carbon;

class AccountsReportController extends Controller
{
   /* private $g_total_sale_d=0;
    private $g_total_sale_m=0;
    private $g_total_sale_y;
    private $g_collection_sale_d;
    private $g_collection_sale_m;
    private $g_collection_sale_y;*/

    public function index(Request $request)
    {

    }

    public function incomeReport(){
        
        $current_date =$month_end_date=$year_end_date= date('Y-m-d');
        $date_explode= explode('-', $current_date);

        $month_start_date = $date_explode[0]."-".$date_explode[1]."-"."01";
        //$month_start_date = $date_explode[0]."-"."01"."-"."01";
        $year_start_date = $date_explode[0]."-"."01"."-"."01";
        //$year_start_date = '2015'."-"."01"."-"."01";
        //$current_date = "2020-03-19";
        $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();
        /*invoice accounts daily,monthly,yearly*/
        $total_invoice_daily =InventoryPosModel::valid()->where('date',$current_date)->get();
        $total_invoice_monthly =InventoryPosModel::valid()->whereBetween('date', [$month_start_date, $current_date])->get();
        $total_invoice_yearly =InventoryPosModel::valid()->whereBetween('date', [$year_start_date, $current_date])->get();
        $total_invoice_shift =InventoryPosModel::valid()
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get();

        $total_invoice =[
            'title' => 'Total Invoice',
            'shift' => count($total_invoice_shift),
            'daily' => count($total_invoice_daily),
            'monthly' => count($total_invoice_monthly),
            'yearly' => count($total_invoice_yearly)
        ];

        /*sales taxable dalily,monthly,yearly*/
        $sales_taxable_d = InventoryPosModel::valid()
        ->where('inv_pos.date',$current_date)
        ->get();

        $sales_taxable_daily = $sales_taxable_d->sum('sub_total');

        $sales_taxable_m = InventoryPosModel::valid()
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get();
        $sales_taxable_monthly = $sales_taxable_m->sum('sub_total');

        $sales_taxable_y = InventoryPosModel::valid()
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get();
        $sales_taxable_yearly = $sales_taxable_y->sum('sub_total');

        $sales_taxable_s = InventoryPosModel::valid()
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get();
        $sales_taxable_shift = $sales_taxable_s->sum('sub_total');

        $total_sales_taxable =[
            'title' => 'Sales Nontaxable',
            'shift' => $sales_taxable_shift,
            'daily' => $sales_taxable_daily,
            'monthly' => $sales_taxable_monthly,
            'yearly' => $sales_taxable_yearly
        ];

        /*sales nontaxable dalily,monthly,yearly*/
        $sales_nontaxable_daily = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->where('inv_pos_details.date',$current_date)
        ->get()
        ->sum('sub_total');
        /*sales nontaxable monthly*/
        $sales_nontaxable_monthly = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->whereBetween('inv_pos_details.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        /*sales nontaxable yearly*/
        $sales_nontaxable_yearly = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->whereBetween('inv_pos_details.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $sales_nontaxable_shift = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->where('inv_pos_details.created_at','>=',$last_shift->start_time)
        ->where('inv_pos_details.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sub_total');

        $total_sales_nontaxable =[
            'title' => 'Sales taxable',
            'shift' => 0,
            'daily' => 0,
            'monthly' => 0,
            'yearly' => 0
        ];

        /*account return dalily*/
        $account_return_d = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->where('inv_product_receive.date',$current_date)
        ->get();
        $account_return_daily = $account_return_d->sum('total_amount') + $account_return_d->sum('receive_cost');
        /*account return monthly*/
       /* $account_return_m = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->whereBetween('inv_product_receive.date', [$month_start_date, $current_date])
        ->get();
        $account_return_monthly = $account_return_m->sum('total_amount') + $account_return_m->sum('receive_cost');

        $account_return_y = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->whereBetween('inv_product_receive.date', [$year_start_date, $current_date])
        ->get();
        $account_return_yearly = $account_return_y->sum('total_amount') + $account_return_y->sum('receive_cost');*/

        $account_return_daily = 0;
        $account_return_monthly = 0;
        $account_return_yearly = 0;

        $total_account_return =[
            'title' => 'Return',
            'shift' => 0,
            'daily' => $account_return_daily,
            'monthly' => $account_return_monthly,
            'yearly' => $account_return_yearly
        ];

        /*service_total dalily,monthly,yearly*/
        $inv_code_service = InventoryItemModel::valid()->where('service','=','1')->get()->pluck('id')->all();

        $service_total_daily = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->where('inv_pos.date',$current_date)
        ->get()
        ->sum('sub_total');

        $service_total_monthly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $service_total_yearly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $service_total_shift= InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->where('inv_pos.created_at','>=',$last_shift->start_time)
        ->where('inv_pos.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sub_total');

        $total_service =[
            'title' => 'Service',
            'shift' => $service_total_shift,
            'daily' => $service_total_daily,
            'monthly' => $service_total_monthly,
            'yearly' => $service_total_yearly
        ];

        /*sales tax dalily,monthly,yearly*/
        $sales_tax_daily = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->where('inv_pos.date',$current_date)
        ->get()
        ->sum('sales_tax');

        $sales_tax_monthly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sales_tax');

        $sales_tax_yearly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sales_tax');

        $sales_tax_shift = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->where('inv_pos.created_at','>=',$last_shift->start_time)
        ->where('inv_pos.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sales_tax');
        /*Account Receiveable Daily,Monthly,Yearly*/
        /*$total_invoice_due_daily =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total+inv_pos.sales_tax as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('date',$current_date)->get()->sum('ar_sub_total');

        $total_invoice_monthly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total+inv_pos.sales_tax as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->whereBetween('date', [$month_start_date, $current_date])->get()->sum('ar_sub_total');

        $total_invoice_yearly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total+inv_pos.sales_tax as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->whereBetween('date', [$year_start_date, $current_date])->get()->sum('ar_sub_total');*/


        $account_received_d = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->where('date',$current_date)
        ->get()
        ->sum('ar_payment');

        $account_received_m = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->whereBetween('date', [$month_start_date, $current_date])
        ->get()
        ->sum('ar_payment');

        $account_received_y = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->whereBetween('date', [$year_start_date, $current_date])
        ->get()
        ->sum('ar_payment');

        $account_received_shift = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->where('created_at','>=',$last_shift->start_time)
        ->where('created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('ar_payment');

        $data['total_account_receive'] =[
            'title' => 'Rceived on Account(AR)',
            'shift' => $account_received_shift,
            'daily' => $account_received_d,
            'monthly' => $account_received_m,
            'yearly' => $account_received_y
        ];

        $total_tax_collect =[
            'title' => 'Sales Tax Collected',
            'shift' => $sales_tax_shift,
            'daily' => $sales_tax_daily,
            'monthly' => $sales_tax_monthly,
            'yearly' => $sales_tax_yearly
        ];

        $data['account_collection']=[
            '0' => $total_invoice,
            '1' => $total_sales_taxable,
            '2' => $total_sales_nontaxable,
            '3' => $total_account_return,
            '4' => $total_service,
            '5' => $total_tax_collect,
        ];

        $data['grand_total_shift'] = 0;
        $data['grand_total_daily'] = 0;
        $data['grand_total_monthly'] = 0;
        $data['grand_total_yearly'] = 0;
        foreach ($data['account_collection'] as $value) {
            if($value['title'] != 'Total Invoice'){
                if($value['title'] == 'Return'){
                    $data['grand_total_shift'] -= $value['shift'];
                    $data['grand_total_daily'] -= $value['daily'];
                    $data['grand_total_monthly'] -= $value['monthly'];
                    $data['grand_total_yearly'] -= $value['yearly'];
                }else{
                    $data['grand_total_shift'] += $value['shift'];
                    $data['grand_total_daily'] += $value['daily'];
                    $data['grand_total_monthly'] += $value['monthly'];
                    $data['grand_total_yearly'] += $value['yearly'];
                }
            }
        }



        $data['company_config']=CompanyConfigur::first();
        return response()->json($data);
    }

    public function paymentType(){
        $current_date =$month_end_date=$year_end_date= date('Y-m-d');
        $date_explode= explode('-', $current_date);
        $month_start_date = $date_explode[0]."-".$date_explode[1]."-"."01";
        //$month_start_date = $date_explode[0]."-"."01"."-"."01";
        $year_start_date = $date_explode[0]."-"."01"."-"."01";
        //$year_start_date = '2015'."-"."01"."-"."01";

        //$current_date = "2020-03-19";

        $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();

        $paytype_query_d = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_m = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->whereBetween('date', [$month_start_date, $current_date])
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_y = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->whereBetween('date', [$year_start_date, $current_date])
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_s = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        ->whereNull('customer_name')
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get()
                        ->sum('cash_sub_total');


        $total_invoice_due_daily =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('pay_type','=','Account Receivable')
        ->where('date',$current_date)->get()->sum('ar_sub_total');

        $total_invoice_monthly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('pay_type','=','Account Receivable')
        ->whereBetween('date', [$month_start_date, $current_date])->get()->sum('ar_sub_total');

        $total_invoice_yearly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('pay_type','=','Account Receivable')
        ->whereBetween('date', [$year_start_date, $current_date])->get()->sum('ar_sub_total');

        $total_invoice_s =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.sub_total as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('pay_type','=','Account Receivable')
        ->where('created_at','>=',$last_shift->start_time)
        ->where('created_at','<=',$last_shift->end_time)
        ->get()->sum('ar_sub_total');

        $total_account_reveiveable= [
            'title' => 'Account Receiveable',
            'shift' => $total_invoice_s,
            'daily' => $total_invoice_due_daily,
            'monthly'=> $total_invoice_monthly,
            'yearly' => $total_invoice_yearly,
        ];
        // calculate income By Paymnt
       /* $get_value = $this->incomeReport();
        $g_value = json_decode($get_value->getContent());
        //dd($g_value);
        $grand_total_monthly = (!empty($g_value->grand_total_monthly)) ? $g_value->grand_total_monthly : 0;
        $a_r_monthly = (!empty($g_value->total_account_receive->monthly)) ? $g_value->total_account_receive->monthly : 0;
,        $paytype_query_m_n = ($grand_total_monthly-$total_invoice_monthly)+$a_r_monthly;
*/
        $total_paytype_cash = [
            'title' => 'Cash',
            'shift' => $paytype_query_s,
            'daily' => $paytype_query_d,
            'monthly' => $paytype_query_m,
            'yearly' => $paytype_query_y,
        ];
        //dd($total_paytype_cash);



        $data['accout_payment_type'] = [
            '0' => $total_paytype_cash,
            '1' => $total_account_reveiveable
        ];

        

        
        $data['company_config']=CompanyConfigur::first();

        return response()->json($data);

    }
    public function texCalculation($text_code,$starting_date,$ending_date,$shift){

        if($ending_date == 'current'){
            $sales_tex_query= InventoryPosDetailsModel::valid()
                ->where('product_tax',$text_code)
                ->where('date',$starting_date)
                ->get();
        }else{
            if($shift != ''){
                $sales_tex_query= InventoryPosDetailsModel::valid()
                ->where('product_tax',$text_code)
                ->where('created_at','>=',$starting_date)
                ->where('created_at','<=',$ending_date)
                ->get();
            }else{
                $sales_tex_query= InventoryPosDetailsModel::valid()
                ->where('product_tax',$text_code)
                ->whereBetween('date', [$starting_date, $ending_date])
                ->get();
            }
        }

        $total_sales_tex = 0;

        foreach ($sales_tex_query as $key => $value) {
            $sales_tex_unit_p = $value['product_price']*($text_code/100);
            $sales_tex_total_p = $sales_tex_unit_p * $value['product_qty'];
            $total_sales_tex += $sales_tex_total_p;
        }
        $total_sales_tex_c = $total_sales_tex;
        $total_sales_tex = 0;
        return  number_format($total_sales_tex_c,2);
    }

    public function salesTex(){
        $current_date =$month_end_date=$year_end_date= date('Y-m-d');
        $date_explode= explode('-', $current_date);
        $month_start_date = $date_explode[0]."-".$date_explode[1]."-"."01";
        $year_start_date = $date_explode[0]."-"."01"."-"."01";

        $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();

        $get_sales_code = CompanyConfigur::first();

        $total_sales_tex_a_d = $this->texCalculation($get_sales_code->sales_tax_a,$current_date,'current','');        

        $total_sales_tex_a_m = $this->texCalculation($get_sales_code->sales_tax_a,$month_start_date,$current_date,'');

        $total_sales_tex_a_y = $this->texCalculation($get_sales_code->sales_tax_a,$year_start_date,$current_date,'');

        $total_sales_tex_a_s = $this->texCalculation($get_sales_code->sales_tax_a,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_a = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_a.'%)',
            'shift' =>$total_sales_tex_a_s,
            'daily' =>$total_sales_tex_a_d,
            'monthly' =>$total_sales_tex_a_m,
            'yearly' =>$total_sales_tex_a_y
        ];

        $total_sales_tex_b_d = $this->texCalculation($get_sales_code->sales_tax_b,$current_date,'current','');        

        $total_sales_tex_b_m = $this->texCalculation($get_sales_code->sales_tax_b,$month_start_date,$current_date,'');

        $total_sales_tex_b_y = $this->texCalculation($get_sales_code->sales_tax_b,$year_start_date,$current_date,'');

        $total_sales_tex_b_s = $this->texCalculation($get_sales_code->sales_tax_b,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_b = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_b.'%)',
            'shift' =>$total_sales_tex_b_s,
            'daily' =>$total_sales_tex_b_d,
            'monthly' =>$total_sales_tex_b_m,
            'yearly' =>$total_sales_tex_b_y
        ];

        $total_sales_tex_c_d = $this->texCalculation($get_sales_code->sales_tax_c,$current_date,'current','');        

        $total_sales_tex_c_m = $this->texCalculation($get_sales_code->sales_tax_c,$month_start_date,$current_date,'');

        $total_sales_tex_c_y = $this->texCalculation($get_sales_code->sales_tax_c,$year_start_date,$current_date,'');

        $total_sales_tex_c_s = $this->texCalculation($get_sales_code->sales_tax_c,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_c = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_c.'%)',
            'shift' =>$total_sales_tex_c_s,
            'daily' =>$total_sales_tex_c_d,
            'monthly' =>$total_sales_tex_c_m,
            'yearly' =>$total_sales_tex_c_y
        ];

        $total_sales_tex_d_d = $this->texCalculation($get_sales_code->sales_tax_d,$current_date,'current','');        

        $total_sales_tex_d_m = $this->texCalculation($get_sales_code->sales_tax_d,$month_start_date,$current_date,'');

        $total_sales_tex_d_y = $this->texCalculation($get_sales_code->sales_tax_d,$year_start_date,$current_date,'');

        $total_sales_tex_d_s = $this->texCalculation($get_sales_code->sales_tax_b,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_d = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_d.'%)',
            'shift' =>$total_sales_tex_d_s,
            'daily' =>$total_sales_tex_d_d,
            'monthly' =>$total_sales_tex_d_m,
            'yearly' =>$total_sales_tex_d_y
        ];

        $data['get_sales_tex_info'] =[
            '0' =>$total_value_tex_code_a,
            '1' =>$total_value_tex_code_b,
            '2' =>$total_value_tex_code_c,
            '3' =>$total_value_tex_code_d
        ];

        $get_sales_tex_info_shift = 0;
        $get_sales_tex_info_daily = 0;
        $get_sales_tex_info_monthly = 0;
        $get_sales_tex_info_yearly = 0;
        foreach ($data['get_sales_tex_info'] as $value) {
            $get_sales_tex_info_shift += $value['shift'];
            $get_sales_tex_info_daily += $value['daily'];
            $get_sales_tex_info_monthly += $value['monthly'];
            $get_sales_tex_info_yearly += $value['yearly'];
        }

        $data['get_sales_tex_info_shift'] = number_format($get_sales_tex_info_shift,2);
        $data['get_sales_tex_info_daily'] = number_format($get_sales_tex_info_daily,2);
        $data['get_sales_tex_info_monthly'] = number_format($get_sales_tex_info_monthly,2);
        $data['get_sales_tex_info_yearly'] = number_format($get_sales_tex_info_yearly,2);

        $data['company_config']=CompanyConfigur::first();

        return response()->json($data);

    }
    public function accountSummery(){

        $current_date= date('Y-m-d');
        //$current_date='2020-03-19';
        $data['summery'] = [];
        $csuromer_list = Customer::valid()->get()->keyBy('id')->all();
        $salesperson_list=SalesPersonModel::valid()->get()->keyBy('id')->all();
        $summery=InventoryPosModel::valid()->where('date',$current_date)->get();
        foreach ($summery as $key => $value) {
            if(array_key_exists($value['customer_name'],$csuromer_list)){
                $customer_name = $csuromer_list[$value['customer_name']]->customer_name;
            }else{
                $customer_name="";
            }
            if(array_key_exists($value->created_by,$salesperson_list)){
                $sales_person_name = $salesperson_list[$value['created_by']]->name;
            }else{

                $sales_person_name="";
            }
            array_push($data['summery'],['number'=>$value['invoice_number'],
                                        'date'=>$value['date'],
                                        'c_name'=>$customer_name,
                                        'sp_name'=>$sales_person_name,
                                        'total'=>$value['sub_total']+$value['sales_tax'],
                                        'paytype'=>$value['pay_type']
                                    ]);
        }
        $paytype_query_d = InventoryPosModel::valid()
                        ->where('pay_type','=','Cash')
                        ->whereNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('sub_total');
        $paytype_due_d = InventoryPosModel::valid()
                        ->where('pay_type','=','Account Receivable')
                        ->whereNotNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('sub_total');
        $data['recieved_income'] = $paytype_query_d;
        $data['recieved_on_account'] = $paytype_due_d;
        return response()->json($data);
    }
    public function accountingInformationReport(){
        $data['report_name'] = "Account Information";
        $data['report_title'] = "Account Information Report";
        $data['reportDate'] = date('F d, Y');
        /*invoice accounts daily,monthly,yearly*/
        $current_date =$month_end_date=$year_end_date= date('Y-m-d');
        $date_explode= explode('-', $current_date);
        $month_start_date = $date_explode[0]."-".$date_explode[1]."-"."01";
        $year_start_date = $date_explode[0]."-"."01"."-"."01";

        $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->orderBy('id','desc')->first();

        $total_invoice_daily =InventoryPosModel::valid()->where('date',$current_date)->get();
        $total_invoice_monthly =InventoryPosModel::valid()->whereBetween('date', [$month_start_date, $current_date])->get();
        $total_invoice_yearly =InventoryPosModel::valid()->whereBetween('date', [$year_start_date, $current_date])->get();
        $total_invoice_shift =InventoryPosModel::valid()
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get();

        $total_invoice =[
            'title' => 'Total Invoice',
            'shift' => count($total_invoice_shift),
            'daily' => count($total_invoice_daily),
            'monthly' => count($total_invoice_monthly),
            'yearly' => count($total_invoice_yearly)
        ];

        /*sales taxable dalily,monthly,yearly*/
        $sales_taxable_d = InventoryPosModel::valid()
        ->where('inv_pos.date',$current_date)
        ->get();

        $sales_taxable_daily = $sales_taxable_d->sum('sub_total');

        $sales_taxable_m = InventoryPosModel::valid()
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get();
        $sales_taxable_monthly = $sales_taxable_m->sum('sub_total');

        $sales_taxable_y = InventoryPosModel::valid()
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get();
        $sales_taxable_yearly = $sales_taxable_y->sum('sub_total');

        $sales_taxable_s = InventoryPosModel::valid()
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get();
        $sales_taxable_shift = $sales_taxable_s->sum('sub_total');

        $total_sales_taxable =[
            'title' => 'Sales Nontaxable',
            'shift' => $sales_taxable_shift,
            'daily' => $sales_taxable_daily,
            'monthly' => $sales_taxable_monthly,
            'yearly' => $sales_taxable_yearly
        ];

        /*sales nontaxable dalily,monthly,yearly*/
        /*sales nontaxable dalily,monthly,yearly*/
        $sales_nontaxable_daily = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->where('inv_pos_details.date',$current_date)
        ->get()
        ->sum('sub_total');
        /*sales nontaxable monthly*/
        $sales_nontaxable_monthly = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->whereBetween('inv_pos_details.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        /*sales nontaxable yearly*/
        $sales_nontaxable_yearly = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->whereBetween('inv_pos_details.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $sales_nontaxable_shift = InventoryPosDetailsModel::valid()
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select(DB::raw('IFNULL(inv_pos_details.product_qty*inv_pos_details.product_price,0) as sub_total'))
        ->where('inv_product.taxable','=', '0')
        ->where('inv_product.service','=', '0')
        ->where('inv_pos_details.created_at','>=',$last_shift->start_time)
        ->where('inv_pos_details.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sub_total');

        $total_sales_nontaxable =[
            'title' => 'Sales taxable',
            'shift' => 0,
            'daily' => 0,
            'monthly' => 0,
            'yearly' => 0
        ];

        /*account return dalily*/
        $account_return_d = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->where('inv_product_receive.date',$current_date)
        ->get();
        $account_return_daily = $account_return_d->sum('total_amount') + $account_return_d->sum('receive_cost');
        /*account return monthly*/
       /* $account_return_m = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->whereBetween('inv_product_receive.date', [$month_start_date, $current_date])
        ->get();
        $account_return_monthly = $account_return_m->sum('total_amount') + $account_return_m->sum('receive_cost');

        $account_return_y = InventoryReceiveModel::valid()->join('inv_product_receive_details', 'inv_product_receive_details.product_receive_id', '=', 'inv_product_receive.id')
        ->select('inv_product_receive.total_amount','inv_product_receive_details.receive_cost')
        ->whereBetween('inv_product_receive.date', [$year_start_date, $current_date])
        ->get();
        $account_return_yearly = $account_return_y->sum('total_amount') + $account_return_y->sum('receive_cost');*/

        $account_return_daily = 0;
        $account_return_monthly = 0;
        $account_return_yearly = 0;

        $total_account_return =[
            'title' => 'Return',
            'shift' => 0,
            'daily' => $account_return_daily,
            'monthly' => $account_return_monthly,
            'yearly' => $account_return_yearly
        ];

         /*service_total dalily,monthly,yearly*/
        $inv_code_service = InventoryItemModel::valid()->where('service','=','1')->get()->pluck('id')->all();

        $service_total_daily = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->where('inv_pos.date',$current_date)
        ->get()
        ->sum('sub_total');

        $service_total_monthly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $service_total_yearly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sub_total');

        $service_total_shift= InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->select('inv_pos.sub_total')
        ->whereIn('inv_pos_details.product_id',$inv_code_service)
        ->where('inv_pos.created_at','>=',$last_shift->start_time)
        ->where('inv_pos.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sub_total');

        $total_service =[
            'title' => 'Service',
            'shift' => $service_total_shift,
            'daily' => $service_total_daily,
            'monthly' => $service_total_monthly,
            'yearly' => $service_total_yearly
        ];

        /*sales tax dalily,monthly,yearly*/
        $sales_tax_daily = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->where('inv_pos.date',$current_date)
        ->get()
        ->sum('sales_tax');

        $sales_tax_monthly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->whereBetween('inv_pos.date', [$month_start_date, $current_date])
        ->get()
        ->sum('sales_tax');

        $sales_tax_yearly = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->whereBetween('inv_pos.date', [$year_start_date, $current_date])
        ->get()
        ->sum('sales_tax');

        $sales_tax_shift = InventoryPosModel::valid()->join('inv_pos_details', 'inv_pos_details.pos_id', '=', 'inv_pos.id')
        ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
        ->select('inv_pos.sales_tax')
        ->where('inv_product.taxable','=', '1')
        ->where('inv_pos.created_at','>=',$last_shift->start_time)
        ->where('inv_pos.created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('sales_tax');

        $total_tax_collect =[
            'title' => 'Sales Tax Collected',
            'shift' => $sales_tax_shift,
            'daily' => $sales_tax_daily,
            'monthly' => $sales_tax_monthly,
            'yearly' => $sales_tax_yearly
        ];

        //received on account
        $account_received_d = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->where('date',$current_date)
        ->get()
        ->sum('ar_payment');

        $account_received_m = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->whereBetween('date', [$month_start_date, $current_date])
        ->get()
        ->sum('ar_payment');

        $account_received_y = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->whereBetween('date', [$year_start_date, $current_date])
        ->get()
        ->sum('ar_payment');

        $account_received_shift = InvArTransactionModel::valid()
        ->where('ar_payment','!=','0')
        ->where('created_at','>=',$last_shift->start_time)
        ->where('created_at','<=',$last_shift->end_time)
        ->get()
        ->sum('ar_payment');

        $data['total_account_receive'] =[
            'title' => 'Rceived on Account(AR)',
            'shift' => $account_received_shift,
            'daily' => $account_received_d,
            'monthly' => $account_received_m,
            'yearly' => $account_received_y
        ];

        $data['account_collection']=[
            '0' => $total_invoice,
            '1' => $total_sales_taxable,
            '2' => $total_sales_nontaxable,
            '3' => $total_account_return,
            '4' => $total_service,
            '5' => $total_tax_collect,
        ];

        $data['grand_total_shift'] = 0;
        $data['grand_total_daily'] = 0;
        $data['grand_total_monthly'] = 0;
        $data['grand_total_yearly'] = 0;
        foreach ($data['account_collection'] as $value) {
            if($value['title'] != 'Total Invoice'){
                if($value['title'] == 'Return'){
                    $data['grand_total_shift'] -= $value['shift'];
                    $data['grand_total_daily'] -= $value['daily'];
                    $data['grand_total_monthly'] -= $value['monthly'];
                    $data['grand_total_yearly'] -= $value['yearly'];
                }else{
                    $data['grand_total_shift'] += $value['shift'];
                    $data['grand_total_daily'] += $value['daily'];
                    $data['grand_total_monthly'] += $value['monthly'];
                    $data['grand_total_yearly'] += $value['yearly'];
                }
            }
        }

       ////payment type

         $paytype_query_d = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_m = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->whereBetween('date', [$month_start_date, $current_date])
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_y = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        //->whereNull('customer_name')
                        ->whereBetween('date', [$year_start_date, $current_date])
                        ->get()
                        ->sum('cash_sub_total');

        $paytype_query_s = InventoryPosModel::valid()
                        ->select(DB::raw('IFNULL(inv_pos.sub_total,0) + IFNULL(inv_pos.sales_tax,0) as cash_sub_total'))
                        ->where('pay_type','=','Cash')
                        ->whereNull('customer_name')
                        ->where('created_at','>=',$last_shift->start_time)
                        ->where('created_at','<=',$last_shift->end_time)
                        ->get()
                        ->sum('cash_sub_total');

        $total_paytype_cash = [
            'title' => 'Cash',
            'shift' => $paytype_query_s,
            'daily' => $paytype_query_d,
            'monthly' => $paytype_query_m,
            'yearly' => $paytype_query_y,
        ];

        $total_invoice_due_daily =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.total_due as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('date',$current_date)->get()->sum('ar_sub_total');

        $total_invoice_monthly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.total_due as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->whereBetween('date', [$month_start_date, $current_date])->get()->sum('ar_sub_total');

        $total_invoice_yearly =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.total_due as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->whereBetween('date', [$year_start_date, $current_date])->get()->sum('ar_sub_total');

        $total_invoice_s =InventoryPosModel::valid()
        ->select(DB::raw('inv_pos.total_due as ar_sub_total'))
        ->whereNotNull('customer_name')
        ->where('created_at','>=',$last_shift->start_time)
        ->where('created_at','<=',$last_shift->end_time)
        ->get()->sum('ar_sub_total');

        $total_account_reveiveable= [
            'title' => 'Account Receiveable',
            'shift' => $total_invoice_s,
            'daily' => $total_invoice_due_daily,
            'monthly'=> $total_invoice_monthly,
            'yearly' => $total_invoice_yearly,
        ];

        $data['accout_payment_type'] = [
            '0' => $total_paytype_cash,
            '1' => $total_account_reveiveable
        ];

        ///sales taxed

         $get_sales_code = CompanyConfigur::first();

        $total_sales_tex_a_d = $this->texCalculation($get_sales_code->sales_tax_a,$current_date,'current','');        

        $total_sales_tex_a_m = $this->texCalculation($get_sales_code->sales_tax_a,$month_start_date,$current_date,'');

        $total_sales_tex_a_y = $this->texCalculation($get_sales_code->sales_tax_a,$year_start_date,$current_date,'');

        $total_sales_tex_a_s = $this->texCalculation($get_sales_code->sales_tax_a,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_a = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_a.'%)',
            'shift' =>$total_sales_tex_a_s,
            'daily' =>$total_sales_tex_a_d,
            'monthly' =>$total_sales_tex_a_m,
            'yearly' =>$total_sales_tex_a_y
        ];

        $total_sales_tex_b_d = $this->texCalculation($get_sales_code->sales_tax_b,$current_date,'current','');        

        $total_sales_tex_b_m = $this->texCalculation($get_sales_code->sales_tax_b,$month_start_date,$current_date,'');

        $total_sales_tex_b_y = $this->texCalculation($get_sales_code->sales_tax_b,$year_start_date,$current_date,'');

        $total_sales_tex_b_s = $this->texCalculation($get_sales_code->sales_tax_b,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_b = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_b.'%)',
            'shift' =>$total_sales_tex_b_s,
            'daily' =>$total_sales_tex_b_d,
            'monthly' =>$total_sales_tex_b_m,
            'yearly' =>$total_sales_tex_b_y
        ];

        $total_sales_tex_c_d = $this->texCalculation($get_sales_code->sales_tax_c,$current_date,'current','');        

        $total_sales_tex_c_m = $this->texCalculation($get_sales_code->sales_tax_c,$month_start_date,$current_date,'');

        $total_sales_tex_c_y = $this->texCalculation($get_sales_code->sales_tax_c,$year_start_date,$current_date,'');

        $total_sales_tex_c_s = $this->texCalculation($get_sales_code->sales_tax_c,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_c = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_c.'%)',
            'shift' =>$total_sales_tex_c_s,
            'daily' =>$total_sales_tex_c_d,
            'monthly' =>$total_sales_tex_c_m,
            'yearly' =>$total_sales_tex_c_y
        ];

        $total_sales_tex_d_d = $this->texCalculation($get_sales_code->sales_tax_d,$current_date,'current','');        

        $total_sales_tex_d_m = $this->texCalculation($get_sales_code->sales_tax_d,$month_start_date,$current_date,'');

        $total_sales_tex_d_y = $this->texCalculation($get_sales_code->sales_tax_d,$year_start_date,$current_date,'');

        $total_sales_tex_d_s = $this->texCalculation($get_sales_code->sales_tax_b,$last_shift->start_time,$last_shift->end_time,'shift');

        $total_value_tex_code_d = [
            'title' =>'Tex Code('.$get_sales_code->sales_tax_d.'%)',
            'shift' =>$total_sales_tex_d_s,
            'daily' =>$total_sales_tex_d_d,
            'monthly' =>$total_sales_tex_d_m,
            'yearly' =>$total_sales_tex_d_y
        ];

        $data['get_sales_tex_info'] =[
            '0' =>$total_value_tex_code_a,
            '1' =>$total_value_tex_code_b,
            '2' =>$total_value_tex_code_c,
            '3' =>$total_value_tex_code_d
        ];

        $get_sales_tex_info_shift = 0;
        $get_sales_tex_info_daily = 0;
        $get_sales_tex_info_monthly = 0;
        $get_sales_tex_info_yearly = 0;
        foreach ($data['get_sales_tex_info'] as $value) {
            $get_sales_tex_info_shift += $value['shift'];
            $get_sales_tex_info_daily += $value['daily'];
            $get_sales_tex_info_monthly += $value['monthly'];
            $get_sales_tex_info_yearly += $value['yearly'];
        }

        $data['get_sales_tex_info_shift'] = number_format($get_sales_tex_info_shift,2);
        $data['get_sales_tex_info_daily'] = number_format($get_sales_tex_info_daily,2);
        $data['get_sales_tex_info_monthly'] = number_format($get_sales_tex_info_monthly,2);
        $data['get_sales_tex_info_yearly'] = number_format($get_sales_tex_info_yearly,2);

        //invoice summery report

        $data['summery'] = [];
        $csuromer_list = Customer::valid()->get()->keyBy('id')->all();
        $salesperson_list=SalesPersonModel::valid()->get()->keyBy('id')->all();
        $summery=InventoryPosModel::valid()->where('date',$current_date)->get();
        foreach ($summery as $key => $value) {
            if(array_key_exists($value['customer_name'],$csuromer_list)){
                $customer_name = $csuromer_list[$value['customer_name']]->customer_name;
            }else{
                $customer_name="";
            }
            if(array_key_exists($value->created_by,$salesperson_list)){
                $sales_person_name = $salesperson_list[$value['created_by']]->name;
            }else{

                $sales_person_name="";
            }
            array_push($data['summery'],['number'=>$value['invoice_number'],
                                        'date'=>$value['date'],
                                        'c_name'=>$customer_name,
                                        'sp_name'=>$sales_person_name,
                                        'total'=>$value['sub_total']+$value['sales_tax'],
                                        'paytype'=>$value['pay_type']
                                    ]);
        }
        $paytype_query_d = InventoryPosModel::valid()
                        ->where('pay_type','=','Cash')
                        ->whereNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('sub_total');
        $paytype_due_d = InventoryPosModel::valid()
                        ->where('pay_type','=','Account Receivable')
                        ->whereNotNull('customer_name')
                        ->where('date',$current_date)
                        ->get()
                        ->sum('sub_total');
        $data['recieved_income'] = $paytype_query_d;
        $data['recieved_on_account'] = $paytype_due_d;

        $data['company_config']=CompanyConfigur::first();

        //return $data;
        return view('report.full-accounts-report',$data);

    }
    public function create()
    {
    }
    
    public function store(Request $request)
    {
        
    }

    public function edit(Request $request)
    {

    }

    public function destroy(Request $request)
    {
    

    }
    
    
}
