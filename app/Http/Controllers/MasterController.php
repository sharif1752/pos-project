<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Model\SalesPersonModel;
use App\Model\SalesPersonShiftModel;
use App\Model\MenuTable;
use App\Model\UserRole;
use App\Model\UserRoleAccess;
use DateTime;
use DB;
class MasterController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){

        $id = Auth::guard('user')->user()->id;
        $data['user'] = SalesPersonModel::findOrFail($id);
        return view('app',$data);
    }
    //Pos
    public function posIndex(Request $request) {
        $id = Auth::guard('user')->user()->id;
        //self::shiftLogDetails($id);
        $data['user'] = SalesPersonModel::findOrFail($id);
        return view('pos.app', $data);
    }

    public function getUserMenuList(){
       /* $where = ['panel_type' => 2];
        $menuList = MenuTable::where($where)->where('status',1)->orderBy('order_no', 'asc')->get();
        $data['menu_list'] = self::buildMenu($menuList->all());
        return response()->json($data);*/

        $id = Auth::guard('user')->user()->role_id;

        $menu_ids = UserRoleAccess::where('role_id',$id)->pluck('menu_id')->all();
        $menuList = MenuTable::whereIn('id',$menu_ids)->orderBy('order_no','Asc')->get();
        $data['menuTopbar'] = MenuTable::whereIn('id',$menu_ids)->where('is_top_bar','=','1')->orderBy('order_no','Asc')->get();

        $data['menu_list'] = self::buildMenu($menuList->all());
        return response()->json($data);

    }

    public static function buildMenu(array $elements, $parentId = 0) {
        $menuGrid = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::buildMenu($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $menuGrid[] = $element;
            }
        }
        return $menuGrid;
    }

    public function autoLogOutAction(Request $request) {
        $data['status'] = 'logout';
        $from  = Session::get('from');
        if($from == 'admin'){
            $data['url'] = url('admin/login');
        }else{
            $data['url'] = url('/');
        }

        return response($data);
    }
    /*public static function shiftLogDetails($id)
    {

      $login_status=DB::table('inv_salesperson_shift')->where('sales_id',$id)->where('status',1)->first();
      $date=new DateTime();
      $date_time=$date->format('Y-m-d H:i:s');
      if(!empty($login_status)){

       $get_chek_date = explode(' ',$date_time);

       $check=DB::table('inv_salesperson_shift')->where('status',1)->where('start_time','<',$get_chek_date[0])->first();

       if($check){
          $format_time =explode(' ',$check->start_time);
          $format_time[1] = '11:59:00';
          $pre_date_time = $format_time[0].' '.$format_time[1];
          DB::table('inv_salesperson_shift')->update(array('status' => 0,"end_time"=>$pre_date_time));
        }
       $data=[
         "sales_id"=>$id,
         "start_time"=>$date_time,
         "status"=>1
       ];
       $insert=SalesPersonShiftModel::create($data);

    }else{
        $data=[
         "sales_id"=>$id,
         "start_time"=>$date_time,
         "status"=>1
       ];
       $insert=SalesPersonShiftModel::create($data);
    }
  }*/

}
