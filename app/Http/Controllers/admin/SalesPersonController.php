<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SalesPersonModel;
use App\Model\MenuTable;
use App\Model\CompanyConfigur;
use App\Model\InventoryPosModel;
use App\Model\InventoryPosDetailsModel;
use DB;
use Image;
use Auth;

class SalesPersonController extends Controller
{

	public function index(Request $request)
    {
        $paginate_num = $request->input('paginate_num');
        $search_key = $request->input('search_key');
        $order = $request->input('order');
        $sort = $request->input('sort');
        $paginate_data = SalesPersonModel::valid()->when($search_key, function($query, $search_key){
                    $query->where(function($query2)use($search_key){
                        $query2->where('name','LIKE','%'.$search_key.'%')
                        ->orWhere('number','LIKE', '%'.$search_key.'%')
                        ->orWhere('username','LIKE', '%'.$search_key.'%')
                        ->orWhere('email','LIKE', '%'.$search_key.'%');
                    });
                    return $query;

            })->where('role_id','=','1')->orderBy($sort,$order)->paginate($paginate_num);

        $company_config=CompanyConfigur::first();
        foreach ($paginate_data as $v) {
            $v->number = self::getNumber($v->number);
        }
        $data['paginate_data'] = $paginate_data;
        $data['formData']['number'] = self::getNumber();
        $data['formData']['commission_on'] = 'profit';
        $data['formData']['company_config'] = $company_config;
        return response()->json($data);
    }

    public function reate()
    {
        $data['number'] = self::getNumber();
        $data['commission_on'] = 'profit';
        return response($data);
    }

    public function store(Request $request)
    {
        $validation = [
            'number' => 'required|numeric|min:1|unique:inv_sales_person,number,'.$request->id,
            'name' => 'required',
            'username' =>  'required|unique:inv_sales_person,username,'.$request->id,
            'commission_percentage' => 'numeric',

        ];
				$request->validate($validation);
        $data = $request->only('number','name', 'username','address_line_1', 'address_line_2','city', 'state','zip', 'phone', 'email', 'ssn', 'commission_percentage', 'commission_on', 'comments');

        if(empty($chk)){
            if(!empty($request->id)){

                if(!empty($request->password)){
                    $data['password'] = bcrypt($request->password);
                    $validation['password'] = 'required';
                    $data['role_id'] = 1;
                }
                $data['updated_by']=Auth::guard('admin')->user()->id;
                $request->validate($validation);
                $save =  SalesPersonModel::valid()->findOrFail($request->id)->update($data);
            }else{

                $data['password'] = bcrypt($request->password);
                $validation['password'] = 'required';
                $data['created_by']=Auth::guard('admin')->user()->id;
                $data['role_id'] = 1;
                $request->validate($validation);
                $save =   SalesPersonModel::create($data);
            }

           if($save){
                $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
            }else{
                $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];
            }

        }else{
            $output = ['status' => 0, 'message' => 'Sorry! Username already exist.'];
        }
        return response()->json($output);
    }

    public function create()
    {
        $data['number'] = self::getNumber();
        $data['commission_on'] = 'profit';
        $data['company_config'] = CompanyConfigur::first();
        return response($data);
    }

    public function edit(Request $request)
    {

    $salesPerson = SalesPersonModel::findOrFail($request->id);
        $company_config=CompanyConfigur::first();

        $last_shift = DB::table('inv_salesperson_shift')->where('status',0)->where('sales_id',$salesPerson->id)->orderBy('id','desc')->first();

        //dd($last_shift);
        if($last_shift){
            $sales_nontaxable_shift = InventoryPosDetailsModel::valid()
            ->join('inv_product', 'inv_product.id', '=', 'inv_pos_details.product_id')
            ->select(DB::raw('inv_pos_details.product_qty*inv_pos_details.product_price as sub_total'))
            ->where('inv_product.service','=', '0')
            ->where('inv_pos_details.created_at','>=',$last_shift->start_time)
            ->where('inv_pos_details.created_at','<=',$last_shift->end_time)
            ->where('inv_pos_details.created_by',$salesPerson->id)
            ->get()
            ->sum('sub_total');
            $salesPerson->shift = $sales_nontaxable_shift;
        }else{
            $salesPerson->shift = 0;
        }

        $salesPerson->password = "";
        $salesPerson->company_config =$company_config;
        return response($salesPerson);
    }

    public function destroy(Request $request)
    {

        $salesPerson = SalesPersonModel::valid()->findOrFail($request->id);
        $data['deleted_by']=Auth::guard('admin')->user()->id;
        $data['valid'] = 0;
        $data['deleted_at']= date('Y-m-d H:i:s');
        if($salesPerson->update($data)){
            return response(['status' => 1, 'message' => 'Your data is successfully deleted']);
        }

    }

    public static  function getNumber($num=0){
        if($num>0){
           $number = $num;
        }else{
           $sales_person_info = SalesPersonModel::valid()->orderBy('number','desc')->first();
           $number = (!empty($sales_person_info)) ? $sales_person_info->number + 1 : 1;
        }
        $numlength = strlen((string)$number);
        if($numlength<4){
            if($numlength ==1){
              $number = '000'.$number;
            }else if($numlength ==2){
              $number = '00'.$number;
            }else{
             $number = '0'.$number;
            }
        }else{
           $number = $number;
        }
        return $number;
    }



}
