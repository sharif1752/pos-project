<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdminModel;
use Auth;
class ChangePasswordController extends Controller
{
    public function passwordChange(Request $request){

    	try{

    		$request->validate([
    			'oldpassword'=>'required',
    			'newpassword'=>'required',
    		]);
            $userId = Auth::guard('admin')->user()->id;
            if(!empty($userId)){
                $user = AdminModel::valid()->find($userId);
                if(\Hash::check($request->oldpassword, $user->password)){
                    $input["password"] = bcrypt($request->newpassword);
                    $save = AdminModel::valid()->find($userId)->update($input);
                    $output = ['status' => 1, 'message' => 'Password successfully changed.'];
                }else{
                    $output = ['status' => 0, 'message' => 'Old password is wrong.'];   
                }
            }else{
                $output = ['status' => 0, 'message' => 'Invalid userId.'];
            }
    	}catch (Throwable $e) {
            throw $e;
        }
        return response()->json($output);

    }
}
