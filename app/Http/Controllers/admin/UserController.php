<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SalesPersonModel;
use App\Model\MenuTable;
use App\Model\UserRole;
use App\Model\UserRoleAccess;
use Image;
use Auth;

class UserController extends Controller
{

	public function index(Request $request)
    {
        $paginate_num = $request->input('paginate_num');
        $search_key = $request->input('search_key');
        $order = $request->input('order');
        $sort = $request->input('sort');
        $data['paginate_data'] =SalesPersonModel::valid()->when($search_key, function($query, $search_key){
                    $query->where(function($query2)use($search_key){
                        $query2->where('name','LIKE','%'.$search_key.'%')
                        ->orWhere('email','LIKE', '%'.$search_key.'%');
                    });
                    return $query;

            })->where('role_id','=','2')->orderBy($sort,$order)->paginate($paginate_num); 
  
        return response()->json($data);
    }

    public static function buildMenu(array $elements, $parentId = 0) {
        $menuGrid = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::buildMenu($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $menuGrid[] = $element;
            }
        }

        return $menuGrid;
    }

    public function store(Request $request)
    {
        $validation = [
            'number' => 'required|unique:inv_sales_person,number,'.$request->id,
            'name' => 'required',
            'email' => 'required|unique:inv_sales_person,email,'.$request->id,
            'username' =>  'required|unique:inv_sales_person,username,'.$request->id,
        ];

        $data = $request->only('name','email','username','number','role_id');

        if(empty($chk)){
            
            if(!empty($request->id)){

                if(!empty($request->password)){
                    $data['password'] = bcrypt($request->password);  
                    $validation['password'] = 'required';
                }
                $data['updated_by']=Auth::guard('admin')->user()->id;
                $data['role_id'] = 2;
                $request->validate($validation);
                $user = SalesPersonModel::findOrFail($request->id);

                $save =  SalesPersonModel::valid()->findOrFail($request->id)->update($data);
            }else{

                $data['password'] = bcrypt($request->password);  
                $validation['password'] = 'required';
                $data['created_by']=Auth::guard('admin')->user()->id;
                $data['role_id'] = 2;
                $request->validate($validation);

                $save =   SalesPersonModel::create($data); 
            }   
            
           if($save){
                $output = ['status' => 1, 'message' => 'Your data is successfully saved'];
            }else{
                $output = ['status' => 0, 'message' => 'Ops! Something went worng.'];     
            }

        }else{
            $output = ['status' => 0, 'message' => 'Sorry! Username already exist.'];     
        }
        return response()->json($output);
    }

    public function show($id)
    {

        $user = SalesPersonModel::findOrFail($id);
        $user->password = "";
        $path ="../public/images/".$user->photo;
        $user->photo =$path;
        return response($user);
    }

    public function destroy($id)
    {
       
        $user = SalesPersonModel::valid()->findOrFail($id);

        $data['deleted_by']=Auth::guard('admin')->user()->id;
        $data['valid'] = 0;
        $data['deleted_at']= date('Y-m-d H:i:s');

          if($user->update($data)){
            return response(['status' => 1, 'message' => 'Your data is successfully deleted']);
        }

    }

    public function create()
    {
        $user = ['status' => "1",'photo'=>"../public/images/default.png"];
        return response($user);
    }

    public function getUserMenuList(){
        $where = ['panel_type' => 2];
        $menuList = MenuTable::where($where)->where('status',1)->orderBy('order_no', 'asc')->get();
        $data['menu_list'] = self::buildMenu($menuList->all());
        return response()->json($data);

    }
    
}
