<?php

namespace App\Model;


class Vendor extends BaseModel
{
    protected $table = 'inv_vendor';

    protected $guarded = array('id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'valid');

    public static function boot()
    {
        parent::userBoot();
    }

    public function scopeValid($query)
    {
        return $query->where('inv_vendor.valid', 1);
    }
    
}
