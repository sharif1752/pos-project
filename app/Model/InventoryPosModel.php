<?php

namespace App\Model;

class InventoryPosModel extends BaseModel
{
	protected $table = 'inv_pos';

	protected $guarded = array('id', 'created_by', 'created_at','updated_by','updated_at','deleted_by','deleted_at','valid');

	public static function boot()
	{
			parent::userBoot();
	}

	public function scopeValid($query)
	{
			return $query->where('inv_pos.valid', 1);
	}
}
