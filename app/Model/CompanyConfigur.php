<?php

namespace App\Model;


class CompanyConfigur extends BaseModel
{
    protected $table = 'inv_company_configure';

    protected $guarded = array('id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'valid');

    public static function boot()
    {
        parent::adminBoot();
    }

    public function scopeValid($query)
    {
        return $query->where('inv_company_configure.valid', 1);
    }
    
}
