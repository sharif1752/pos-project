<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SalesPersonModel extends Authenticatable
{
	use Notifiable;

    protected $guard ='user';

    protected $table = 'inv_sales_person';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function scopeValid($query)
    {
        return $query->where('inv_sales_person.valid', 1);
    }
}
