<?php

namespace App\Model;

class UserRole extends BaseModel
{
    protected $table = 'user_roel';

    protected $guarded = array('id', 'created_at', 'updated_at');

    

    public static function boot()
    {
        parent::adminBoot();
    }

    public function scopeValid($query)
    {
        return $query->where('user_roel.valid', 1);
    }


}
