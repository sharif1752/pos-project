<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRoleAccess extends BaseModel
{
    protected $table = 'user_access_role';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public static function boot()
    {
        parent::adminBoot();
    }

    public function scopeValid($query)
    {
        return $query->where('user_access_role.valid', 1);
    }
}
