<?php

namespace App\Model;

class InventoryCategoryModel extends BaseModel
{
	protected $table = 'inv_product_category';

	protected $guarded = array('id', 'created_by', 'created_at','updated_by','updated_at','deleted_by','deleted_at','valid');

	public static function boot()
	{
			parent::userBoot();
	}

	public function scopeValid($query)
	{
			return $query->where('inv_product_category.valid', 1);
	}
}
