<?php

namespace App\Model;

class SalesPersonShiftModel extends BaseModel
{
	protected $table = 'inv_salesperson_shift';

	protected $guarded = array('id', 'created_by', 'created_at','updated_by','updated_at','deleted_by','deleted_at','valid');
    protected $dates = ['created_at'];
	public static function boot()
	{
			parent::userBoot();
	}

	public function scopeValid($query)
	{
			return $query->where('inv_salesperson_shift.valid', 1);
	}
}
