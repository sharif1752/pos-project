<?php

namespace App\Model;


class Customer extends BaseModel
{
    protected $table = 'inv_customer';

    protected $guarded = array('id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by', 'valid');

    public static function boot()
    {
        parent::userBoot();
    }

    public function scopeValid($query)
    {
        return $query->where('inv_customer.valid', 1);
    }
    
}
