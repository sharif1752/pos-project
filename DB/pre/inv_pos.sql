-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 29, 2020 at 07:15 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_pos`
--

CREATE TABLE `inv_pos` (
  `id` int(11) NOT NULL,
  `invoice_number` int(11) NOT NULL,
  `date` date NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `pay_type` varchar(100) NOT NULL,
  `sub_total` double(16,2) NOT NULL,
  `sales_tax` float(11,2) DEFAULT '0.00',
  `total_discount` float(11,2) DEFAULT '0.00',
  `total_due` double(16,2) DEFAULT '0.00',
  `total_pay` double(16,2) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `valid` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inv_pos`
--

INSERT INTO `inv_pos` (`id`, `invoice_number`, `date`, `customer_name`, `pay_type`, `sub_total`, `sales_tax`, `total_discount`, `total_due`, `total_pay`, `comment`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`, `valid`) VALUES
(47691, 53316, '2020-01-01', NULL, 'Cash', 1.50, 0.00, 0.00, 0.00, 1.50, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47645, 53270, '2020-01-01', NULL, 'Cash', 2.00, 0.00, 0.00, 0.00, 2.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47687, 53312, '2020-01-01', NULL, 'Cash', 2.00, 0.00, 0.00, 0.00, 2.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47646, 53271, '2020-01-01', NULL, 'Cash', 4.00, 0.00, 0.00, 0.00, 4.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47631, 53256, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47639, 53264, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47648, 53273, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47659, 53284, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47661, 53286, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47673, 53298, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47684, 53309, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47688, 53313, '2020-01-01', NULL, 'Cash', 5.00, 0.00, 0.00, 0.00, 5.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47669, 53294, '2020-01-01', NULL, 'Cash', 8.00, 0.00, 0.00, 0.00, 8.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47643, 53268, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47644, 53269, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47647, 53272, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47665, 53290, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47666, 53291, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47671, 53296, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47676, 53301, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47678, 53303, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47682, 53307, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47683, 53308, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47689, 53314, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1),
(47693, 53318, '2020-01-01', NULL, 'Cash', 10.00, 0.00, 0.00, 0.00, 10.00, NULL, NULL, '2019-12-31 18:00:00', NULL, '2019-12-31 18:00:00', NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_pos`
--
ALTER TABLE `inv_pos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_pos`
--
ALTER TABLE `inv_pos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48672;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
