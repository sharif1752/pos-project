-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2020 at 09:15 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_salesperson_shift`
--

CREATE TABLE `inv_salesperson_shift` (
  `id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL COMMENT 'salesperson-->primary_id',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_by` int(11) NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inv_salesperson_shift`
--

INSERT INTO `inv_salesperson_shift` (`id`, `sales_id`, `start_time`, `end_time`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`, `valid`) VALUES
(1, 6, '2020-02-22 06:01:29', '2020-02-23 11:59:00', '0', 6, '2020-02-24 04:01:11', 6, '2020-02-23 00:01:29', 0, '0000-00-00 00:00:00', 1),
(2, 6, '2020-02-23 06:11:11', '2020-02-23 11:59:00', '0', 6, '2020-02-24 04:01:11', 6, '2020-02-23 00:11:11', 0, '0000-00-00 00:00:00', 1),
(3, 6, '2020-02-23 06:22:23', '2020-02-23 11:59:00', '0', 6, '2020-02-24 04:01:11', 6, '2020-02-23 00:22:23', 0, '0000-00-00 00:00:00', 1),
(4, 2, '2020-02-24 03:42:21', '2020-02-23 11:59:00', '0', 2, '2020-02-24 04:01:11', 2, '2020-02-23 21:42:21', 0, '0000-00-00 00:00:00', 1),
(5, 6, '2020-02-24 04:01:11', '0000-00-00 00:00:00', '1', 6, '2020-02-23 22:01:11', 6, '2020-02-23 22:01:11', 0, '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_salesperson_shift`
--
ALTER TABLE `inv_salesperson_shift`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_salesperson_shift`
--
ALTER TABLE `inv_salesperson_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
