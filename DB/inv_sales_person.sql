-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 19, 2020 at 05:25 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_sales_person`
--

CREATE TABLE `inv_sales_person` (
  `id` int(11) NOT NULL,
  `number` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(191) DEFAULT 'NULL',
  `password` varchar(191) DEFAULT 'NULL',
  `address_line_1` varchar(200) DEFAULT NULL,
  `address_line_2` varchar(200) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `ssn` varchar(100) DEFAULT NULL,
  `commission_percentage` int(11) DEFAULT NULL,
  `commission_on` varchar(100) DEFAULT NULL,
  `total_sales_1` double(16,2) DEFAULT NULL COMMENT 'daily',
  `total_sales_2` double(16,2) DEFAULT NULL COMMENT 'monthly',
  `total_sales_3` double(16,2) DEFAULT NULL COMMENT 'yearly',
  `commission_earned_1` double(16,2) DEFAULT NULL,
  `commission_earned_2` double(16,2) DEFAULT NULL,
  `commission_earned_3` double(16,2) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `valid` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_sales_person`
--

INSERT INTO `inv_sales_person` (`id`, `number`, `name`, `username`, `password`, `address_line_1`, `address_line_2`, `city`, `state`, `zip`, `phone`, `email`, `role_id`, `ssn`, `commission_percentage`, `commission_on`, `total_sales_1`, `total_sales_2`, `total_sales_3`, `commission_earned_1`, `commission_earned_2`, `commission_earned_3`, `comments`, `remember_token`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `valid`) VALUES
(1, 0001, 'Rahim', 'rahim', '$2y$10$GLLXS.90VnaRiAq240eroe4GUGtdGR7cGDyUBC9MEtDPqYMBqC9ge', 'Dhaka, Bangladesh', NULL, 'Dhaka', 'Dhaka', '101', '112345', 'a@gmail.com', 2, '12334', 20, 'profit', 7629.60, 8016.70, 8016.70, 1525.92, 1603.34, 1603.34, 'Test User', NULL, '2019-12-18 01:42:11', '2020-01-09 00:20:20', 1, NULL, NULL, NULL, 1),
(2, 0002, 'Karim', 'karim', '$2y$10$48YwSUCUDqklcf2zld1raOjDU8Gy50HnkVNdEfUS2c85v0gbNUEyq', 'Cumilla, Bangladesh', NULL, 'Cumilla', 'cumilla', '345', '1234', 'k@gmail.com', 1, '123', 10, 'profit', 50.00, 50.00, 50.00, 5.00, 5.00, 5.00, 'Test Person', NULL, '2019-12-18 03:13:36', '2020-01-16 00:59:05', 1, 1, NULL, NULL, 1),
(3, 0003, 'Noyon', 'Noyon', '$2y$10$MnFfGp3z5MXy5bWAgaiH1uei1M0YhSUu1j.ZeAEwaytamvA41q/9i', 'mirpur,dhaka', 'dhaka', 'dhaka', 'Dhaka', '1216', '01849942053', 'noyon@gmail.com', 2, '23', 20, 'gross', 1564.00, 1564.00, 1564.00, 312.80, 312.80, 312.80, 'noyon', NULL, '2020-01-09 00:27:22', '2020-01-09 01:12:37', 1, NULL, NULL, NULL, 1),
(4, 0004, 'Tania', 'Tania', '$2y$10$iGRRsjKeybpYrjj1pyWWa.Ke9CvBa0BbipoxhBvSrJ5YsENhukImu', 'Mirpur', NULL, 'Dhaka', 'D', '1204', '01912096637', 's@gamil.com', 2, NULL, 0, 'profit', 850.00, 850.00, 850.00, 0.00, 0.00, 0.00, NULL, NULL, '2020-01-09 01:52:41', '2020-01-16 00:34:35', 1, 1, NULL, NULL, 1),
(5, 0005, 'anwar', 'anwar', '$2y$10$7.5PgV8DTI1.CzeGEAgLxeQQdJH9D94kIhtTTqht7YC/gVl66P/FS', 'dhaka', NULL, 'dhaka', NULL, NULL, '0998777', 'anwar@gmail.com', 2, NULL, 20, 'profit', 302.00, 302.00, 302.00, 60.40, 60.40, 60.40, NULL, NULL, '2020-01-09 01:55:22', '2020-01-09 03:57:09', 1, NULL, NULL, NULL, 1),
(6, 0006, 'habib', 'habib', '$2y$10$zfGqIoLSMHmBJnhD9kg86uastx/guopukwKHnRDdboR7fJVg1GT.i', 'dhaka', NULL, 'dhaka', '12', '23', '098766', 'habib@gmail.com', 3, '122', 0, 'profit', 98.00, 98.00, 98.00, 0.00, 0.00, 0.00, NULL, NULL, '2020-01-09 04:21:22', '2020-01-13 22:40:35', 1, 1, NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_sales_person`
--
ALTER TABLE `inv_sales_person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `number` (`number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_sales_person`
--
ALTER TABLE `inv_sales_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
