-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2020 at 08:48 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_tabe`
--

CREATE TABLE `menu_tabe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order_no` int(11) DEFAULT NULL,
  `menu_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_child` tinyint(4) NOT NULL,
  `menu_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `panel_type` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_tabe`
--

INSERT INTO `menu_tabe` (`id`, `menu_name`, `uid`, `parent_id`, `order_no`, `menu_link`, `has_child`, `menu_icon`, `panel_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 29, 1, '/adminlist', 0, 'icon-user', 1, 1, '2019-10-07 03:24:41', '2019-11-28 00:46:34'),
(2, 'User Role', 'User Role', 29, 2, '/userRole', 0, 'icon-user', 1, 1, '2019-10-07 03:26:25', '2019-11-28 00:51:25'),
(9, 'Admin Role', 'Admin Role', 29, 8, '/adminRole', 0, 'icon-user', 1, 1, '2019-10-18 22:11:55', '2019-11-28 00:51:41'),
(18, 'User', 'User', 29, 15, '/userlist', 0, 'icon-user', 1, 1, '2019-11-04 01:16:29', '2019-11-28 00:46:12'),
(29, 'peoples', 'peoples', 0, 19, NULL, 1, 'icon-group', 1, 1, '2019-11-28 00:40:56', '2019-11-28 00:48:11'),
(37, 'Inventory', 'Inventory', 0, 20, NULL, 1, 'icon-table', 2, 1, '2019-12-11 22:06:14', '2019-12-11 22:23:54'),
(39, 'Inventory Category', 'Inventory Category', 37, 21, '/inventory_category', 0, 'icon-reorder', 2, 1, '2019-12-11 22:26:03', '2019-12-11 22:26:03'),
(40, 'Inventory Item', 'Inventory Item', 37, 22, '/inventory_item', 0, 'icon-pencil', 2, 1, '2019-12-11 22:38:49', '2019-12-12 00:04:40'),
(41, 'Inventory Receive', 'Inventory Receive', 37, 23, '/inventory_receive', 0, 'icon-signin', 2, 1, '2019-12-11 22:41:29', '2019-12-11 22:41:29'),
(42, 'check', 'check', 29, 24, '/check', 1, 'icon-table', 1, 1, '2019-12-12 03:07:41', '2019-12-12 03:07:41'),
(43, 'Setting', 'setting', 0, 25, '/setting', 1, 'icon-cogs', 1, 1, '2019-12-16 21:53:44', '2019-12-16 21:53:44'),
(44, 'Vendor', 'vendor', 0, 26, '/vendor', 1, 'icon-table', 2, 1, '2019-12-21 23:31:48', '2019-12-21 23:47:31'),
(45, 'SalesPerson', 'salesperson', 0, 27, '/salesPerson', 1, 'icon-table', 1, 1, '2019-12-21 23:55:17', '2019-12-21 23:55:17'),
(46, 'Customer', 'cusotmer', 0, 28, '/customer', 1, 'icon-table', 2, 1, '2019-12-22 01:50:09', '2019-12-22 01:50:09'),
(47, 'Pos', 'Pos', 0, 26, '/pos', 0, 'icon-edit-sign', 2, 1, '2019-12-17 18:12:26', '2019-12-17 18:12:26'),
(48, 'Account Receivable', 'Account Receivable', 0, 27, '/account_receivable', 1, 'icon-edit-sign', 2, 1, '2019-12-23 16:47:26', '2019-12-23 16:47:26'),
(49, 'View Statement', 'View Statement', 48, 28, '/view_statement', 0, 'icon-file-text-alt', 2, 1, '2019-12-23 16:52:35', '2019-12-23 16:52:35'),
(50, 'Report', 'Report', 0, 29, NULL, 1, 'icon-table', 1, 1, '2019-12-25 21:17:54', '2019-12-25 21:17:54'),
(51, 'Inventory', 'inventory', 50, 30, NULL, 1, 'icon-reorder', 1, 1, '2019-12-25 21:18:55', '2019-12-25 21:18:55'),
(52, 'Item List', 'item list', 51, 31, '/itemlist', 0, 'icon-reorder', 1, 1, '2019-12-25 21:20:09', '2019-12-26 01:30:08'),
(53, 'Best Seller', 'best seller', 51, 32, '/bestseller', 0, 'icon-reorder', 1, 1, '2019-12-25 21:22:30', '2019-12-26 01:29:18'),
(54, 'Worst Seller', 'worst seller', 51, 33, '/worstseller', 0, 'icon-reorder', 1, 1, '2019-12-25 21:23:17', '2019-12-26 01:29:11'),
(55, 'Items On Order', 'intems on order', 51, 34, '/itemsorder', 0, 'icon-reorder', 1, 1, '2019-12-26 01:28:25', '2019-12-26 01:28:25'),
(56, 'Price List', 'Price List', 51, 35, '/pricelist', 0, 'icon-reorder', 1, 1, '2019-12-27 21:17:23', '2019-12-27 21:17:23'),
(57, 'Customer', 'Customer', 50, 36, NULL, 1, 'icon-group', 1, 1, '2019-12-27 21:21:46', '2019-12-27 21:21:46'),
(58, 'Customer List', 'Customer List', 57, 37, '/customerlist', 0, 'icon-user', 1, 1, '2019-12-27 21:22:55', '2019-12-27 21:24:04'),
(59, 'Vendor', 'vendor', 50, 38, NULL, 1, 'icon-reorder', 1, 1, '2019-12-27 21:27:18', '2019-12-27 21:27:54'),
(60, 'Vendor List', 'vendor list', 59, 39, '/vendorlist', 0, 'icon-reorder', 1, 1, '2019-12-27 21:29:58', '2019-12-27 21:29:58'),
(61, 'Sales Person', 'Sales Person', 50, 40, NULL, 1, 'icon-reorder', 1, 1, '2019-12-27 22:48:29', '2019-12-27 22:49:04'),
(62, 'Salesperson List', 'Salesperson List', 61, 41, '/salespersonlist', 0, 'icon-reorder', 1, 1, '2019-12-27 22:53:48', '2019-12-27 22:59:41'),
(63, 'Physical Inventory List', 'Physical Inventory', 51, 42, '/physicalinventory', 1, 'icon-reorder', 1, 1, '2019-12-28 02:42:52', '2019-12-28 02:56:30'),
(64, 'Inventory Value By Category', 'Inventory Value By Category', 51, 43, 'inv-value-by-category', 0, 'icon-reorder', 1, 1, '2019-12-28 22:25:32', '2019-12-28 22:27:07'),
(65, 'Inventory Vlue By Vendor', 'Inventory Vlue By Vendor', 51, 44, 'inv-value-by-vendor', 0, 'icon-reorder', 1, 1, '2019-12-29 03:39:52', '2019-12-29 03:40:47'),
(66, 'Items Bellow Recoder Level', 'Items Bellow Recoder Level', 51, 45, 'items-bellow-reorder-level', 0, 'icon-reorder', 1, 1, '2019-12-29 04:59:23', '2019-12-29 09:27:48'),
(67, 'Inventory Sales Report', 'Inventory Sales Report', 51, 46, '/inventory-sales-report', 0, 'icon-reorder', 1, 1, '2019-12-30 08:44:48', '2019-12-30 08:46:35'),
(68, 'Invoice History', 'Invoice History', 50, 47, '/invoicehistory', 0, 'icon-reorder', 1, 1, '2019-12-31 11:00:50', '2019-12-31 11:01:42'),
(69, 'Accounts', 'Accounts', 50, 48, '/accounts', 0, 'icon-reorder', 1, 1, '2020-01-02 11:05:36', '2020-01-02 11:05:36'),
(70, 'Account Receiveable', 'Account Receiveable', 50, 49, NULL, 1, 'icon-signin', 1, 1, '2020-01-05 23:01:10', '2020-01-05 23:01:10'),
(71, 'AR Aging Report', 'AR Asing Report', 70, 50, '/ar-asing-report', 0, 'icon-reorder', 1, 1, '2020-01-05 23:04:04', '2020-01-05 23:04:04'),
(72, 'Post AR Payment', 'Post AR Payment', 48, 29, '/post_ar_payment', 0, 'icon-check', 2, 1, '2019-12-27 17:08:29', '2019-12-27 17:08:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_tabe`
--
ALTER TABLE `menu_tabe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_tabe`
--
ALTER TABLE `menu_tabe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
